<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('kiln_tuban_2.png');
		}

		.pinch:before {
			content: "";
			display: block;
			padding-bottom: 54.5%;
		}


		.kilnfeedlsftitle {
			position: absolute;
			width: 10%;
			top: 17%;
			left: 2%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.kilnfeedlsftitle:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.kilnfeedlsfvalue {
			position: absolute;
			top: 17%;
			left: 41%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.7vw;
			text-align: center;
		}

		.kilnfeedlsfvalue:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}


		.feedtoilctitle {
			position: absolute;
			width: 10%;
			top: 20%;
			left: 2%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.feedtoilctitle:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.feedtoilcvalue {
			position: absolute;
			width: 10%;
			top: 20%;
			left: 42%;
			color: #7CFF00;
			font-size: 0.7vw;
			text-align: center;
		}

		.feedtoilcvalue:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.feedtoslctitle {
			position: absolute;
			top: 23%;
			left: 2%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.feedtoilctitle:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.feedtoslcvalue {
			position: absolute;
			top: 23%;
			left: 41%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.7vw;
			text-align: center;
		}

		.feedtoslcvalue:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.rawmealsilo {
			position: absolute;
			top: 45%;
			left: 0.6%;
			width: 6.9%;
			color: black;
			font-size: 0.5vw;
			text-align: center;
			font-weight: bold
		}

		.rawmealsilo:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.rawmealsilotitle {
			position: absolute;
			width: 7%;
			top: 47%;
			left: 2.5%;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
			text-align: center;
		}

		.rawmealsilotitle:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.rawmealsilovalue {
			position: absolute;
			width: 10%;
			top: 49.5%;
			left: 3.4%;
			color: #7CFF00;
			font-size: 0.7vw;
			text-align: center;
		}

		.rawmealsilovalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		/*  */
		.rawmealsilo2 {
			position: absolute;
			font-weight: bold;
			top: 45%;
			left: 6.6%;
			color: black;
			width: 7%;
			font-size: 0.5vw;
			text-align: center;
			font-weight: bold
		}

		.rawmealsilo2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.rawmealsilo2title {
			position: absolute;
			top: 47%;
			left: 8.7%;
			width: 10%;
			color: black;
			font-size: 0.6vw;
			text-align: center;
			font-weight: bold;
		}

		.rawmealsilo2title:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.rawmealsilo2value {
			position: absolute;
			top: 49.5%;
			left: 9.3%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.7vw;
			text-align: center;
		}

		.rawmealsilo2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422bi1titile {
			position: absolute;
			top: 62%;
			left: 2.9%;
			width: 10%;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
			text-align: center;
		}

		.name422bi1titile:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.name422bi1value {
			position: absolute;
			top: 64.5%;
			left: 3.6%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.5vw;
			text-align: center;
		}

		.name422bi1value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422bi2titile {
			position: absolute;
			top: 62%;
			left: 8.4%;
			width: 10%;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
			text-align: center;
		}

		.name422bi2titile:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422bi2value {
			position: absolute;
			top: 64.2%;
			left: 9.2%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
			text-align: center;
		}

		.name422bi2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422be1name {
			position: absolute;
			top: 5%;
			left: 13.7%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.name422be1name:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422be1value {
			position: absolute;
			top: 7%;
			left: 14%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
			text-align: center;
		}

		.name422be1value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422be2name {
			position: absolute;
			top: 5%;
			left: 23%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.name422be2name:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422be2value {
			position: absolute;
			top: 7%;
			left: 24%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
			text-align: center;
		}

		.name422be2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422srname {
			position: absolute;
			top: 17%;
			left: 25.6%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
			text-align: center;
		}

		.name422srname:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422srvalue {
			position: absolute;
			top: 20%;
			left: 26%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
			text-align: center;
		}

		.name422srvalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.clinkerprodratetitile {
			position: absolute;
			top: 14.4%;
			line-height: 10%;
			right: 1%;
			left: 46%;
			color: white;
			font-size: 0.7vw;
		}

		.clinkerprodratevalue {
			position: absolute;
			top: 0%;
			left: 22%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.clinkertemptitle {
			position: absolute;
			line-height: 10%;
			top: 14.4%;
			right: 1%;
			left: 65%;
			color: white;
			font-size: 0.7vw;
		}

		.clinkertempvalue {
			position: absolute;
			top: 2%;
			left: 27%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.nhvcoaltitle {
			position: absolute;
			line-height: 10%;
			top: 14.4%;
			right: 2%;
			left: 80%;
			color: white;
			font-size: 0.7vw;
		}

		.nhvcoalvalue {
			position: absolute;
			top: 2%;
			left: 60%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.name442fn2name {
			position: absolute;
			top: 27%;
			left: 35.4%;
			color: white;
			font-size: 0.7vw;
			width: 10%;
			text-align: center;
		}

		.name442fn2name:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442fn2value {
			position: absolute;
			top: 34%;
			left: 35.8%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442fn2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value1 {
			position: absolute;
			top: 36%;
			left: 35.5%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442da2titile {
			position: absolute;
			top: 27%;
			left: 42%;
			color: white;
			font-size: 0.7vw;
			width: 10%;
			text-align: center;
		}

		.name442da2titile:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442da2value {
			position: absolute;
			top: 34%;
			left: 42.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442da2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value2 {
			position: absolute;
			top: 31.8%;
			left: 46.4%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value3 {
			position: absolute;
			top: 33.7%;
			left: 46.4%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf54title {
			position: absolute;
			top: 30%;
			left: 51.7%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf54title:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf54value {
			position: absolute;
			top: 33%;
			left: 53.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf54value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442an2titile {
			position: absolute;
			top: 38%;
			left: 51%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442an2titile:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		/*  */
		.name442an2name {
			position: absolute;
			top: 41.5%;
			line-height: 10%;
			right: 1%;
			left: 51%;
			color: white;
			font-size: 0.7vw;
		}

		.name442an2value {
			position: absolute;
			top: 1%;
			left: 8%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		/*  */
		.name422rf5name {
			position: absolute;
			top: 30%;
			left: 61%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf5name:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf5value {
			position: absolute;
			top: 33%;
			left: 61.5%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf5value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value4 {
			position: absolute;
			top: 24%;
			left: 66%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value5 {
			position: absolute;
			top: 25.5%;
			left: 63.8%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442da1 {
			position: absolute;
			top: 24%;
			left: 69.4%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442da1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442da1value {
			position: absolute;
			top: 30%;
			left: 69.9%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442da1value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442fn1 {
			position: absolute;
			top: 24%;
			left: 73.8%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442fn1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442fn1value {
			position: absolute;
			top: 30%;
			left: 74.2%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442fn1value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value6 {
			position: absolute;
			top: 30%;
			left: 77%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value6:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442an1 {
			position: absolute;
			top: 33%;
			left: 73.3%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442an1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442an1name {
			position: absolute;
			top: 36%;
			line-height: 10%;
			right: 1%;
			left: 73.3%;
			color: white;
			font-size: 0.7vw;
		}

		.name442an1value {
			position: absolute;
			top: 1%;
			left: 19%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.name428bi1 {
			position: absolute;
			top: 22.3%;
			left: 85.1%;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name428bi1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name428bi1value {
			position: absolute;
			top: 24.4%;
			left: 85.7%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name428bi1value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482pp2 {
			position: absolute;
			top: 41.6%;
			left: 78.1%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482pp2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name428pp1 {
			position: absolute;
			top: 41.6%;
			left: 92.3%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name428pp1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482bi2 {
			position: absolute;
			top: 49.7%;
			left: 89.8%;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482bi2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482bi2value {
			position: absolute;
			top: 52%;
			left: 90.5%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482bi2value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482bi3 {
			position: absolute;
			top: 49.7%;
			left: 80.7%;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482bi3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482bi3value {
			position: absolute;
			top: 52%;
			left: 81.4%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482bi3value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482fn5 {
			position: absolute;
			top: 69%;
			left: 95.5%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482fn5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482fn4 {
			position: absolute;
			top: 76%;
			left: 95.5%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482fn4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482fn4value {
			position: absolute;
			top: 82.6%;
			left: 96%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482fn4value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482da4 {
			position: absolute;
			top: 76%;
			left: 92%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482da4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name482da4value {
			position: absolute;
			top: 82.6%;
			left: 93%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name482da4value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno44 {
			position: absolute;
			top: 65%;
			left: 73%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno44:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno44value1 {
			position: absolute;
			top: 67%;
			left: 72%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno44value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno44value2 {
			position: absolute;
			top: 69%;
			left: 72.3%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno44value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno46 {
			position: absolute;
			top: 65%;
			left: 79.8%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno46:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno46value1 {
			position: absolute;
			top: 67%;
			left: 79%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno46value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno46value2 {
			position: absolute;
			top: 69%;
			left: 79.3%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno46value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno48 {
			position: absolute;
			top: 65%;
			left: 88%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno48:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno48value1 {
			position: absolute;
			top: 67%;
			left: 87%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno48value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.nameno48value2 {
			position: absolute;
			top: 69%;
			left: 87.8%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.nameno48value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.cooler {
			position: absolute;
			width: 10%;
			top: 79%;
			left: 81.6%;
			color: yellow;
			font-weight: bold;
			font-size: 0.6vw;
			text-align: center;
		}

		.cooler:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value7 {
			position: absolute;
			top: 90.9%;
			left: 84.8%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value7:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value8 {
			position: absolute;
			top: 93%;
			left: 83.4%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value8:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.kilntitle {
			position: absolute;
			top: 94%;
			left: 65.8%;
			line-height: 10%;
			right: 1%;
			color: white;
			font-size: 0.6vw;
		}

		.kilnvalue {
			position: absolute;
			top: 0%;
			left: 22%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		/* .value9 {
			position: absolute;
			top: 92%;
			left: 63%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value9:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		} */

		.value10 {
			position: absolute;
			top: 95%;
			left: 60.9%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value10:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value11 {
			position: absolute;
			top: 92%;
			left: 60.9%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value11:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value13 {
			position: absolute;
			top: 90%;
			left: 44.8%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value13:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name442an3title {
			position: absolute;
			top: 88%;
			left: 34.7%;
			color: white;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name442an3title:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		/*  */
		.name442an3 {
			position: absolute;
			top: 91%;
			left: 34.7%;
			line-height: 10%;
			color: white;
			font-size: 0.7vw;
		}

		.name442an3value {
			position: absolute;
			top: 0%;
			left: 300%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		/*  */
		.name422rf4a {
			position: absolute;
			top: 45.4%;
			left: 27.1%;
			color: white;
			font-size: 0.7vw;
			width: 10%;
			text-align: center;
		}

		.name422rf4a:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf4avalue {
			position: absolute;
			top: 48.3%;
			left: 27.3%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf4avalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf4 {
			position: absolute;
			top: 45.4%;
			left: 31.7%;
			color: white;
			font-size: 0.7vw;
			width: 10%;
			text-align: center;
		}

		.name422rf4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name422rf4value {
			position: absolute;
			top: 48.3%;
			left: 31.7%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.name422rf4value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value14 {
			position: absolute;
			top: 52.3%;
			left: 36%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value14:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value15 {
			position: absolute;
			top: 54%;
			left: 33.7%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value15:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value16 {
			position: absolute;
			top: 52.3%;
			left: 58%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value16:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value17 {
			position: absolute;
			top: 54%;
			left: 55.5%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value17:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value18 {
			position: absolute;
			top: 57.3%;
			left: 48.9%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value18:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value19 {
			position: absolute;
			top: 59%;
			left: 48.9%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value19:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value20 {
			position: absolute;
			top: 53%;
			left: 70.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value20:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value21 {
			position: absolute;
			top: 55%;
			left: 70.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value21:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value22 {
			position: absolute;
			top: 67%;
			left: 36%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value22:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value23 {
			position: absolute;
			top: 69%;
			left: 33.1%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value23:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value24 {
			position: absolute;
			top: 67.3%;
			left: 58%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value24:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value25 {
			position: absolute;
			top: 69.3%;
			left: 55.5%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value25:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value26 {
			position: absolute;
			top: 75.8%;
			left: 41.4%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value26:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value27 {
			position: absolute;
			top: 78%;
			left: 39%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value27:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value28 {
			position: absolute;
			top: 73%;
			left: 70.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value28:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value29 {
			position: absolute;
			top: 75%;
			left: 70.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value29:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value30 {
			position: absolute;
			top: 79%;
			left: 70.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value30:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value31 {
			position: absolute;
			top: 77%;
			left: 78.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value31:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value32 {
			position: absolute;
			top: 84%;
			left: 47.6%;
			color: #7CFF00;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.value32:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelia1 {
			position: absolute;
			top: 40%;
			left: 43.7%;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
			width: 10%;
			text-align: center;
		}

		.labelia1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelib1 {
			position: absolute;
			top: 40%;
			font-weight: bold;
			left: 47.7%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelib1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelii1 {
			position: absolute;
			top: 51%;
			font-weight: bold;
			left: 40.3%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelii1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labeliii1 {
			position: absolute;
			top: 59%;
			font-weight: bold;
			left: 46%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labeliii1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labeliv1 {
			position: absolute;
			top: 68.8%;
			font-weight: bold;
			left: 40.3%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labeliv1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelslc1 {
			position: absolute;
			top: 77%;
			font-weight: bold;
			left: 45.7%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelslc1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelia2 {
			position: absolute;
			top: 40%;
			left: 65.6%;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
			width: 10%;
			text-align: center;
		}

		.labelia2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelib2 {
			position: absolute;
			top: 40%;
			font-weight: bold;
			left: 69.6%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelib2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelii2 {
			position: absolute;
			top: 51%;
			font-weight: bold;
			left: 62.3%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelii2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labeliii2 {
			position: absolute;
			top: 59%;
			font-weight: bold;
			left: 67.8%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labeliii2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labeliv2 {
			position: absolute;
			top: 68.8%;
			font-weight: bold;
			left: 62.3%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labeliv2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelslc2 {
			position: absolute;
			top: 77%;
			font-weight: bold;
			left: 67.8%;
			color: black;
			font-size: 0.6vw;
			width: 10%;
			text-align: center;
		}

		.labelslc2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}

		.square-box:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="kilnfeedlsftitle">
				<div class="square-content">
					<span>Kiln Feed LSF</span>
					<div class="kilnfeedlsfvalue">
						<span>99 %</span>
					</div>
				</div>
			</div>
			<div class="feedtoilctitle">
				<div class="square-content">
					<span>Feed to ILC</span>
					<div class="feedtoilcvalue">
						<span>228 t/h</span>
					</div>
				</div>
			</div>
			<div class="feedtoslctitle">
				<div class="square-content">
					<span>Feed to SLC</span>
					<div class="feedtoslcvalue">
						<span>282 t/h</span>
					</div>
				</div>
			</div>
			<div class="rawmealsilo">
				<div class="square-content">
					<span>RAW MEAL SILO 412BS1</span>
				</div>
			</div>

			<div class="rawmealsilovalue">
				<div class="square-content">
					<span>66%</span>
				</div>
			</div>
			<div class="rawmealsilo2">
				<div class="square-content">
					<span>RAW MEAL SILO 412BS2</span>
				</div>
			</div>
			<div class="rawmealsilo2value">
				<div class="square-content">
					<span>68 %</span>
				</div>
			</div>
			<div class="name422bi1titile">
				<div class="square-content">
					<span>422BI1</span>
				</div>
			</div>
			<div class="name422bi1value">
				<div class="square-content">
					<span>66 t</span>
				</div>
			</div>
			<div class="name422bi2titile">
				<div class="square-content">
					<span>422BI2</span>
				</div>
			</div>
			<div class="name422bi2value">
				<div class="square-content">
					<span>55 t </span>
				</div>
			</div>
			<div class="name422be1name">
				<div class="square-content">
					<span>422BE1</span>
				</div>
			</div>
			<div class="name422be1value">
				<div class="square-content">
					<span>26 %</span>
				</div>
			</div>
			<div class="name422be2name">
				<div class="square-content">
					<span>422BE2</span>
				</div>
			</div>

			<div class="name422be2value">
				<div class="square-content">
					<span>28%</s>
				</div>
			</div>
			<div class="name422srname">
				<div class="square-content">
					<span>422SR</span>
				</div>
			</div>
			<div class="name422srvalue">
				<div class="square-content">
					<span>26 %</span>
				</div>
			</div>
			<div class="clinkerprodratetitile">
				<p>Clinker Prod Rate</p>
				<p>Clinker Prod Yesterday</p>
				<p>Clinker Prod Today</p>
				<div class="clinkerprodratevalue">
					<p>354 th</p>
					<p>8443 th</p>
					<p>3034 th</p>
				</div>
			</div>
			<div class="clinkertemptitle">
				<p>Clinker Temp</p>
				<p>Clinker FCaO</p>
				<p>Clinker C3S</p>
				<div class="clinkertempvalue">
					<p>107°C</p>
					<p>0,89</p>
					<p>59,7%</p>
				</div>
			</div>

			<div class="nhvcoaltitle">
				<p>NHV Coal</p>
				<p>Heat Consumption</p>
				<p>Total Coal Feed</p>
				<div class="nhvcoalvalue">
					<p>4000 kcal/kg</p>
					<p>492 kcal</p>
					<p>32 t/h</p>
				</div>
			</div>
			<div class="name442fn2name">
				<div class="square-content">
					<span>442FN2</span>
				</div>
			</div>
			<div class="name442fn2value">
				<div class="square-content">
					<span>26%</span>
				</div>
			</div>
			<div class="value1">
				<div class="square-content">
					<span>1,1m/s</s>
				</div>
			</div>

			<div class="name442da2titile">
				<div class="square-content">
					<span>442DA2</span>
				</div>
			</div>
			<div class="name442da2value">
				<div class="square-content">
					<span>26 %</s>
				</div>
			</div>
			<div class="value2">
				<div class="square-content">
					<span>422 °C</span>
				</div>
			</div>
			<div class="value3">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>

			<div class="name422rf54title">
				<div class="square-content">
					<span>422RF54</span>
				</div>
			</div>
			<div class="name422rf54value">
				<div class="square-content">
					<span>26%</s>
				</div>
			</div>
			<div class="name442an2titile">
				<div class="square-content">
					<span>4222AN2</span>
				</div>
			</div>
			<div class="name442an2name">
				<p>O2 </p>
				<p>Co2</p>
				<div class="name442an2value">
					<p>2,17%</p>
					<p>0.03%</p>
				</div>
			</div>
			<div class="name422rf5name">
				<div class="square-content">
					<span>422RF5</span>
				</div>
			</div>
			<div class="name422rf5value">
				<div class="square-content">
					<span>26%</span>
				</div>
			</div>
			<div class="value4">
				<div class="square-content">
					<span>442 °C</span>
				</div>
			</div>
			<div class="value5">
				<div class="square-content">
					<span>557 mmH2O</span>
				</div>
			</div>

			<div class="name442da1">
				<div class="square-content">
					<span>442DA1</span>
				</div>
			</div>
			<div class="name442da1value">
				<div class="square-content">
					<span>26%s</span>
				</div>
			</div>
			<div class="name442fn1">
				<div class="square-content">
					<span>442FN1</span>
				</div>
			</div>
			<div class="name442fn1value">
				<div class="square-content">
					<span>26%</span>
				</div>
			</div>
			<div class="value6">
				<div class="square-content">
					<span>1,1 mm/s</span>
				</div>
			</div>
			<div class="name442an1">
				<div class="square-content">
					<span>422AN1</span>
				</div>
			</div>
			<div class="name442an1name">
				<p>O2</p>
				<p>Co2</p>
				<div class="name442an1value">
					<P>2.17 %</P>
					<P>0,03 %</P>
				</div>
			</div>
			<div class="name428bi1">
				<div class="square-content">
					<span>428BI1</span>
				</div>
			</div>
			<div class="name428bi1value">
				<div class="square-content">
					<span>32 t</span>
				</div>
			</div>
			<div class="name482pp2">
				<div class="square-content">
					<span>482PP2 </span>
				</div>
			</div>
			<div class="name428pp1">
				<div class="square-content">
					<span>428PP1</span>
				</div>
			</div>
			<div class="name482bi2">
				<div class="square-content">
					<span>482BI2</span>
				</div>
			</div>
			<div class="name482bi2value">
				<div class="square-content">
					<span>32 t</span>
				</div>
			</div>
			<div class="name482bi3">
				<div class="square-content">
					<span>482BI3</span>
				</div>
			</div>
			<div class="name482bi3value">
				<div class="square-content">
					<span>32 t</span>
				</div>
			</div>
			<div class="name482fn5">
				<div class="square-content">
					<span>482FN5</span>
				</div>
			</div>
			<div class="name482fn4">
				<div class="square-content">
					<span>482FN4</span>
				</div>
			</div>
			<div class="name482fn4value">
				<div class="square-content">
					<span>26 %</span>
				</div>
			</div>
			<div class="name482da4">
				<div class="square-content">
					<span>482DA4</span>
				</div>
			</div>
			<div class="name482da4value">
				<div class="square-content">
					<span>26%</span>
				</div>
			</div>
			<div class="nameno44">
				<div class="square-content">
					<span>44</span>
				</div>
			</div>
			<div class="nameno44value1">
				<div class="square-content">
					<span>3,2 t/h</span>
				</div>
			</div>
			<div class="nameno44value2">
				<div class="square-content">
					<span>28 %</span>
				</div>
			</div>
			<div class="nameno46">
				<div class="square-content">
					<span>46</span>
				</div>
			</div>
			<div class="nameno46value1">
				<div class="square-content">
					<span>2,2 t/h </span>
				</div>
			</div>
			<div class="nameno46value2">
				<div class="square-content">
					<span>32%</span>
				</div>
			</div>
			<div class="nameno48">
				<div class="square-content">
					<span>48</span>
				</div>
			</div>
			<div class="nameno48value1">
				<div class="square-content">
					<span>22,1 t/h</span>
				</div>
			</div>
			<div class="nameno48value2">
				<div class="square-content">
					<span>87 %</span>
				</div>
			</div>
			<div class="cooler">
				<div class="square-content">
					<p>COOLER</p>
				</div>
			</div>
			<div class="value7">
				<div class="square-content">
					<span>960 C</span>
				</div>
			</div>
			<div class="value8">
				<div class="square-content">
					<span>17 mmH2O</span>
				</div>
			</div>
			<div class="kilntitle">
				<p>Kiln speed</p>
				<p>kiln torque</p>
				<div class="kilnvalue">
					<p>2,61 rpm</p>
					<p>58,96%</p>
				</div>
			</div>
			<!-- <div class="value9">
							<p>A</p>
						</div> -->
			<div class="value10">
				<div class="square-content">
					<span>821 rpm</span>
				</div>
			</div>
			<div class="value11">
				<div class="square-content">
					<span>441 A</span>
				</div>
			</div>
			<div class="value13">
				<div class="square-content">
					<span>26 mmH2O</span>
				</div>
			</div>
			<div class="name442an3title">
				<div class="square-content">
					<span>442AN3</span>
				</div>
			</div>
			<div class="name442an3">
				<p>O2</p>
				<p>CO</p>
				<p>NO</p>
				<div class="name442an3value">
					<p>2,17 %</p>
					<p>0,03%</p>
					<p>603ppm</p>
				</div>
			</div>
			<div class="name422rf4a">
				<div class="square-content">
					<span>422RF4A</span>
				</div>
			</div>
			<div class="name422rf4avalue">
				<div class="square-content">
					<span>26 %</span>
				</div>
			</div>
			<div class="name422rf4">
				<div class="square-content">
					<span>422RF4</span>
				</div>
			</div>
			<div class="name422rf4value">
				<div class="square-content">
					<span>26 %</span>
				</div>
			</div>
			<div class="value14">
				<div class="square-content">
					<span>426°C</span>
				</div>
			</div>
			<div class="value15">
				<div class="square-content">
					<span>557 mmH2O</span>
				</div>
			</div>
			<div class="value16">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value17">
				<div class="square-content">
					<span>423 mmH20</span>
				</div>
			</div>
			<div class="value18">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value19">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value20">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value21">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value22">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value23">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value24">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value25">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value26">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value27">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value28">
				<div class="square-content">
					<span>422°C</span>
				</div>
			</div>
			<div class="value29">
				<div class="square-content">
					<span>557 mmH20</span>
				</div>
			</div>
			<div class="value30">
				<div class="square-content">
					<span>442°C</span>
				</div>
			</div>
			<div class="value31">
				<div class="square-content">
					<span>26%</span>
				</div>
			</div>
			<div class="value32">
				<div class="square-content">
					<span>442°C</span>
				</div>
			</div>
			<!-- Label  IA dan IB Kiri -->
			<div class="labelia1">
				<div class="square-content">
					<span>IA</span>
				</div>
			</div>
			<div class="labelib1">
				<div class="square-content">
					<span>IB</span>
				</div>
			</div>
			<div class="labelii1">
				<div class="square-content">
					<span>II</span>
				</div>
			</div>
			<div class="labeliii1">
				<div class="square-content">
					<span>III</span>
				</div>
			</div>
			<div class="labeliv1">
				<div class="square-content">
					<span>IV</span>
				</div>
			</div>
			<div class="labelslc1">
				<div class="square-content">
					<span>SLC</span>
				</div>
			</div>
			<!-- Label IA dan IB Kanan -->
			<div class="labelia2">
				<div class="square-content">
					<span>IA</span>
				</div>
			</div>
			<div class="labelib2">
				<div class="square-content">
					<span>IB</span>
				</div>
			</div>
			<div class="labelii2">
				<div class="square-content">
					<span>II</s>
				</div>
			</div>
			<div class="labeliii2">
				<div class="square-content">
					<span>III</span>
				</div>
			</div>
			<div class="labeliv2">
				<div class="square-content">
					<span>IV</span>
				</div>
			</div>
			<div class="labelslc2">
				<div class="square-content">
					<span>SLC</span>
				</div>
			</div>
		</div>
	</div>
</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		var url = 'ISIKAN URL API';
		$.ajax({
			url: url,
			type: 'get',
			// dataType: 'json',
			success: function(data) {
				var data1 = data.replace("<title>Json</title>", "");
				var data2 = data1.replace("(", "[");
				var data3 = data2.replace(");", "]");
				var dataJson = JSON.parse(data3);
				tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
				tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
				tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
				tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
				tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
				tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
				tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
				tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
				$("#isisilo10").html(tonageSilo10);
				$("#isisilo11").html(tonageSilo11);
				$("#isisilo12").html(tonageSilo12);
			}
		});
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>