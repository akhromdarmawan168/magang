<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('finish_mill2_tonasa5.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .labelclinkerstorage {
            position: absolute;
            top: 20%;
            left: 4.7%;
            width: 10%;
            font-weight: bold;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .labelclinkerstorage:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelclinkerstoragevalue {
            position: absolute;
            top: 17%;
            left: 7.7%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.7vw;
        }

        .labelclinkerstoragevalue:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label542CV01 {
            position: absolute;
            top: 31%;
            left: 6.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label542CV01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BE01 {
            position: absolute;
            top: 20%;
            left: 33.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label548BE01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BE01value {
            position: absolute;
            top: 20%;
            left: 37.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548BE01value:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BE02 {
            position: absolute;
            top: 28%;
            left: 29.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label548BE02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BE02value {
            position: absolute;
            top: 28%;
            left: 27%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548BE02value:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label547BI01 {
            position: absolute;
            top: 62%;
            left: 4.3%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label547BI01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BE011 {
            position: absolute;
            top: 62%;
            left: 9.4%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label548BE011:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BI02 {
            position: absolute;
            top: 62%;
            left: 14.5%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label548BI02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BI03 {
            position: absolute;
            top: 62%;
            left: 19.7%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label548BI03:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BI04 {
            position: absolute;
            top: 62%;
            left: 25%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label548BI04:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelfm7 {
            position: absolute;
            top: 74%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: black;

            font-size: 0.6vw;
        }

        .labelfm7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .bg1 {
            position: absolute;
            top: 73%;
            left: 4%;
            width: 4%;
            text-align: center;
            background: white;
            font-size: 0.6vw;

        }

        .bg1:before {
            content: "";
            display: block;
            padding-bottom: 39%;
        }

        .label548BC02 {
            position: absolute;
            top: 73%;
            left: 20%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label548BC02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BC01 {
            position: absolute;
            top: 85%;
            left: 16%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labell548BC01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .label548BI05 {
            position: absolute;
            top: 38%;
            left: 38.4%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label548BI05:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .clinkerqualitytitle {
            position: absolute;
            top: 16%;
            left: 41.3%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .clinkerqualitytitle:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelfl {
            position: absolute;
            top: 20%;
            left: 42.3%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
        }

        .labelfl:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelflvalue {
            position: absolute;
            top: 20%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelflvalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelc3s {
            position: absolute;
            top: 23%;
            left: 42.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labelc3s:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelc3svalue {
            position: absolute;
            top: 20%;
            left: 7.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelc3svalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labeltemp {
            position: absolute;
            top: 26%;
            left: 42.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labeltemp:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labeltempvalue {
            position: absolute;
            top: 20%;
            left: 7.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labeltempvalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .value1 {
            position: absolute;
            top: 11%;
            left: 60.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label548DA02 {
            position: absolute;
            top: 21%;
            left: 69.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label548DA02:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label548DA02value {
            position: absolute;
            top: 15%;
            left: 70%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548DA02value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label588FN09 {
            position: absolute;
            top: 21%;
            left: 74.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label588FN09:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label588FN09value {
            position: absolute;
            top: 9%;
            left: 68%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label588FN09value:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .label548DA04 {
            position: absolute;
            top: 24%;
            left: 80.5%;
            width: 10%;
            text-align: right;
            color: white;
            font-size: 0.6vw;
        }

        .label548DA04:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label548DA04value {
            position: absolute;
            top: 19%;
            left: 80.5%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548DA04value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label568BE01 {
            position: absolute;
            top: 29%;
            left: 82.5%;
            width: 10%;
            text-align: right;
            color: white;
            font-size: 0.6vw;
        }

        .label568BE01:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .label568BE01value {
            position: absolute;
            top: 24%;
            left: 4%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label568BE01value:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .cementsilo13 {
            position: absolute;
            top: 61.4%;
            left: 88%;
            width: 3.4%;
            text-align: right;
            color: black;
            font-size: 0.5vw;
            font-weight: bold;

        }

        .cementsilo13:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo13value {
            position: absolute;
            top: 58%;
            left: 88.7%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .cementsilo13value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo14 {
            position: absolute;
            top: 61.4%;
            left: 94.6%;
            width: 3.4%;
            text-align: right;
            color: black;
            font-size: 0.5vw;
            font-weight: bold;

        }

        .cementsilo14:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo14value {
            position: absolute;
            top: 58%;
            left: 95.4%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .cementsilo14value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo15 {
            position: absolute;
            top: 78.6%;
            left: 88%;
            width: 3.4%;
            text-align: right;
            color: black;
            font-size: 0.5vw;
            font-weight: bold;

        }

        .cementsilo15:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo15value {
            position: absolute;
            top: 75.8%;
            left: 88.7%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .cementsilo15value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .cementsilo16 {
            position: absolute;
            top: 78.6%;
            left: 94.6%;
            width: 3.4%;
            text-align: right;
            color: black;
            font-size: 0.5vw;
            font-weight: bold;

        }

        .cementsilo16:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }


        .cementsilo16value {
            position: absolute;
            top: 75.8%;
            left: 95.4%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .cementsilo16value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label444FN18 {
            position: absolute;
            top: 80.4%;
            left: 76.5%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .label444FN18:before {
            content: "";
            display: block;
            padding-bottom: 10%;

        }

        .label548DA01 {
            position: absolute;
            top: 76.4%;
            left: 72.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .label548DA01:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label548DA01value {
            position: absolute;
            top: 76.4%;
            left: 67%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548DA01value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .labelcementprod {
            position: absolute;
            top: 37%;
            left: 67.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .labelcementprod:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .valuecementprod {
            position: absolute;
            top: 40%;
            left: 64.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .valuecementprod:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value2 {
            position: absolute;
            top: 30.5%;
            left: 67%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value3 {
            position: absolute;
            top: 30.5%;
            left: 58.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value4 {
            position: absolute;
            top: 35%;
            left: 53%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value5 {
            position: absolute;
            top: 51%;
            left: 63%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value6 {
            position: absolute;
            top: 49%;
            left: 54%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value7 {
            position: absolute;
            top: 51%;
            left: 46%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value7:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value8 {
            position: absolute;
            top: 63%;
            left: 42%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value9 {
            position: absolute;
            top: 71%;
            left: 42%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value9:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value10 {
            position: absolute;
            top: 83%;
            left: 40%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value10:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value11 {
            position: absolute;
            top: 71%;
            left: 58%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value11:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .label548HS01 {
            position: absolute;
            top: 63%;
            left: 57.3%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
        }

        .label548HS01:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .label548HS01value {
            position: absolute;
            top: 1%;
            left: 16%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .label548HS01value:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }


        .labelDiffpress {
            position: absolute;
            top: 65%;
            left: 57.3%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
        }

        .labelDiffpress:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelDiffpressvalue {
            position: absolute;
            top: 1%;
            left: 20%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelDiffpressvalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .value12 {
            position: absolute;
            top: 77%;
            left: 59%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .value12:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .labels {
            position: absolute;
            top: 88.4%;
            left: 49.4%;
            width: 10%;
            text-align: left;
            color: black;
            font-size: 0.7vw;
        }

        .labels:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .labelPXPCM8 {
            position: absolute;
            top: 88.4%;
            left: 51%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .labelPXPCM8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .titlePPCFM8 {
            position: absolute;
            top: 85%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .titlePPCFM8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        /* ppc fm 8 */
        .labelBlaine {
            position: absolute;
            top: 89%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
        }

        .labelBlaine:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelBlainevalue {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelBlainevalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelso3 {
            position: absolute;
            top: 91.8%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labelso3:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelso3value {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelso3value:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelMesh {
            position: absolute;
            top: 94%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labelMesh:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelMeshvalue {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelMeshvalue:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }



        /* opc fm 8 */
        .titleOPCFM8 {
            position: absolute;
            top: 85%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .titlePPCFM8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelBlaineopc {
            position: absolute;
            top: 89%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: White;
            font-size: 0.6vw;
        }

        .labelBlaineopc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelBlainevalueopc {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelBlainevalueopc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelso3opc {
            position: absolute;
            top: 91.8%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labelso3opc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelso3valueopc {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelso3valueopc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelMeshopc {
            position: absolute;
            top: 94%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .labelMeshopc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .labelMeshvalueopc {
            position: absolute;
            top: 20%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .labelMeshvalueopc:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }


        /* table bottom left */
        .clinker {
            position: absolute;
            top: 90%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .clinker:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .gypsum {
            position: absolute;
            top: 20%;
            left: 15%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .gypsum:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .trass {
            position: absolute;
            top: 20%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .trass:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .additive {
            position: absolute;
            top: 20%;
            left: 110%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }


        .additive:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .flyash {
            position: absolute;
            top: 20%;
            left: 160%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }


        .flyash:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .total {
            position: absolute;
            top: 20%;
            left: 200%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }


        .total:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* PV */
        .labelpv {
            position: absolute;
            top: 92.1%;
            left: 2%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .labelpv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* value PV */
        .clinkerpv {
            position: absolute;
            top: 92.1%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .clinkerpv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .gypsumpv {
            position: absolute;
            top: 20%;
            left: 15%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .gypsumpv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .trasspv {
            position: absolute;
            top: 20%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .trasspv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .additivepv {
            position: absolute;
            top: 20%;
            left: 110%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .additivepv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .flyashpv {
            position: absolute;
            top: 20%;
            left: 160%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .flyashpv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .totalpv {
            position: absolute;
            top: 20%;
            left: 200%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .totalpv:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* QCX */
        .labelqcx {
            position: absolute;
            top: 94.6%;
            left: 2%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .labelqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* value qcx */
        .clinkerqcx {
            position: absolute;
            top: 94.6%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .clinkerqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .gypsumqcx {
            position: absolute;
            top: 20%;
            left: 15%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .gypsumqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .trassqcx {
            position: absolute;
            top: 20%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .trassqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .additiveqcx {
            position: absolute;
            top: 20%;
            left: 110%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .additiveqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .flyashqcx {
            position: absolute;
            top: 20%;
            left: 160%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .flyashqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .totalqcx {
            position: absolute;
            top: 20%;
            left: 200%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }


        .totalqcx:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .spanbutton1 {
            position: absolute;
            width: 7%;
            height: 7%;
            left: 92%;
            top: 4%;
            font-weight: normal;
            overflow: hidden;
            /* font-size: 10px; */
            font-size: 0.6vw;
            text-align: center;
            color: #42ff00;
        }

        .spanbutton1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .spanbutton2 {
            position: absolute;
            width: 7%;
            height: 7%;
            left: 84%;
            top: 4%;
            font-weight: normal;
            overflow: hidden;
            /* font-size: 10px; */
            font-size: 0.6vw;
            text-align: center;
            color: #42ff00;
        }

        .spanbutton2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }







        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="labelclinkerstorage">
                <div class="square-content">
                    <span>CLINKER STORAGE</span>
                </div>
            </div>
            <div class="labelclinkerstoragevalue">
                <div class="square-content">
                    <span>88 %</span>
                </div>
            </div>
            <div class="label542CV01">
                <div class="square-content">
                    <span>542CV01</span>
                </div>
            </div>
            <div class="label548BE01">
                <div class="square-content">
                    <span>548BE01</span>
                </div>
            </div>
            <div class="label548BE01value">
                <div class="square-content">
                    <span>66 %</span>
                </div>
            </div>
            <div class="label548BE02">
                <div class="square-content">
                    <span>548BE02</span>
                </div>
            </div>
            <div class="label548BE02value">
                <div class="square-content">
                    <span>66 %</span>
                </div>
            </div>
            <div class="label547BI01">
                <div class="square-content">
                    <span>548BE01</span>
                </div>
            </div>
            <div class="label548BE011">
                <div class="square-content">
                    <span>548BE01</span>
                </div>
            </div>
            <div class="label548BI02">
                <div class="square-content">
                    <span>548BI02</span>
                </div>
            </div>
            <div class="label548BI03">
                <div class="square-content">
                    <span>548BI03</span>
                </div>
            </div>
            <div class="label548BI04">
                <div class="square-content">
                    <span>548BI04</span>
                </div>
            </div>
            <div class="labelfm7">
                <div class="square-content">
                    <span>FM 7</span>
                </div>
            </div>
            <div class="label548BC02">
                <div class="square-content">
                    <span>548BC02</span>
                </div>
            </div>
            <div class="label548BC01">
                <div class="square-content">
                    <span>548BC01</span>
                </div>
            </div>
            <div class="label548BI05">
                <div class="square-content">
                    <span>548BI05</span>
                </div>
            </div>
            <div class="clinkerqualitytitle">
                <div class="square-content">
                    <span>CLINKER QUALITY</span>
                </div>
            </div>
            <!--  -->
            <div class="labelfl">
                <div class="square-content">
                    <span>FL</span>
                    <div class="labelflvalue">
                        <span>1,33</span>
                    </div>
                </div>
            </div>
            <div class="labelc3s">
                <div class="square-content">
                    <span>C3S</span>
                    <div class="labelc3svalue">
                        <span>57,50</span>
                    </div>
                </div>
            </div>
            <div class="labeltemp">
                <div class="square-content">
                    <span>TEMP</span>
                    <div class="labeltempvalue">
                        <span>87,0</span>
                    </div>
                </div>
            </div>
            <!--  -->
            <div class="value1">
                <div class="square-content">
                    <span>17 mbar</span>
                </div>
            </div>
            <div class="label548DA02">
                <div class="square-content">
                    <span>548DA02</span>
                </div>
            </div>
            <div class="label548DA02value">
                <div class="square-content">
                    <span>94,6 %</span>
                </div>
            </div>
            <div class="label588FN09">
                <div class="square-content">
                    <span>588FN09</span>
                </div>
            </div>
            <div class="label588FN09value">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">85 A</p>
                    <p style="margin-bottom: 1px;">1920 Kw</p>
                    <p style="margin-bottom: 1px;">935 rpm</p>
                </div>
            </div>
            <div class=" label548DA04">
                <div class="square-content">
                    <span>548DA04</span>
                </div>
            </div>
            <div class="label548DA04value">
                <div class="square-content">
                    <span>97,2 %</span>
                </div>
            </div>
            <div class="label568BE01">
                <div class="square-content">
                    <span>568BE01</span>
                    <div class="label568BE01value">
                        <span>48,4 %</span>
                    </div>
                </div>
            </div>
            <div class="cementsilo13">
                <div class="square-content">
                    <span>CEMENT SILO 13</span>
                </div>
            </div>
            <div class="cementsilo13value">
                <div class="square-content">
                    <span>88 %</span>
                </div>
            </div>
            <div class="cementsilo14">
                <div class="square-content">
                    <span>CEMENT SILO 14</span>
                </div>
            </div>
            <div class="cementsilo14value">
                <div class="square-content">
                    <span>98 %</span>
                </div>
            </div>
            <div class="cementsilo15">
                <div class="square-content">
                    <span>CEMENT SILO 15</span>
                </div>
            </div>
            <div class="cementsilo15value">
                <div class="square-content">
                    <span>90 %</span>
                </div>
            </div>
            <div class="cementsilo16">
                <div class="square-content">
                    <span>CEMENT SILO 16</span>
                </div>
            </div>
            <div class="cementsilo16value">
                <div class="square-content">
                    <span>90 %</span>
                </div>
            </div>
            <div class="label444FN18">
                <div class="square-content">
                    <span>444FN18</span>
                </div>
            </div>
            <div class="label548DA01">
                <div class="square-content">
                    <span>548DA01</span>
                </div>
            </div>
            <div class="label548DA01value">
                <div class="square-content">
                    <span>96,0 %</span>
                </div>
            </div>

            <div class="labelcementprod">
                <div class="square-content">
                    <span>CEMENT PROD.</span>
                </div>
            </div>
            <div class="valuecementprod">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">8417 ton</p>
                    <p style="margin-bottom: 1px;">8344 ton</p>
                </div>
            </div>
            <div class="value2">
                <div class="square-content">
                    <span>56 %</span>
                </div>
            </div>
            <div class="value3">
                <div class="square-content">
                    <span>49 %</span>
                </div>
            </div>
            <div class="value4">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">87,71 °C</p>
                    <p style="margin-bottom: 1px;">-57,1 mbar</p>
                </div>
            </div>
            <div class="value5">
                <div class="square-content">
                    <span>5741 lit/h</span>
                </div>
            </div>
            <div class="value6">
                <div class="square-content">
                    <span>63,75 %</span>
                </div>
            </div>
            <div class="value7">
                <div class="square-content">
                    <span>217 t/h</span>
                </div>
            </div>
            <div class="value8">
                <div class="square-content">
                    <span>27,7 kWh/ton </span>
                </div>
            </div>
            <div class="value9">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">5,5 mm/s</p>
                    <p style="margin-bottom: 1px;">7,2 mm/s</p>
                </div>
            </div>
            <div class="value10">
                <div class="square-content">
                    <span>0,51 t/h</span>
                </div>
            </div>
            <div class="value11">
                <div class="square-content">
                    <span>101 °C</span>
                </div>
            </div>
            <div class="label548HS01">
                <div class="square-content">
                    <span>548HS01 :</span>
                    <div class="label548HS01value">
                        <span>94,8 bar</span>
                    </div>
                </div>
            </div>
            <div class="labelDiffpress">
                <div class="square-content">
                    <span>Diff. press. :</span>
                    <div class="labelDiffpressvalue">
                        <span>42,6 mbar</span>
                    </div>
                </div>
            </div>
            <div class="value12">
                <div class="square-content">
                    <span>2198 KW</span>
                </div>
            </div>
            <div class="labels">
                <div class="square-content">
                    <span>S</span>
                </div>
            </div>
            <div class="labelPXPCM8">
                <div class="square-content">
                    <span>PXP CM 8</span>
                </div>
            </div>
            <div class="titlePPCFM8">
                <div class="square-content">
                    <span>PPC FM 8</span>
                </div>
            </div>
            <!-- ppc fm 8 -->
            <div class="labelBlaine">
                <div class="square-content">
                    <span>Blaine</span>
                    <div class="labelBlainevalue">
                        <span>3353</span>
                    </div>
                </div>
            </div>
            <div class="labelso3">
                <div class="square-content">
                    <span>SO3</span>
                    <div class="labelso3value">
                        <span>1,71</span>
                    </div>
                </div>
            </div>
            <div class="labelMesh">
                <div class="square-content">
                    <span>Mesh (% Res)</span>
                    <div class="labelMeshvalue">
                        <span>12,16</span>
                    </div>
                </div>
            </div>
            <!-- opc fm 8 -->

            <div class="titleOPCFM8">
                <div class="square-content">
                    <span>OPC FM 8</span>
                </div>
            </div>
            <div class="labelBlaineopc">
                <div class="square-content">
                    <span>Blaine</span>
                    <div class="labelBlainevalueopc">
                        <span>3353</span>
                    </div>
                </div>
            </div>
            <div class="labelso3opc">
                <div class="square-content">
                    <span>SO3</span>
                    <div class="labelso3valueopc">
                        <span>1,71</span>
                    </div>
                </div>
            </div>
            <div class="labelMeshopc">
                <div class="square-content">
                    <span>Mesh (% Res)</span>
                    <div class="labelMeshvalueopc">
                        <span>12,16</span>
                    </div>
                </div>
            </div>
            <!-- table bottom left -->
            <div class="clinker">
                <div class="square-content">
                    <span>Clinker</span>
                    <div class="gypsum">
                        <span>Gypsum</span>
                    </div>
                    <div class="trass">
                        <span>Trass</span>
                    </div>
                    <div class="additive">
                        <span>Additive</span>
                    </div>
                    <div class="flyash">
                        <span>fly Ash</span>
                    </div>
                    <div class="total">
                        <span>Total</span>
                    </div>
                </div>
            </div>

            <!-- Pv -->
            <div class="labelpv">
                <div class="square-content">
                    <span>PV</span>
                </div>
            </div>
            <!-- value PV -->
            <div class="clinkerpv">
                <div class="square-content">
                    <span>154,6 t/h</span>
                    <div class="gypsumpv">
                        <span>8,6 t/h</span>
                    </div>
                    <div class="trasspv">
                        <span>36,5 t/h</span>
                    </div>
                    <div class="additivepv">
                        <span>10,7 t/h</span>
                    </div>
                    <div class="flyashpv">
                        <span>4,0 t/h</span>
                    </div>
                    <div class="totalpv">
                        <span>197 t/h</span>
                    </div>
                </div>
            </div>

            <!-- QCX -->
            <div class="labelqcx">
                <div class="square-content">
                    <span>QCX</span>
                </div>
            </div>
            <!-- value qcx -->
            <div class="clinkerqcx">
                <div class="square-content">
                    <span>72,0 %</span>
                    <div class="gypsumqcx">
                        <span>4,0 %</span>
                    </div>
                    <div class="trassqcx">
                        <span>17,0 %</span>
                    </div>
                    <div class="additiveqcx">
                        <span>5,0 %</span>
                    </div>
                    <div class="flyashqcx">
                        <span>2,0 %</span>
                    </div>
                    <div class="totalqcx">
                        <span></span>
                    </div>
                </div>
            </div>
            <!-- Button -->
            <div class="spanButton2">
                <div class="square-content">
                    <a href="">
                        <button type="button" class="btn btn-default" style="width: 100%;">FM 7</button></a>
                </div>
            </div>
            <div class="spanButton1">
                <div class="square-content">
                    <a href="">
                        <button disabled="" type="button" class="btn btn-default" style="width: 100%;">FM 8</button></a>
                </div>
            </div>
            <div class="bg1">
                <div class='square-content'>
                    <div>
                        <span>
                            <a href="">FM</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        //     url: url,
        //     type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        //     });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>


</html>