<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('finish mill 1.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }


        .span1 {
            position: absolute;
            top: 8.9%;
            left: -0.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span2 {
            position: absolute;
            top: 8%;
            left: 12.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span3 {
            position: absolute;
            top: 29.6%;
            left: 1.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span4 {
            position: absolute;
            top: 29.6%;
            left: 9.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span5 {
            position: absolute;
            top: 29.6%;
            left: 18.1%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span6 {
            position: absolute;
            top: 29.6%;
            left: 26.1%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span7 {
            position: absolute;
            top: 68.6%;
            left: 34.1%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span8 {
            position: absolute;
            top: 68.6%;
            left: 46.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span9 {
            position: absolute;
            top: 12.6%;
            left: 24.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span10 {
            position: absolute;
            top: 15.6%;
            left: 22.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span11 {
            position: absolute;
            top: 17.6%;
            left: 22.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span12 {
            position: absolute;
            top: 19.6%;
            left: 22.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span13 {
            position: absolute;
            top: 15.6%;
            left: 28.7%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span14 {
            position: absolute;
            top: 17.6%;
            left: 28.9%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span15 {
            position: absolute;
            top: 19.6%;
            left: 28.7%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span16 {
            position: absolute;
            top: 15.6%;
            left: 34.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span17 {
            position: absolute;
            top: 17.6%;
            left: 34.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span18 {
            position: absolute;
            top: 19.6%;
            left: 35.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span19 {
            position: absolute;
            top: 15.6%;
            left: 43.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span20 {
            position: absolute;
            top: 17.6%;
            left: 43.1%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span21 {
            position: absolute;
            top: 19.6%;
            left: 43.1%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span22 {
            position: absolute;
            top: 28.5%;
            left: 49.8%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span23 {
            position: absolute;
            top: 73.4%;
            left: 2.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span24 {
            position: absolute;
            top: 77.4%;
            left: 2.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span25 {
            position: absolute;
            top: 79.4%;
            left: 2.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span26 {
            position: absolute;
            top: 85.4%;
            left: 2.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span27 {
            position: absolute;
            top: 87.4%;
            left: 2.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span28 {
            position: absolute;
            top: 73.4%;
            left: 8.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span29 {
            position: absolute;
            top: 77.4%;
            left: 8.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span30 {
            position: absolute;
            top: 79.4%;
            left: 8.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span31 {
            position: absolute;
            top: 85.4%;
            left: 8.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span32 {
            position: absolute;
            top: 87.4%;
            left: 8.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span33 {
            position: absolute;
            top: 73.4%;
            left: 14.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span34 {
            position: absolute;
            top: 77.4%;
            left: 14.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span35 {
            position: absolute;
            top: 79.4%;
            left: 14.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span36 {
            position: absolute;
            top: 85.4%;
            left: 14.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span37 {
            position: absolute;
            top: 87.4%;
            left: 14.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span38 {
            position: absolute;
            top: 73.4%;
            left: 20.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span39 {
            position: absolute;
            top: 77.4%;
            left: 20.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span40 {
            position: absolute;
            top: 79.4%;
            left: 20.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span41 {
            position: absolute;
            top: 85.4%;
            left: 20.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span42 {
            position: absolute;
            top: 87.4%;
            left: 20.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span43 {
            position: absolute;
            top: 73.4%;
            left: 25.6%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span44 {
            position: absolute;
            top: 77.4%;
            left: 26.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span45 {
            position: absolute;
            top: 79.4%;
            left: 26.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span46 {
            position: absolute;
            top: 85.4%;
            left: 26.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span47 {
            position: absolute;
            top: 87.4%;
            left: 26.1%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span48 {
            position: absolute;
            top: 83.4%;
            left: 79.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span49 {
            position: absolute;
            top: 86.4%;
            left: 79.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span50 {
            position: absolute;
            top: 88.4%;
            left: 79.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span51 {
            position: absolute;
            top: 90.4%;
            left: 79.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span52 {
            position: absolute;
            top: 86.4%;
            left: 76.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span53 {
            position: absolute;
            top: 88.4%;
            left: 76.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span54 {
            position: absolute;
            top: 90.4%;
            left: 76.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span55 {
            position: absolute;
            top: 86.4%;
            left: 88.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span56 {
            position: absolute;
            top: 88.4%;
            left: 88.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span57 {
            position: absolute;
            top: 90.4%;
            left: 88.1%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span58 {
            position: absolute;
            top: 90.4%;
            left: 85.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span59 {
            position: absolute;
            top: 88.4%;
            left: 85.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span60 {
            position: absolute;
            top: 86.4%;
            left: 85.1%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span61 {
            position: absolute;
            top: 27.6%;
            left: -1.9%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span62 {
            position: absolute;
            top: 27.6%;
            left: 6.1%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span63 {
            position: absolute;
            top: 27.6%;
            left: 14.1%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span64 {
            position: absolute;
            top: 27.6%;
            left: 23.4%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span65 {
            position: absolute;
            top: 33%;
            left: 22.4%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span66 {
            position: absolute;
            top: 33%;
            left: 14%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span67 {
            position: absolute;
            top: 33%;
            left: 5.6%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span68 {
            position: absolute;
            top: 33%;
            left: -2.4%;
            width: 10%;
            text-align: right;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span69 {
            position: absolute;
            top: 50%;
            left: -3.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span70 {
            position: absolute;
            top: 65%;
            left: 19.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span71 {
            position: absolute;
            top: 56%;
            left: 24.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span72 {
            position: absolute;
            top: 59.5%;
            left: 40.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span73 {
            position: absolute;
            top: 57.5%;
            left: 40.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span73:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span74 {
            position: absolute;
            top: 77.5%;
            left: 40.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span74:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span75 {
            position: absolute;
            top: 79.5%;
            left: 40.4%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span75:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span76 {
            position: absolute;
            top: 62%;
            left: 42.4%;
            width: 10%;
            text-align: right;
            color: white;
            font-size: 0.6vw;
        }

        .span76:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span77 {
            position: absolute;
            top: 74%;
            left: 42.4%;
            width: 10%;
            text-align: right;
            color: white;
            font-size: 0.6vw;
        }

        .span77:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span78 {
            position: absolute;
            top: 62%;
            left: 55%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span78:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span79 {
            position: absolute;
            top: 68.5%;
            left: 55.5%;
            width: 10%;
            text-align: right;
            color: red;
            font-size: 0.6vw;
        }

        .span79:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span80 {
            position: absolute;
            top: 75.5%;
            left: 53.5%;
            width: 10%;
            text-align: right;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span80:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span81 {
            position: absolute;
            top: 31%;
            left: 32.5%;
            width: 10%;
            text-align: right;
            color: white;
            font-size: 0.6vw;
        }

        .span81:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span82 {
            position: absolute;
            top: 34.5%;
            left: 39.6%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span82:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span83 {
            position: absolute;
            top: 42.5%;
            left: 39.6%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span83:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span84 {
            position: absolute;
            top: 32%;
            left: 45%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span84:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span85 {
            position: absolute;
            top: 28%;
            left: 47%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.6vw;

        }

        .span85:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span86 {
            position: absolute;
            top: 33%;
            left: 53.8%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span86:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span87 {
            position: absolute;
            top: 44.6%;
            left: 52.5%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span87:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span88 {
            position: absolute;
            top: 50.6%;
            left: 50%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span88:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span89 {
            position: absolute;
            top: 19.6%;
            left: 54%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;


        }

        .span89:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span90 {
            position: absolute;
            top: 13.6%;
            left: 55%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;

        }

        .span90:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span91 {
            position: absolute;
            top: 11%;
            left: 58.5%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;

        }

        .span91:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span92 {
            position: absolute;
            top: 35%;
            left: 68%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span92:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span93 {
            position: absolute;
            top: 38%;
            left: 68%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span93:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span94 {
            position: absolute;
            top: 13%;
            left: 75%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.6vw;
        }

        .span94:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span95 {
            position: absolute;
            top: 17.5%;
            left: 88%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span95:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span96 {
            position: absolute;
            top: 17.5%;
            left: 91%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span96:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span97 {
            position: absolute;
            top: 12.5%;
            left: 88%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span97:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span98 {
            position: absolute;
            top: 10.5%;
            left: 79%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span98:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span99 {
            position: absolute;
            top: 10.5%;
            left: 82%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span99:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span100 {
            position: absolute;
            top: 10.5%;
            left: 85%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span100:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span101 {
            position: absolute;
            top: 8.5%;
            left: 85%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span101:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span102 {
            position: absolute;
            top: 8.5%;
            left: 82%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span102:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span103 {
            position: absolute;
            top: 8.5%;
            left: 79%;
            width: 10%;
            text-align: left;
            color: #7cff00;
            font-size: 0.6vw;
        }

        .span103:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span104 {
            position: absolute;
            top: 30.5%;
            left: 79.5%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span104:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span105 {
            position: absolute;
            top: 51%;
            left: 90.5%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span105:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span106 {
            position: absolute;
            top: 51%;
            left: 88%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.6vw;
        }

        .span106:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span107 {
            position: absolute;
            top: 90.5%;
            left: 73.5%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span107:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span108 {
            position: absolute;
            top: 92.5%;
            left: 73.9%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.6vw;
        }

        .span108:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">

            <div class="span1">
                <div class="square-content">
                    <span>74,59%</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>60.75%</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>284.18 t</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>89.60 t</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>39.19 t</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>t</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>98 %</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>33 %</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>FINISH MILL PROD.</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>Clinker</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>Gypsum </span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>Trass</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>6392,25 ton</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>131,69 ton</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>1594,72 ton</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>Limestone</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>Prod Today</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>Prod Yesterday</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>51,82 ton</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>8417,72 ton</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>8502,92 ton</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>DA04</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>CM1 FEED</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>103 t/h</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>350 t/h</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>MANUAL SP</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>RMP SP</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>CLINKER</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>103 t/h</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>82,2 %</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>82,2 %</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>82,2 %</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>GYPSUM</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>103 t/h</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>11,3 %</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>11,3 %</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>11,3 %</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>TRASS</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>1,2 t/h</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>1,2 %</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>1,2 %</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>1,2 %</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>LIMESTONE</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>5,3 t/h</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>3,2 %</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>3,2 %</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>3,2 %</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>CEMENT QUALITY</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>FL</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>Blaine</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>Res.</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>1,2 %</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>2,5cm2/gr</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>1,5%</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>SO3</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>C3S</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>Temp</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>112°C</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>3,5 %</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>13,5 %</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>CLINKER</span>
                </div>
            </div>
            <div class="span62">
                <div class="square-content">
                    <span>GYPSUM</span>
                </div>
            </div>
            <div class="span63">
                <div class="square-content">
                    <span>TRASS</span>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>LIMESTONE</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <span>FB04</span>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>FB03</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <span>FB02</span>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>FB01</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>13,5 %</span>
                </div>
            </div>
            <div class="span70">
                <div class="square-content">
                    <span>0,00 mbar</span>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>13,44 t/h</span>
                </div>
            </div>
            <div class="span72">
                <div class="square-content">
                    <span>23,77 hours</span>
                </div>
            </div>
            <div class="span73">
                <div class="square-content">
                    <span>32807 kW</span>
                </div>
            </div>
            <div class="span74">
                <div class="square-content">
                    <span>32807 kW</span>
                </div>
            </div>
            <div class="span75">
                <div class="square-content">
                    <span>23,77 hours</span>
                </div>
            </div>
            <div class="span76">
                <div class="square-content">
                    <span>MD11</span>
                </div>
            </div>
            <div class="span77">
                <div class="square-content">
                    <span>MD01</span>
                </div>
            </div>
            <div class="span78">
                <div class="square-content">
                    <span>-2,29 mbar</span>
                </div>
            </div>
            <div class="span79">
                <div class="square-content">
                    <span>xx m3/s</span>
                </div>
            </div>
            <div class="span80">
                <div class="square-content">
                    <span>99°C</span>
                </div>
            </div>
            <div class="span81">
                <div class="square-content">
                    <span>419FA09</span>
                </div>
            </div>
            <div class="span82">
                <div class="square-content">
                    <span>1,5%</span>
                </div>
            </div>
            <div class="span83">
                <div class="square-content">
                    <span>DA05</span>
                </div>
            </div>
            <div class="span84">
                <div class="square-content">
                    <span>DC02</span>
                </div>
            </div>
            <div class="span85">
                <div class="square-content">
                    <span>99°C</span>
                </div>
            </div>
            <div class="span86">
                <div class="square-content">
                    <span>DA06</span>
                </div>
            </div>
            <div class="span87">
                <div class="square-content">
                    <span>SC05</span>
                </div>
            </div>
            <div class="span88">
                <div class="square-content">
                    <span>420AS03</span>
                </div>
            </div>
            <div class="span89">
                <div class="square-content">
                    <span>419FA07</span>
                </div>
            </div>
            <div class="span90">
                <div class="square-content">
                    <span>DA03</span>
                </div>
            </div>
            <div class="span91">
                <div class="square-content">
                    <span>1,5 %</span>
                </div>
            </div>
            <div class="span92">
                <div class="square-content">
                    <span>72,5 %</span>
                </div>
            </div>
            <div class="span93">
                <div class="square-content">
                    <span>50,8 A</span>
                </div>
            </div>
            <div class="span94">
                <div class="square-content">
                    <span>99°C</span>
                </div>
            </div>
            <div class="span95">
                <div class="square-content">
                    <span>DA04</span>
                </div>
            </div>
            <div class="span96">
                <div class="square-content">
                    <span>46,07 %</span>
                </div>
            </div>
            <div class="span97">
                <div class="square-content">
                    <span>1,5 %</span>
                </div>
            </div>
            <div class="span98">
                <div class="square-content">
                    <span>83,40 kV</span>
                </div>
            </div>
            <div class="span99">
                <div class="square-content">
                    <span>75,40 kV</span>
                </div>
            </div>
            <div class="span100">
                <div class="square-content">
                    <span>0,00 kV</span>
                </div>
            </div>
            <div class="span101">
                <div class="square-content">
                    <span>0,16 mA</span>
                </div>
            </div>
            <div class="span102">
                <div class="square-content">
                    <span>92,80 mA</span>
                </div>
            </div>
            <div class="span103">
                <div class="square-content">
                    <span>40,19 mA</span>
                </div>
            </div>
            <div class="span104">
                <div class="square-content">
                    <span>419AS03</span>
                </div>
            </div>
            <div class="span105">
                <div class="square-content">
                    <span>421BE01</span>
                </div>
            </div>
            <div class="span106">
                <div class="square-content">
                    <span>50,8 A</span>
                </div>
            </div>
            <div class="span107">
                <div class="square-content">
                    <span>419BE01</span>
                </div>
            </div>
            <div class="span108">
                <div class="square-content">
                    <span>50,8 A</span>
                </div>
            </div>

        </div>
    </div>

</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>