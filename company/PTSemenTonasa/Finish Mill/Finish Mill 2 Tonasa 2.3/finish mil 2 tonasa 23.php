<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('finish mil 2 tonasa 23.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;

        }

        .span1 {
            position: absolute;
            top: 16.5%;
            left: 4.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span2 {
            position: absolute;
            top: 17%;
            left: 18.7%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span3 {
            position: absolute;
            top: 33.5%;
            left: 7.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }




        .span4 {
            position: absolute;
            top: 49.5%;
            left: 8.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span5 {
            position: absolute;
            top: 28.5%;
            left: 55.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span6 {
            position: absolute;
            top: 28.5%;
            left: 51.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span7 {
            position: absolute;
            top: 28.5%;
            left: 38.7%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span8 {
            position: absolute;
            top: 28.5%;
            left: 34.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span9 {
            position: absolute;
            top: 27%;
            left: 45%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span10 {
            position: absolute;
            top: 19%;
            left: 44%;
            width: 3%;
            text-align: center;
            color: #fff;
            font-size: 0.7vw;
        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span11 {
            position: absolute;
            top: 18.5%;
            left: 48%;
            width: 3%;
            text-align: center;
            color: #fff;
            font-size: 0.7vw;
        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span12 {
            position: absolute;
            top: 18.5%;
            left: 51.5%;
            width: 3%;
            text-align: center;
            color: #fff;
            font-size: 0.7vw;
        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span13 {
            position: absolute;
            top: 36.5%;
            left: 54%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span14 {
            position: absolute;
            top: 36.5%;
            left: 50%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span15 {
            position: absolute;
            top: 36.5%;
            left: 37.2%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span16 {
            position: absolute;
            top: 36.5%;
            left: 33.2%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span17 {
            position: absolute;
            top: 38.5%;
            left: 31.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span18 {
            position: absolute;
            top: 42.5%;
            left: 31.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span19 {
            position: absolute;
            top: 45%;
            left: 36.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span20 {
            position: absolute;
            top: 45%;
            left: 53.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span21 {
            position: absolute;
            top: 50.5%;
            left: 57.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span22 {
            position: absolute;
            top: 56%;
            left: 19.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span23 {
            position: absolute;
            top: 56%;
            left: 28.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span24 {
            position: absolute;
            top: 56%;
            left: 38.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span25 {
            position: absolute;
            top: 58%;
            left: 42.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span26 {
            position: absolute;
            top: 60%;
            left: 25.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span27 {
            position: absolute;
            top: 64%;
            left: 30%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span28 {
            position: absolute;
            top: 74%;
            left: 18%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span29 {
            position: absolute;
            top: 82.5%;
            left: 25.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span30 {
            position: absolute;
            top: 84%;
            left: 28.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span31 {
            position: absolute;
            top: 81%;
            left: 31.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span32 {
            position: absolute;
            top: 70.5%;
            left: 31%;
            width: 8%;
            FONT-WEIGHT: bold;
            text-align: center;
            color: black;
            font-size: 0.8vw;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span33 {
            position: absolute;
            top: 87.2%;
            left: 33.8%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span34 {
            position: absolute;
            top: 87.2%;
            left: 36.2%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span35 {
            position: absolute;
            top: 87.2%;
            left: 38.6%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span36 {
            position: absolute;
            top: 87.2%;
            left: 41%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span37 {
            position: absolute;
            top: 89.2%;
            left: 37.2%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span38 {
            position: absolute;
            top: 88.5%;
            left: 47.2%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span39 {
            position: absolute;
            top: 83%;
            left: 47.7%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span40 {
            position: absolute;
            top: 10.5%;
            left: 62.7%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span41 {
            position: absolute;
            top: 15.5%;
            left: 71.2%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span42 {
            position: absolute;
            top: 17.5%;
            left: 83%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span43 {
            position: absolute;
            top: 17.5%;
            left: 87.2%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span44 {
            position: absolute;
            top: 23.5%;
            left: 74%;
            width: 8%;
            font-weight: bold;
            text-align: center;
            color: #101010;
            font-size: 0.7vw;

        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span45 {
            position: absolute;
            top: 28.5%;
            left: 69.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span46 {
            position: absolute;
            top: 32.5%;
            left: 69.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span47 {
            position: absolute;
            top: 33.7%;
            left: 93.8%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span48 {
            position: absolute;
            top: 36.6%;
            left: 81.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span49 {
            position: absolute;
            top: 35.3%;
            left: 90.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span50 {
            position: absolute;
            top: 46%;
            left: 84.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span51 {
            position: absolute;
            top: 47.5%;
            left: 96.5%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span52 {
            position: absolute;
            top: 51.5%;
            left: 67%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span53 {
            position: absolute;
            top: 53.5%;
            left: 71%;
            width: 8%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;

        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span54 {
            position: absolute;
            top: 57.5%;
            left: 65%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span55 {
            position: absolute;
            top: 67.5%;
            left: 65%;
            width: 8%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span56 {
            position: absolute;
            top: 70%;
            left: 65%;
            width: 6%;
            background: white;
            text-align: center;
            color: #333;
            font-weight: bold;
            font-size: 0.7vw;

        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span57 {
            position: absolute;
            top: 70%;
            left: 79%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span58 {
            position: absolute;
            top: 72%;
            left: 87%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span59 {
            position: absolute;
            top: 79.5%;
            left: 68%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span60 {
            position: absolute;
            top: 79.8%;
            left: 76%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span61 {
            position: absolute;
            top: 79.8%;
            left: 80%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span62 {
            position: absolute;
            top: 79.8%;
            left: 84.7%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span63 {
            position: absolute;
            top: 79.8%;
            left: 88.9%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span64 {
            position: absolute;
            top: 79.5%;
            left: 96.5%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.7vw;

        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span65 {
            position: absolute;
            top: 87.5%;
            left: 72.5%;
            width: 6%;
            font-weight: bold;
            text-align: center;
            color: black;
            font-size: 0.8vw;

        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span66 {
            position: absolute;
            top: 87.5%;
            left: 79.4%;
            width: 6%;
            font-weight: bold;
            text-align: center;
            color: black;
            font-size: 0.8vw;

        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span67 {
            position: absolute;
            top: 87.5%;
            left: 86.3%;
            width: 6%;
            font-weight: bold;
            text-align: center;
            color: black;
            font-size: 0.8vw;

        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span68 {
            position: absolute;
            top: 87.5%;
            left: 93.3%;
            width: 6%;
            font-weight: bold;
            text-align: center;
            color: black;
            font-size: 0.8vw;

        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span69 {
            position: absolute;
            top: 94.5%;
            left: 72.3%;
            width: 6%;
            text-align: center;
            color: #7cff00;
            font-size: 0.7vw;

        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span70 {
            position: absolute;
            top: 94.5%;
            left: 79%;
            width: 6%;
            text-align: center;
            color: #7cff00;
            font-size: 0.7vw
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span71 {
            position: absolute;
            top: 94.5%;
            left: 85.9%;
            width: 6%;
            text-align: center;
            color: #7cff00;
            font-size: 0.7vw;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span72 {
            position: absolute;
            top: 94.5%;
            left: 92.9%;
            width: 6%;
            text-align: center;
            color: #7cff00;
            font-size: 0.7vw;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>GYPSUM 02</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>GYPSUM 03</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>LIMESTONE 04</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>TRASS 01</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>1</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>2</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>3</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>4</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>17</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>30</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>31</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>32</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>33</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>18</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>22</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>22</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>14</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>26</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>M11/A/B</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>19</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>23</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>M11</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>M9</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>M12/A/B</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>M12</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>M10</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>M13</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>CEMENT MILL 2 08</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>M15</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>09</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>08</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>13</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>29</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>27</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>27</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>22106M2</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>28</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>22101</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>22101M4</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>22106</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>35</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>34</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>34</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>FLY ASH Z3</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>FROM Z3</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>24</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>3210204</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>23035</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>22103</span>
                </div>
            </div>
            <div class="span62">
                <div class="square-content">
                    <span>22102</span>
                </div>
            </div>
            <div class="span63">
                <div class="square-content">
                    <span>3210201</span>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>22306</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <span>4</span>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>3</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <span>2</span>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>1</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>0.0 m</span>
                </div>
            </div>
            <div class="span70">
                <div class="square-content">
                    <span>7.0 m</span>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>0.6 m</span>
                </div>
            </div>
            <div class="span72">
                <div class="square-content">
                    <span>3.0 m</span>
                </div>
            </div>
        </div>
    </div>

</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>