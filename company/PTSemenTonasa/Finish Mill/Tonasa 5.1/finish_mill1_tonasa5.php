<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('finish_mill1_tonasa5.png');

		}

		.pinch:before {
			content: "";
			display: block;
			padding-bottom: 54.5%;
		}

		.labelClinkerStorage {
			position: absolute;
			top: 20.4%;
			left: 3.6%;
			width: 6.4%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.5vw;
		}

		.labelClinkerStorage:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerStorageValue {
			position: absolute;
			top: -114.6%;
			left: -4%;
			width: 29.4%;
			font-weight: bold;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.labelClinkerStorageValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label551AC05 {
			position: absolute;
			top: 30%;
			left: 4.6%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label551AC05:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinker1 {
			position: absolute;
			top: 60.9%;
			left: 2.1%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinker1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label548BI01 {
			position: absolute;
			top: -24%;
			left: -45%;
			width: 29.4%;
			text-align: center;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
		}

		.label548BI01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinker2 {
			position: absolute;
			top: 61.3%;
			left: 7.4%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinker2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BI01 {
			position: absolute;
			top: -24%;
			left: -45%;
			width: 29.4%;
			text-align: center;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
		}

		.label552BI01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelGypsum {
			position: absolute;
			top: 61.3%;
			left: 12.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelGypsum:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BI02 {
			position: absolute;
			top: -24%;
			left: -45%;
			width: 29.4%;
			text-align: center;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
		}

		.label552BI02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.bg1 {
			position: absolute;
			top: 73.6%;
			left: 1.1%;
			width: 4.4%;
			text-align: center;
			background: white;
			font-size: 0.6vw;

		}

		.bg1:before {
			content: "";
			display: block;
			padding-bottom: 39%;
		}

		.labelTrass {
			position: absolute;
			top: 61.3%;
			left: 18%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelTrass:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BI03 {
			position: absolute;
			top: -24%;
			left: -46%;
			width: 29.4%;
			text-align: center;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
		}

		.label552BI03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelAdditives {
			position: absolute;
			top: 61.3%;
			left: 22.6%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelAdditives:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BI04 {
			position: absolute;
			top: -24%;
			left: -44%;
			width: 29.4%;
			text-align: center;
			font-weight: bold;
			color: black;
			font-size: 0.6vw;
		}

		.label552BI04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelFM2 {
			position: absolute;
			top: 74%;
			left: 2.8%;
			width: 29.4%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labelFM2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BC01 {
			position: absolute;
			top: 72%;
			left: 15%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552BC01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BC02 {
			position: absolute;
			top: 74.07%;
			left: 18.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.label552BC02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BC03 {
			position: absolute;
			top: 77.5%;
			left: 20.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552BC03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label547BE01 {
			position: absolute;
			top: 21%;
			left: 33.7%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label547BE01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label547BE01Value {
			position: absolute;
			top: -37%;
			left: -34%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.label547BE01Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelPv {
			position: absolute;
			top: 92%;
			left: 1.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelPv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelQck {
			position: absolute;
			top: -19%;
			left: -47.4%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelQck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinker3 {
			position: absolute;
			top: 89.5%;
			left: 4.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinker3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinker3Pv {
			position: absolute;
			top: -18.5%;
			left: -46.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelClinker3Pv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinker3Qck {
			position: absolute;
			top: 2.5%;
			left: -45.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelClinker3Qck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelGypsum2 {
			position: absolute;
			top: -41%;
			left: -28.4%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelGypsum2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelGypsum2Pv {
			position: absolute;
			top: 22%;
			left: 0.6%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelGypsum2Pv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelGypsum2Qck {
			position: absolute;
			top: 44%;
			left: 0.6%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelGypsum2Qck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelTrass2 {
			position: absolute;
			top: -41%;
			left: -11%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelTrass2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelTrass2Pv {
			position: absolute;
			top: 23%;
			left: 0%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelTrass2Pv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelTrass2Qck {
			position: absolute;
			top: 45%;
			left: 0%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelTrass2Qck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelAdditives2 {
			position: absolute;
			top: -42%;
			left: 5.6%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelAdditives2Pv {
			position: absolute;
			top: 24%;
			left: -0.4%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}


		.labelAdditives2Pv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelAdditives2Qck {
			position: absolute;
			top: 46%;
			left: 0.6%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}


		.labelAdditives2Qck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelFlyAsh {
			position: absolute;
			top: -40%;
			left: 24%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelFlyAsh:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelFlyAshPv {
			position: absolute;
			top: 22%;
			left: 0%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelFlyAshPv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelFlyAshQck {
			position: absolute;
			top: 45%;
			left: 0%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelFlyAshQck:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelTotal {
			position: absolute;
			top: -41%;
			left: 39.6%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelTotal:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelTotalPv {
			position: absolute;
			top: 24%;
			left: -0.4%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelTotalPv:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQuality {
			position: absolute;
			top: 16.5%;
			left: 41%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}


		.labelClinkerQuality:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityFL {
			position: absolute;
			top: -11.5%;
			left: -47%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinkerQualityFL:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityFLValue {
			position: absolute;
			top: 1.5%;
			left: 19%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelClinkerQualityFLValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityC3s {
			position: absolute;
			top: 17.5%;
			left: -46%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinkerQualityC3s:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityC3sValue {
			position: absolute;
			top: -1.5%;
			left: 18%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelClinkerQualityC3sValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityTemp {
			position: absolute;
			top: 46.5%;
			left: -45%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelClinkerQualityTemp:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelClinkerQualityTempValue {
			position: absolute;
			top: -1.5%;
			left: 17%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelClinkerQualityTempValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BI05 {
			position: absolute;
			top: 37.5%;
			left: 38.7%;
			width: 29.4%;
			text-align: center;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.label552BI05:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BC04 {
			position: absolute;
			top: 49.5%;
			left: 40.7%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552BC04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552BC06 {
			position: absolute;
			top: 59%;
			left: 38.3%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552BC06:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552VC01 {
			position: absolute;
			top: 78.5%;
			left: 47.7%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552VC01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552VC02 {
			position: absolute;
			top: 82%;
			left: 43%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552VC02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552VC02Value {
			position: absolute;
			top: -27%;
			left: -28%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.label552VC02Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label544FN02 {
			position: absolute;
			top: 80.5%;
			left: 76.7%;
			width: 29.4%;
			text-align: center;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.label544FN02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552DA09 {
			position: absolute;
			top: 20.5%;
			left: 69.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552DA09:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552DA09Value {
			position: absolute;
			top: -74.5%;
			left: -8.8%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label552DA09Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552FN15 {
			position: absolute;
			top: 21%;
			left: 74.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552FN15:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552FN15Value1 {
			position: absolute;
			top: -113%;
			left: -9.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label552FN15Value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552FN15Value2 {
			position: absolute;
			top: -97%;
			left: -9.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label552FN15Value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552FN15Value3 {
			position: absolute;
			top: -84%;
			left: -9.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label552FN15Value3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552DA10 {
			position: absolute;
			top: 24%;
			left: 80.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label552DA10:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label552DA10Value {
			position: absolute;
			top: -70%;
			left: -9.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label552DA10Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label567BE01 {
			position: absolute;
			top: 30%;
			left: 82%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label567BE01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label567BE01Value {
			position: absolute;
			top: -29%;
			left: 2%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label567BE01Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQuality {
			position: absolute;
			top: 86.5%;
			left: 59.2%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQuality:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityTime {
			position: absolute;
			top: -22%;
			left: -47%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQualityTime:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityTimeValue {
			position: absolute;
			top: -2%;
			left: 16%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualityTimeValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityBlaine {
			position: absolute;
			top: 5%;
			left: -46.2%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelCementQualityBlaine:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelCementQualityBlaineValue {
			position: absolute;
			top: 1%;
			left: 15%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualityBlaineValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityR-45 {
			position: absolute;
			top: 35%;
			left: -47%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQualityR-45:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityR-45Value {
			position: absolute;
			top: -2%;
			left: 16%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualityR-45Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityfreeLime {
			position: absolute;
			top: -21%;
			left: -14%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQualityFreeLime:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualityfreeLimeValue {
			position: absolute;
			top: 0%;
			left: 18%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualityFreeLimeValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualitySo3 {
			position: absolute;
			top: 7%;
			left: -17%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQualitySo3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementQualitySo3Value {
			position: absolute;
			top: -2%;
			left: 21%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualitySo3Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelCementQualityLoi {
			position: absolute;
			top: 33%;
			left: -17%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelCementQualityLoi:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelCementQualityLoiValue {
			position: absolute;
			top: 0%;
			left: 21%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementQualityLoiValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelCementSilo1 {
			position: absolute;
			top: 61%;
			left: 84.5%;
			width: 4.4%;
			text-align: center;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.labelCementSilo1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementSilo1Value {
			position: absolute;
			top: -196%;
			left: 23%;
			width: 57.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.labelCementSilo1Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementSilo2 {
			position: absolute;
			top: 61%;
			left: 89.8%;
			width: 4.4%;
			text-align: center;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.labelCementSilo2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementSilo2Value {
			position: absolute;
			top: -196%;
			left: 23%;
			width: 57.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.labelCementSilo2Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementSilo3 {
			position: absolute;
			top: 61%;
			left: 94.9%;
			width: 4.4%;
			text-align: center;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.labelCementSilo3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementSilo3Value {
			position: absolute;
			top: -196%;
			left: 23%;
			width: 57.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.7vw;
		}

		.labelCementSilo3Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementProd {
			position: absolute;
			top: 85%;
			left: 90%;
			width: 57.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw
		}

		.labelCementProd:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementProdValue1 {
			position: absolute;
			top: -46%;
			left: -26%;
			width: 57.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementProdValue1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelCementProdValue2 {
			position: absolute;
			top: -36%;
			left: -26%;
			width: 57.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelCementProdValue2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label544DA04 {
			position: absolute;
			top: 76.5%;
			left: 72.5%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label544DA04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label544DA04Value {
			position: absolute;
			top: -38.5%;
			left: -62.3%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label544DA04Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label547HS01 {
			position: absolute;
			top: 63.5%;
			left: 57%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label547HS01 :before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.label547HS01Value {
			position: absolute;
			top: 65.5%;
			left: -27.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.label547HS01Value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelDiffPress {
			position: absolute;
			top: 65.5%;
			left: 57%;
			width: 29.4%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelDiffPress :before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelDiffPressValue {
			position: absolute;
			top: -38.5%;
			left: -26.3%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.labelDiffPressValue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value1 {
			position: absolute;
			top: 70.5%;
			left: 59%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value2 {
			position: absolute;
			top: 78%;
			left: 59%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value3 {
			position: absolute;
			top: 11%;
			left: 62%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value4 {
			position: absolute;
			top: 49%;
			left: 63%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value5 {
			position: absolute;
			top: 49.5%;
			left: 53.2%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value6 {
			position: absolute;
			top: 61%;
			left: 45.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value6:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value7 {
			position: absolute;
			top: 65%;
			left: 43%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value7:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value8 {
			position: absolute;
			top: 69%;
			left: 43%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value8:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.Value9 {
			position: absolute;
			top: 70.5%;
			left: 43%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value9:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.Value10 {
			position: absolute;
			top: 33.5%;
			left: 53%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value10:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.Value11 {
			position: absolute;
			top: 35.5%;
			left: 53%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value11:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.Value12 {
			position: absolute;
			top: 30.5%;
			left: 60%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value12:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.Value13 {
			position: absolute;
			top: 30.5%;
			left: 66.5%;
			width: 29.4%;
			text-align: center;
			color: #3AD120;
			font-size: 0.6vw;
		}

		.Value13:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.spanbutton1 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 92%;
			top: 4%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;
			color: #42ff00;
		}

		.spanbutton1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.spanbutton2 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 84%;
			top: 4%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;
			color: #42ff00;
		}

		.spanbutton2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}





		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}

		.square-box:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="labelClinkerStorage">
				<div class="square-content">
					<span>CLINKER STORAGE</span>
					<div class="labelClinkerStorageValue">
						<span>88 %</span>
					</div>
				</div>
			</div>
			<div class="label551AC05">
				<div class="square-content">
					<span>551AC05
					</span>
				</div>
			</div>
			<div class="labelClinker1">
				<div class="square-content">
					<span>CLINKER</span>
					<div class="label548BI01">
						<span>548BI01</span>
					</div>
				</div>
			</div>
			<div class="labelClinker2">
				<div class="square-content">
					<span>CLINKER</span>
					<div class="label552BI01">
						<span>552BI01</span>
					</div>
				</div>
			</div>
			<div class="labelGypsum">
				<div class="square-content">
					<span>GYPSUM</span>
					<div class="label552BI02">
						<span>552BI02</span>
					</div>
				</div>
			</div>
			<div class="labelTrass">
				<div class="square-content">
					<span>TRASS</span>
					<div class="label552BI03">
						<span>552BI03</span>
					</div>
				</div>
			</div>
			<div class="labelAdditives">
				<div class="square-content">
					<span>ADDITIVES</span>
					<div class="label552BI04">
						<span>552BI04</span>
					</div>
				</div>
			</div>
			<div class="labelFM2">
				<div class="square-content">
					<span>FM2</span>
				</div>
			</div>
			<div class="label552BC01">
				<div class="square-content">
					<span>552BC01</span>
				</div>
			</div>
			<div class="label552BC02">
				<div class="square-content">
					<span>552BC02</span>
				</div>
			</div>
			<div class="label552BC03">
				<div class="square-content">
					<span>552BC03</span>
				</div>
			</div>
			<div class="label547BE01">
				<div class="square-content">
					<span>547BE01</span>
					<div class="label547BE01Value">
						<span>61 %</span>
					</div>
				</div>
			</div>
			<div class="labelPv">
				<div class="square-content">
					<span>PV</span>
					<div class="labelQck">
						<span>QCK</span>
					</div>
				</div>
			</div>
			<div class="labelClinker3">
				<div class="square-content">
					<span>Clinker</span>
					<div class="labelClinker3Pv">
						<span>154,6 t/h</span>
					</div>
					<div class="labelClinker3Qck">
						<span>72,0 %</span>
					</div>
					<div class="labelGypsum2">
						<span>Gypsum</span>
						<div class="labelGypsum2Pv">
							<span>8,6 t/h</span>
						</div>
						<div class="labelGypsum2Qck">
							<span>4,0 %</span>
						</div>
					</div>
					<div class="labelTrass2">
						<span>Trass</span>
						<div class="labelTrass2Pv">
							<span>36,5 t/h</span>
						</div>
						<div class="labelTrass2Qck">
							<span>17,0 %</span>
						</div>
					</div>
					<div class="labelAdditives2">
						<span>Additives</span>
						<div class="labelAdditives2Pv">
							<span>10,7 t/h</span>
						</div>
						<div class="labelAdditives2Qck">
							<span>5,0 %</span>
						</div>
					</div>
					<div class="labelFlyAsh">
						<span>Fluy Ash</span>
						<div class="labelFlyAshPv">
							<span>4,0 t/h</span>
						</div>
						<div class="labelFlyAshQck">
							<span>2,0 %</span>
						</div>
					</div>
					<div class="labelTotal">
						<span>Total</span>
						<div class="labelTotalPv">
							<span>197 t/h</span>
						</div>
					</div>
				</div>
			</div>
			<div class="labelClinkerQuality">
				<div class="square-content">
					<span>CLINKER QUALITY</span>
					<div class="labelClinkerQualityFL">
						<Span>FL</Span>
						<div class="labelClinkerQualityFLValue">
							<span>1,33</span>
						</div>
					</div>
					<div class="labelClinkerQualityC3s">
						<span>C3S</span>
						<div class="labelClinkerQualityC3sValue">
							<span>57,50</span>
						</div>
					</div>
					<div class="labelClinkerQualityTemp">
						<span>TEMP</span>
						<div class="labelClinkerQualityTempValue">
							<span>87,0</span>
						</div>
					</div>
				</div>
			</div>
			<div class="label552BI05">
				<div class="square-content">
					<span>552BI05</span>
				</div>
			</div>
			<div class="label552BC04">
				<div class="square-content">
					<span>552BC04</span>
				</div>
			</div>
			<div class="label552BC06">
				<div class="square-content">
					<span>552BC06</span>
				</div>
			</div>
			<div class="label552VC01">
				<div class="square-content">
					<span>552VC01</span>
				</div>
			</div>
			<div class="label552VC02">
				<div class="square-content">
					<span>552VC02</span>
				</div>
				<div class="label552VC02Value">
					<span>0,51 t/h</span>

				</div>
			</div>
			<div class="label544FN02">
				<div class="square-content">
					<span>544FN02</span>
				</div>
			</div>
			<div class="label552DA09">
				<div class="square-content">
					<span>552DA09</span>
				</div>
				<div class="label552DA09Value">
					<span>94,6 %</span>
				</div>
			</div>
			<div class="label552FN15">
				<div class="square-content">
					<span>552FN15</span>
				</div>
				<div class="label552FN15Value1">
					<span>85 A</span>
				</div>
				<div class="label552FN15Value2">
					<span>1920 KW</span>
				</div>
				<div class="label552FN15Value3">
					<span>935 rpm</span>
				</div>
			</div>
			<div class="label552DA10">
				<div class="square-content">
					<span>552DA10</span>
				</div>
				<div class="label552DA10Value">
					<span>97,2 %</span>
				</div>

			</div>
			<div class="label567BE01">
				<div class="square-content">
					<span>567BE01</span>
				</div>
				<div class="label567BE01Value">
					<span>48,4 %</span>
				</div>
			</div>
			<div class="labelCementQuality">
				<div class="square-content">
					<span>CEMENT QUALITY</span>
					<div class="labelCementQualityTime">
						<span>Time</span>
						<div class="labelCementQualityTimeValue">
							<span>15.32</span>
						</div>
					</div>
					<div class="labelCementQualityBlaine">
						<span>Blaine</span>
						<div class="labelCementQualityBlaineValue">
							<span>360</span>
						</div>
					</div>
					<div class="labelCementQualityR-45">
						<span>R-45</span>
						<div class="labelCementQualityR-45Value">
							<span>9.80</span>
						</div>
					</div>
					<div class="labelCementQualityFreeLime">
						<span>FreeLime</span>
						<div class="labelCementQualityFreeLimeValue">
							<span>1.48</span>
						</div>
					</div>
					<div class="labelCementQualitySo3">
						<span>SO3</span>
						<div class="labelCementQualitySo3Value">
							<span>1.71</span>
						</div>
					</div>
					<div class="labelCementQualityLoi">
						<span>LOI</span>
						<div class="labelCementQualityLoiValue">
							<span>372.00</span>
						</div>
					</div>
				</div>
			</div>
			<div class="labelCementSilo1">
				<div class="square-content">
					<span>CEMENT SILO 1</span>
				</div>
				<div class="labelCementSilo1Value">
					<span>88 %</span>
				</div>
			</div>
			<div class="labelCementSilo2">
				<div class="square-content">
					<span>CEMENT SILO 2</span>
				</div>
				<div class="labelCementSilo2Value">
					<span>67 %</span>
				</div>
			</div>
			<div class="labelCementSilo3">
				<div class="square-content">
					<span>CEMENT SILO 3</span>
				</div>
				<div class="labelCementSilo3Value">
					<span>98 %</span>
				</div>
			</div>
			<div class="labelCementProd">
				<div class="square-content">
					<span>CEMENT PROD.</span>
				</div>
				<div class="labelCementProdValue1">
					<span>8417 ton</span>
				</div>
				<div class="labelCementProdValue2">
					<span>3317 ton</span>
				</div>
			</div>
			<div class="label544DA04">
				<div class="square-content">
					<span>544DA04</span>
					<div class="label544DA04Value">
						<span>96,0 %</span>
					</div>
				</div>
			</div>
			<div class="label547HS01">
				<div class="square-content">
					<span>5547HS01 :</span>
					<div class="label547HS01Value">
						<span>94,8 bar</span>
					</div>
				</div>
			</div>
			<div class="labelDiffPress">
				<div class="square-content">
					<span>Diff. press. :</span>
					<div class="labelDiffPressValue">
						<span>42,6 mbar</span>
					</div>
				</div>
			</div>
			<div class="Value1">
				<div class="square-content">
					<span>101 °C</span>
				</div>
			</div>
			<div class="Value2">
				<div class="square-content">
					<span>2198 KW</span>
				</div>
			</div>
			<div class="Value3">
				<div class="square-content">
					<span>17 mbar</span>
				</div>
			</div>
			<div class="Value4">
				<div class="square-content">
					<span>5741 lit/h</span>
				</div>
			</div>
			<div class="Value5">
				<div class="square-content">
					<span>63,75 %</span>
				</div>
			</div>
			<div class="Value6">
				<div class="square-content">
					<span>217 t/h</span>
				</div>
			</div>
			<div class="Value7">
				<div class="square-content">
					<span>27,7 kWh/ton</span>
				</div>
			</div>
			<div class="Value8">
				<div class="square-content">
					<span>5,5 mm/s</span>
				</div>
			</div>
			<div class="Value9">
				<div class="square-content">
					<span>7,2 mm/s</span>
				</div>
			</div>
			<div class="Value10">
				<div class="square-content">
					<span>87,71 °C</span>
				</div>
			</div>
			<div class="Value11">
				<div class="square-content">
					<span>-57,1 mbar</span>
				</div>
			</div>
			<div class="Value12">
				<div class="square-content">
					<span>49 %</span>
				</div>
			</div>
			<div class="Value13">
				<div class="square-content">
					<span>56 %</span>
				</div>
			</div>
			<div class="spanButton2">
				<div class="square-content">
					<a href="">
						<button disabled="" type="button" class="btn btn-default" style="width: 100%;">FM 1</button></a>
				</div>
			</div>
			<div class="spanButton1">
				<div class="square-content">
					<a href="">
						<button type="button" class="btn btn-default" style="width: 100%;">FM 2</button></a>
				</div>
			</div>
			<div class="bg1">
				<div class='square-content'>
					<div>
						<span>
							<a href="">F1</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		// var url = 'ISIKAN URL API';
		// $.ajax({
		// 	url: url,
		// 	type: 'get',
		// 	// dataType: 'json',
		// 	success: function(data) {
		// 		var data1 = data.replace("<title>Json</title>", "");
		// 		var data2 = data1.replace("(", "[");
		// 		var data3 = data2.replace(");", "]");
		// 		var dataJson = JSON.parse(data3);
		// 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
		// 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
		// 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
		// 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
		// 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
		// 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
		// 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
		// 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
		// 		$("#isisilo10").html(tonageSilo10);
		// 		$("#isisilo11").html(tonageSilo11);
		// 		$("#isisilo12").html(tonageSilo12);
		// 	}
		// });
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>