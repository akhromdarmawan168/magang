<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('finish mil 3 23.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;

        }

        .span1 {
            position: absolute;
            top: 15.5%;
            left: 3.5%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span2 {
            position: absolute;
            top: 15.5%;
            left: 12%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span3 {
            position: absolute;
            top: 15.5%;
            left: 21%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span4 {
            position: absolute;
            top: 15.5%;
            left: 28.3%;
            width: 3%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span5 {
            position: absolute;
            top: 56%;
            left: 17.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span6 {
            position: absolute;
            top: 58.3%;
            left: 40.8%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span7 {
            position: absolute;
            top: 60.5%;
            left: 20.4%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span8 {
            position: absolute;
            top: 57%;
            left: 23.1%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span9 {
            position: absolute;
            top: 57.1%;
            left: 26%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span10 {
            position: absolute;
            top: 70%;
            left: 28%;
            width: 6%;
            text-align: center;
            color: black;
            FONT-WEIGHT: bold;
            font-size: 0.7vw;
        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span11 {
            position: absolute;
            top: 79.6%;
            left: 15.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span12 {
            position: absolute;
            top: 80.6%;
            left: 20.1%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span13 {
            position: absolute;
            top: 83.7%;
            left: 23.2%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span14 {
            position: absolute;
            top: 83.7%;
            left: 26.1%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span15 {
            position: absolute;
            top: 88%;
            left: 23.8%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span16 {
            position: absolute;
            top: 86.7%;
            left: 30.1%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span17 {
            position: absolute;
            top: 86.7%;
            left: 32.6%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span18 {
            position: absolute;
            top: 86.7%;
            left: 35%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span19 {
            position: absolute;
            top: 86.7%;
            left: 37.4%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span20 {
            position: absolute;
            top: 88.5%;
            left: 32.4%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span21 {
            position: absolute;
            top: 82%;
            left: 42%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span22 {
            position: absolute;
            top: 91.5%;
            left: 40.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span23 {
            position: absolute;
            top: 66%;
            left: 52.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span24 {
            position: absolute;
            top: 28.1%;
            left: 62.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span25 {
            position: absolute;
            top: 33.1%;
            left: 62.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span26 {
            position: absolute;
            top: 17.3%;
            left: 71.8%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span27 {
            position: absolute;
            top: 15.3%;
            left: 81.8%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span28 {
            position: absolute;
            top: 21.5%;
            left: 82.5%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span29 {
            position: absolute;
            top: 18.9%;
            left: 91.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span30 {
            position: absolute;
            top: 29%;
            left: 79.9%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span31 {
            position: absolute;
            top: 29%;
            left: 85.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span32 {
            position: absolute;
            top: 38.7%;
            left: 80.9%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span33 {
            position: absolute;
            top: 37.2%;
            left: 90.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span34 {
            position: absolute;
            top: 39%;
            left: 91.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span35 {
            position: absolute;
            top: 44%;
            left: 76.7%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span36 {
            position: absolute;
            top: 45%;
            left: 65.2%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span37 {
            position: absolute;
            top: 50.5%;
            left: 96%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span38 {
            position: absolute;
            top: 65.5%;
            left: 96.6%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span39 {
            position: absolute;
            top: 71.2%;
            left: 76%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span40 {
            position: absolute;
            top: 71.2%;
            left: 80%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span41 {
            position: absolute;
            top: 73.2%;
            left: 76%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span42 {
            position: absolute;
            top: 73.2%;
            left: 85%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span43 {
            position: absolute;
            top: 80%;
            left: 64%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span44 {
            position: absolute;
            top: 81.5%;
            left: 73.4%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span45 {
            position: absolute;
            top: 81.5%;
            left: 78.1%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span46 {
            position: absolute;
            top: 81.5%;
            left: 82.5%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span47 {
            position: absolute;
            top: 81.5%;
            left: 87.3%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span48 {
            position: absolute;
            top: 80%;
            left: 96%;
            width: 3%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span49 {
            position: absolute;
            top: 88%;
            left: 70.7%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span50 {
            position: absolute;
            top: 88%;
            left: 77.7%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span51 {
            position: absolute;
            top: 88%;
            left: 84.8%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span52 {
            position: absolute;
            top: 88%;
            left: 92%;
            width: 3%;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span53 {
            position: absolute;
            top: 94.5%;
            left: 70%;
            width: 3%;
            text-align: center;
            color: #7cff00;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span54 {
            position: absolute;
            top: 94.5%;
            left: 77.2%;
            width: 3%;
            text-align: center;
            color: #7cff00;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span55 {
            position: absolute;
            top: 94.5%;
            left: 84.2%;
            width: 3%;
            text-align: center;
            color: #7cff00;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span56 {
            position: absolute;
            top: 94.5%;
            left: 91.3%;
            width: 3%;
            text-align: center;
            color: #7cff00;
            font-weight: bold;
            font-size: 0.7vw;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .spanButton1 {
            position: absolute;
            top: 69.5%;
            left: 63.3%;
            width: 5%;
            text-align: center;
            background: #d5d5d5;
            font-size: 0.7vw;
        }

        .spanButton1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>GYPSUM</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>CLINKER</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>TRASS</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>LIMESTONE</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>2210</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>FN.401</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>CEMENT MILL 3 0801</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>2093</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>0803</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>0805</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>0801</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>0806</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>0902</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>2308</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>2302</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>2306 </span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>2305 </span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>531.BF360 </span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>AS361 </span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>FN380 </span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>FN362 </span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>FN363 </span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>FN151 </span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>3210103 </span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>3210104 </span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>3210105 </span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>03</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>3210703</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>2101</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>3210304</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>3230503M2</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>0301</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>3230503</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>4</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>2</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>1</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>3</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>0.0 m</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>7.0 m</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>0.6 m</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>3.1 m</span>
                </div>
            </div>
            <div class="spanButton1">
                <div class="square-content">
                    <a href="">TO Z2</a>
                </div>
            </div>
        </div>
    </div>

</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>