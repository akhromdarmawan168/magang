<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('rm 2 tonasa 23.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;

        }

        .span1 {
            position: absolute;
            width: 10%;
            top: 27.5%;
            left: 6.8%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span2 {
            position: absolute;
            width: 2%;
            top: 28.5%;
            left: 2.5%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span3 {
            position: absolute;
            width: 4%;
            top: 38.5%;
            left: 7.1%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span4 {
            position: absolute;
            width: 4%;
            top: 42.5%;
            left: 3.3%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span5 {
            position: absolute;
            width: 6%;
            top: 73.5%;
            left: 16.5%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span6 {
            position: absolute;
            width: 6%;
            top: 45%;
            left: 16.4%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span7 {
            position: absolute;
            width: 6%;
            top: 45%;
            left: 18.9%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span8 {
            position: absolute;
            width: 6%;
            top: 45%;
            left: 22.8%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span9 {
            position: absolute;
            width: 6%;
            top: 45%;
            left: 26.9%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span10 {
            position: absolute;
            width: 6%;
            top: 45%;
            left: 29.5%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span11 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 66.9%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span12 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 71.2%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span13 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 75.5%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span14 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 79.9%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span15 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 84.2%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span16 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 88.7%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span17 {
            position: absolute;
            width: 0%;
            top: 61%;
            left: 93.1%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span18 {
            position: absolute;
            width: 5%;
            top: 82%;
            left: 79.6%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span19 {
            position: absolute;
            width: 5%;
            top: 34%;
            left: 5.4%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span20 {
            position: absolute;
            width: 5%;
            top: 36%;
            left: 6%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span21 {
            position: absolute;
            width: 5%;
            top: 34.2%;
            left: 12.3%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span22 {
            position: absolute;
            width: 5%;
            top: 43.2%;
            left: 7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span23 {
            position: absolute;
            width: 5%;
            top: 47.2%;
            left: 5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span24 {
            position: absolute;
            width: 5%;
            top: 55.2%;
            left: 2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span25 {
            position: absolute;
            width: 5%;
            top: 66%;
            left: 11%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span26 {
            position: absolute;
            width: 5%;
            top: 50%;
            left: 13.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span27 {
            position: absolute;
            width: 5%;
            top: 60.1%;
            left: 15.1%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span28 {
            position: absolute;
            width: 5%;
            top: 61%;
            left: 18.3%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span29 {
            position: absolute;
            width: 5%;
            top: 67.8%;
            left: 14.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span30 {
            position: absolute;
            width: 5%;
            top: 67.8%;
            left: 19.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span31 {
            position: absolute;
            width: 5%;
            top: 64%;
            left: 21.3%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span32 {
            position: absolute;
            width: 5%;
            top: 66.5%;
            left: 23.1%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span33 {
            position: absolute;
            width: 5%;
            top: 66.4%;
            left: 26.6%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span34 {
            position: absolute;
            width: 5%;
            top: 68.2%;
            left: 26%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span35 {
            position: absolute;
            width: 5%;
            top: 70.2%;
            left: 26%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span36 {
            position: absolute;
            width: 5%;
            top: 76.7%;
            left: 23.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span37 {
            position: absolute;
            width: 5%;
            top: 78.3%;
            left: 26.1%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span38 {
            position: absolute;
            width: 5%;
            top: 80.3%;
            left: 26.1%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span39 {
            position: absolute;
            width: 5%;
            top: 81.6%;
            left: 10.4%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span40 {
            position: absolute;
            width: 5%;
            top: 81.6%;
            left: 12.3%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span41 {
            position: absolute;
            width: 5%;
            top: 81.6%;
            left: 13.88%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span42 {
            position: absolute;
            width: 5%;
            top: 81.9%;
            left: 16.6%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span43 {
            position: absolute;
            width: 5%;
            top: 81.9%;
            left: 18.15%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span44 {
            position: absolute;
            width: 5%;
            top: 81.9%;
            left: 19.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span45 {
            position: absolute;
            width: 5%;
            top: 82.5%;
            left: 21.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span46 {
            position: absolute;
            width: 5%;
            top: 89%;
            left: 14.4%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span47 {
            position: absolute;
            width: 5%;
            top: 30.5%;
            left: 17.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span48 {
            position: absolute;
            width: 5%;
            top: 33.2%;
            left: 17.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span49 {
            position: absolute;
            width: 5%;
            top: 36.2%;
            left: 19.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span50 {
            position: absolute;
            width: 5%;
            top: 36.2%;
            left: 21%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span51 {
            position: absolute;
            width: 5%;
            top: 36.2%;
            left: 22.57%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span52 {
            position: absolute;
            width: 5%;
            top: 30.8%;
            left: 27.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span53 {
            position: absolute;
            width: 5%;
            top: 34.6%;
            left: 27.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span54 {
            position: absolute;
            width: 5%;
            top: 34.6%;
            left: 30%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span55 {
            position: absolute;
            width: 5%;
            top: 30.6%;
            left: 30.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span56 {
            position: absolute;
            width: 5%;
            top: 28.8%;
            left: 35.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span57 {
            position: absolute;
            width: 5%;
            top: 34.8%;
            left: 35.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span58 {
            position: absolute;
            width: 5%;
            top: 44.6%;
            left: 32.3%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span59 {
            position: absolute;
            width: 5%;
            top: 28.7%;
            left: 51.6%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span60 {
            position: absolute;
            width: 5%;
            top: 35.8%;
            left: 52%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span61 {
            position: absolute;
            width: 5%;
            top: 36%;
            left: 49.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span62 {
            position: absolute;
            width: 5%;
            top: 39.2%;
            left: 49.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span63 {
            position: absolute;
            width: 5%;
            top: 50.8%;
            left: 44.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span64 {
            position: absolute;
            width: 5%;
            top: 56.8%;
            left: 39.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span65 {
            position: absolute;
            width: 5%;
            top: 56.8%;
            left: 45%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span66 {
            position: absolute;
            width: 5%;
            top: 70%;
            left: 45.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span67 {
            position: absolute;
            width: 5%;
            top: 70%;
            left: 46.75%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span68 {
            position: absolute;
            width: 5%;
            top: 70%;
            left: 48.48%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span69 {
            position: absolute;
            width: 5%;
            top: 77.5%;
            left: 41.48%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span70 {
            position: absolute;
            width: 5%;
            top: 77.5%;
            left: 45.48%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span71 {
            position: absolute;
            width: 5%;
            top: 77.5%;
            left: 48.28%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span72 {
            position: absolute;
            width: 5%;
            top: 77.5%;
            left: 52.28%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span73 {
            position: absolute;
            width: 5%;
            top: 75.8%;
            left: 52%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span73:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span74 {
            position: absolute;
            width: 5%;
            top: 85%;
            left: 49.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span74:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span75 {
            position: absolute;
            width: 5%;
            top: 85%;
            left: 49.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span75:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span76 {
            position: absolute;
            width: 5%;
            top: 47%;
            left: 55.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span76:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span77 {
            position: absolute;
            width: 5%;
            top: 40.5%;
            left: 58.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span77:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span78 {
            position: absolute;
            width: 5%;
            top: 38.3%;
            left: 60.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span78:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span79 {
            position: absolute;
            width: 5%;
            top: 43.3%;
            left: 60%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .span79:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span80 {
            position: absolute;
            width: 5%;
            top: 36.3%;
            left: 63%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span80:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span81 {
            position: absolute;
            width: 10%;
            top: 44.9%;
            left: 69.5%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span81:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span82 {
            position: absolute;
            width: 10%;
            top: 47.6%;
            left: 69.5%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span82:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span83 {
            position: absolute;
            width: 7%;
            top: 50.8%;
            left: 69.3%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span83:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span84 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 68%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span84:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span85 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 72.55%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span85:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span86 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 76.9%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span86:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span87 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 81.28%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span87:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span88 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 85.75%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span88:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span89 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 90%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span89:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span90 {
            position: absolute;
            width: 7%;
            top: 53.8%;
            left: 94.4%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span90:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span91 {
            position: absolute;
            width: 7%;
            top: 58%;
            left: 62.6%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span91:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span92 {
            position: absolute;
            width: 7%;
            top: 66%;
            left: 63.8%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span92:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span93 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 69.5%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span93:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span94 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 73.9%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span94:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span95 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 78.2%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span95:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span96 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 82.6%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span96:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span97 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 87%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span97:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span98 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 91.3%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span98:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span99 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 95.7%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span99:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span100 {
            position: absolute;
            width: 7%;
            top: 76.5%;
            left: 66.3%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span100:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span101 {
            position: absolute;
            width: 7%;
            top: 83.8%;
            left: 74.5%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span101:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span102 {
            position: absolute;
            width: 7%;
            top: 64.6%;
            left: 56%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span102:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span103 {
            position: absolute;
            width: 7%;
            top: 64.6%;
            left: 59.7%;
            color: white;
            font-size: 0.5vw;
            text-align: left;
        }

        .span103:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span104 {
            position: absolute;
            width: 7%;
            top: 29.6%;
            left: 11%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
            ss
        }

        .span104:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span105 {
            position: absolute;
            width: 7%;
            top: 37.6%;
            left: 4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span105:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span106 {
            position: absolute;
            width: 7%;
            top: 26.6%;
            left: 15%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span106:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span107 {
            position: absolute;
            width: 7%;
            top: 46.8%;
            left: 8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span107:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span108 {
            position: absolute;
            width: 7%;
            top: 51%;
            left: 4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span108:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span109 {
            position: absolute;
            width: 7%;
            top: 57%;
            left: 4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span109:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span110 {
            position: absolute;
            width: 7%;
            top: 64%;
            left: 4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span110:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span111 {
            position: absolute;
            width: 7%;
            top: 66.3%;
            left: 4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span111:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span112 {
            position: absolute;
            width: 7%;
            top: 68.3%;
            left: 11.4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span112:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span113 {
            position: absolute;
            width: 7%;
            top: 68%;
            left: 30.4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span113:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span114 {
            position: absolute;
            width: 7%;
            top: 71.3%;
            left: 23.3%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span114:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span115 {
            position: absolute;
            width: 7%;
            top: 81.3%;
            left: 23.3%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;
        }

        .span115:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span116 {
            position: absolute;
            width: 7%;
            top: 78%;
            left: 17.8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span116:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span117 {
            position: absolute;
            width: 7%;
            top: 29%;
            left: 21.8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span117:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span118 {
            position: absolute;
            width: 7%;
            top: 31%;
            left: 21.8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span118:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span119 {
            position: absolute;
            width: 7%;
            top: 26.5%;
            left: 26.7%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span119:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span120 {
            position: absolute;
            width: 7%;
            top: 26.5%;
            left: 29.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span120:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span121 {
            position: absolute;
            width: 7%;
            top: 27.5%;
            left: 44.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span121:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span122 {
            position: absolute;
            width: 7%;
            top: 29%;
            left: 44.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span122:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span123 {
            position: absolute;
            width: 7%;
            top: 27%;
            left: 51.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span123:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span124 {
            position: absolute;
            width: 7%;
            top: 37.5%;
            left: 51.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span124:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span125 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 42.2%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span125:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span126 {
            position: absolute;
            width: 7%;
            top: 70%;
            left: 51.5%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span126:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span127 {
            position: absolute;
            width: 7%;
            top: 79%;
            left: 41.5%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span127:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span128 {
            position: absolute;
            width: 7%;
            top: 79%;
            left: 48%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span128:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span129 {
            position: absolute;
            width: 7%;
            top: 88.5%;
            left: 34%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span129:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span130 {
            position: absolute;
            width: 7%;
            top: 88.5%;
            left: 37.6%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span130:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span131 {
            position: absolute;
            width: 7%;
            top: 66.5%;
            left: 56.4%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;


        }

        .span131:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span132 {
            position: absolute;
            width: 7%;
            top: 66.5%;
            left: 58.8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span132:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span133 {
            position: absolute;
            width: 7%;
            top: 49.5%;
            left: 64.8%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span133:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span134 {
            position: absolute;
            width: 7%;
            top: 84%;
            left: 86.5%;
            color: #7cff00;
            font-size: 0.5vw;
            text-align: left;

        }

        .span134:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span135 {
            position: absolute;
            width: 11%;
            top: 84.5%;
            left: 55%;
            color: white;
            font-size: 0.7vw;
            text-align: left;

        }

        .span135:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span136 {
            position: absolute;
            width: 11%;
            top: 84.5%;
            left: 66%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;

        }

        .span136:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span137 {
            position: absolute;
            width: 11%;
            top: 87.3%;
            left: 55%;
            color: white;
            font-size: 0.7vw;
            text-align: left;

        }

        .span137:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span138 {
            position: absolute;
            width: 11%;
            top: 87.3%;
            left: 66%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;

        }

        .span138:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span139 {
            position: absolute;
            width: 11%;
            top: 31.5%;
            left: 79%;
            color: white;
            font-size: 0.7vw;
            text-align: left;

        }

        .span139:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span140 {
            position: absolute;
            width: 11%;
            top: 34.5%;
            left: 79%;
            color: white;
            font-size: 0.7vw;
            text-align: left;

        }

        .span140:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span141 {
            position: absolute;
            width: 11%;
            top: 31.5%;
            left: 89%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;

        }

        .span141:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span142 {
            position: absolute;
            width: 11%;
            top: 34.5%;
            left: 89%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;

        }

        .span142:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span143 {
            position: absolute;
            width: 11%;
            top: 41.2%;
            left: 79%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span143:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span144 {
            position: absolute;
            width: 11%;
            top: 44.2%;
            left: 79%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span144:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span145 {
            position: absolute;
            width: 11%;
            top: 47.2%;
            left: 79%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span145:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span146 {
            position: absolute;
            width: 11%;
            top: 41.2%;
            left: 82%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span146:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span147 {
            position: absolute;
            width: 11%;
            top: 44.2%;
            left: 82%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span147:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span148 {
            position: absolute;
            width: 11%;
            top: 47.2%;
            left: 82%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span148:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span149 {
            position: absolute;
            width: 11%;
            top: 41.2%;
            left: 87%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span149:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span150 {
            position: absolute;
            width: 11%;
            top: 44.2%;
            left: 87%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span150:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span151 {
            position: absolute;
            width: 11%;
            top: 47.2%;
            left: 87%;
            color: white;
            font-size: 0.7vw;
            text-align: left;
        }

        .span151:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span152 {
            position: absolute;
            width: 11%;
            top: 41.2%;
            left: 90%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span152:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span153 {
            position: absolute;
            width: 11%;
            top: 44.2%;
            left: 90%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span151:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span154 {
            position: absolute;
            width: 11%;
            top: 47.2%;
            left: 90%;
            color: #7cff00;
            font-size: 0.7vw;
            text-align: left;
        }

        .span151:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span155 {
            position: absolute;
            width: 3%;
            top: 26.8%;
            left: 34%;
            color: black;
            font-size: 0.5vw;
            text-align: center;
            background: white;
        }

        .span155:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>SILICA</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>PASIR BESI</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>L. STONE</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>CLAY</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>RAW MILL 2</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>3</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>4</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>16</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>2</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>1</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>BLENDING BIN 1</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>BLENDING BIN 2</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>BLENDING BIN 3</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>BLENDING BIN 4</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>BLENDING BIN 5</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>BLENDING BIN 6</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>BLENDING BIN 7</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>RAW MEAL SILO 2 </span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>0501</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>03M1</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>06M5</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>01M1</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>02M1</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>04M1</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>003M1</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>20926M1</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>18AM01</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>05021</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>05032</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>17</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>06M14</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>06M2</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>004M1</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>M15</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>16</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>06M3</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>M17</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>18</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>06M7</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>M8</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>M6</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>M12</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>M9</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>M10</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>M13</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>07M01</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>15</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>14</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>13M1</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>09M1</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>10M1</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>12M</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>20M11</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>20M1</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>20M2</span>
                </div>
            </div>
            <div class="span62">
                <div class="square-content">
                    <span>20M4</span>
                </div>
            </div>
            <div class="span63">
                <div class="square-content">
                    <span>23M01</span>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>18M02</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <span>17AM01</span>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <span>0805</span>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>3081001</span>
                </div>
            </div>
            <div class="span70">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>3081002</span>
                </div>
            </div>
            <div class="span72">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="span73">
                <div class="square-content">
                    <span>3080903</span>
                </div>
            </div>
            <div class="span74">
                <div class="square-content">
                    <span>3081101</span>
                </div>
            </div>
            <div class="span75">
                <div class="square-content">
                    <span>3081101</span>
                </div>
            </div>
            <div class="span76">
                <div class="square-content">
                    <span>M1M01</span>
                </div>
            </div>
            <div class="span77">
                <div class="square-content">
                    <span>13M01</span>
                </div>
            </div>
            <div class="span78">
                <div class="square-content">
                    <span>02M04</span>
                </div>
            </div>
            <div class="span79">
                <div class="square-content">
                    <span>02M01</span>
                </div>
            </div>
            <div class="span80">
                <div class="square-content">
                    <span>FROM RAW MEAL SILO 3</span>
                </div>
            </div>
            <div class="span81">
                <div class="square-content">
                    <span>TO RAW MEAL SILO 3</span>
                </div>
            </div>
            <div class="span82">
                <div class="square-content">
                    <span>FROM RAW MEAL 3</span>
                </div>
            </div>
            <div class="span83">
                <div class="square-content">
                    <span>3110407</span>
                </div>
            </div>
            <div class="span84">
                <div class="square-content">
                    <span>03M02</span>
                </div>
            </div>
            <div class="span85">
                <div class="square-content">
                    <span>M03</span>
                </div>
            </div>
            <div class="span86">
                <div class="square-content">
                    <span>M04</span>
                </div>
            </div>
            <div class="span87">
                <div class="square-content">
                    <span>M05</span>
                </div>
            </div>
            <div class="span88">
                <div class="square-content">
                    <span>M06</span>
                </div>
            </div>
            <div class="span89">
                <div class="square-content">
                    <span>M07</span>
                </div>
            </div>
            <div class="span90">
                <div class="square-content">
                    <span>M08</span>
                </div>
            </div>
            <div class="span91">
                <div class="square-content">
                    <span>03M01</span>
                </div>
            </div>
            <div class="span92">
                <div class="square-content">
                    <span>06M08</span>
                </div>
            </div>
            <div class="span93">
                <div class="square-content">
                    <span>06M01</span>
                </div>
            </div>
            <div class="span94">
                <div class="square-content">
                    <span>M02</span>
                </div>
            </div>
            <div class="span95">
                <div class="square-content">
                    <span>M03</span>
                </div>
            </div>
            <div class="span96">
                <div class="square-content">
                    <span>M04</span>
                </div>
            </div>
            <div class="span97">
                <div class="square-content">
                    <span>M05</span>
                </div>
            </div>
            <div class="span98">
                <div class="square-content">
                    <span>M06</span>
                </div>
            </div>
            <div class="span99">
                <div class="square-content">
                    <span>M07</span>
                </div>
            </div>
            <div class="span100">
                <div class="square-content">
                    <span>06M09</span>
                </div>
            </div>
            <div class="span101">
                <div class="square-content">
                    <span>04M01</span>
                </div>
            </div>
            <div class="span102">
                <div class="square-content">
                    <span>01</span>
                </div>
            </div>
            <div class="span103">
                <div class="square-content">
                    <span>M02</span>
                </div>
            </div>
            <div class="span104">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span105">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span106">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span107">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span108">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span109">
                <div class="square-content">
                    <span>100,02 t/h</span>
                </div>
            </div>
            <div class="span110">
                <div class="square-content">
                    <span>339,46°C</span>
                </div>
            </div>
            <div class="span111">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="span112">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span113">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="span114">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span115">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span116">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="span117">
                <div class="square-content">
                    <span>39,46°C</span>
                </div>
            </div>
            <div class="span118">
                <div class="square-content">
                    <span>39,46°C</span>
                </div>
            </div>
            <div class="span119">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span120">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span121">
                <div class="square-content">
                    <span>339,46°C</span>
                </div>
            </div>
            <div class="span122">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="span123">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span124">
                <div class="square-content">
                    <span>90%</span>
                </div>
            </div>
            <div class="span125">
                <div class="square-content">
                    <span>90%</span>
                </div>
            </div>
            <div class="span126">
                <div class="square-content">
                    <span>90%</span>
                </div>
            </div>
            <div class="span127">
                <div class="square-content">
                    <span>90,02 t/h</span>
                </div>
            </div>
            <div class="span128">
                <div class="square-content">
                    <span>90,02 t/h</span>
                </div>
            </div>
            <div class="span129">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span130">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span131">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span132">
                <div class="square-content">
                    <span>30 A</span>
                </div>
            </div>
            <div class="span133">
                <div class="square-content">
                    <span>0406</span>
                </div>
            </div>
            <div class="span134">
                <div class="square-content">
                    <span>25.2 m</span>
                </div>
            </div>
            <div class="span135">
                <div class="square-content">
                    <span>POWER CONSUMPTION</span>
                </div>
            </div>
            <div class="span136">
                <div class="square-content">
                    <span>14.200 KWh</span>
                </div>
            </div>
            <div class="span137">
                <div class="square-content">
                    <span>RUN HOURS</span>
                </div>
            </div>
            <div class="span138">
                <div class="square-content">
                    <span>23.08 hours</span>
                </div>
            </div>
            <div class="span139">
                <div class="square-content">
                    <span>Total Prod Today</span>
                </div>
            </div>
            <div class="span140">
                <div class="square-content">
                    <span>Total Prod Yesterday</span>
                </div>
            </div>
            <div class="span141">
                <div class="square-content">
                    <span>8417,72 ton</span>
                </div>
            </div>
            <div class="span142">
                <div class="square-content">
                    <span>8502,92 ton</span>
                </div>
            </div>
            <div class="span143">
                <div class="square-content">
                    <span>LSF</span>
                </div>
            </div>
            <div class="span144">
                <div class="square-content">
                    <span>SM</span>
                </div>
            </div>
            <div class="span145">
                <div class="square-content">
                    <span>AM</span>
                </div>
            </div>
            <div class="span146">
                <div class="square-content">
                    <span>115</span>
                </div>
            </div>
            <div class="span147">
                <div class="square-content">
                    <span>2,5</span>
                </div>
            </div>
            <div class="span148">
                <div class="square-content">
                    <span>1,5</span>
                </div>
            </div>
            <div class="span149">
                <div class="square-content">
                    <span>R90</span>
                </div>
            </div>
            <div class="span150">
                <div class="square-content">
                    <span>R200</span>
                </div>
            </div>
            <div class="span151">
                <div class="square-content">
                    <span>H20</span>
                </div>
            </div>
            <div class="span152">
                <div class="square-content">
                    <span>13,5%</span>
                </div>
            </div>
            <div class="span153">
                <div class="square-content">
                    <span>3,5%</span>
                </div>
            </div>
            <div class="span154">
                <div class="square-content">
                    <span>0,5%</span>
                </div>
            </div>
            <div class="span155">
                <div class="square-content">
                    <a href="">DAMPER</a>
                </div>
            </div>
        </div>
    </div>
</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        var url = 'http://10.15.5.150/dev/par4digma/api/index.php/plant_tonasa/kiln_t5';
        $.ajax({
            url: url,
            type: 'get',
            //dataType: 'json',
            success: function(data) {
                var data1 = data.replace("<title>Json</title>", "");
                var data2 = data1.replace("(", "[");
                var data3 = data2.replace(");", "]");
                var dataJson = JSON.parse(data3);
                get_KILN_44 = parseFloat(dataJson[0].tags[44].props[0].val).toFixed(2) + " A";
                $("#get_KILN_44").html(get_KILN_44);
                tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
                tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
                tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
                tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
                tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
                tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
                tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
                tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
                $("#isisilo10").html(tonageSilo10);
                $("#isisilo11").html(tonageSilo11);
                $("#isisilo12").html(tonageSilo12);
            }
        });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>