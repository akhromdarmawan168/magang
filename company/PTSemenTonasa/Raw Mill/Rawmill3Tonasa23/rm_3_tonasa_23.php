<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('rm_3_tonasa_23.png');
        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;

        }

        .lstone {
            position: absolute;
            width: 10%;
            top: 23.5%;
            left: 1.6%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .lstone:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .silica {
            position: absolute;
            width: 10%;
            top: 23.5%;
            left: 6.2%;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .silica:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .pasirbesi {
            position: absolute;
            width: 10%;
            top: 23.5%;
            left: 10.5%;
            ;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .pasirbesi:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .clay {
            position: absolute;
            width: 10%;
            top: 23.5%;
            left: 19.3%;
            ;
            color: black;
            font-weight: bold;
            font-size: 0.6vw;
            text-align: center;
        }

        .clay:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0603 {
            position: absolute;
            width: 10%;
            top: 29.8%;
            left: 3.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r0603:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1204 {
            position: absolute;
            width: 10%;
            top: 29.5%;
            left: 8.0%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r1204:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0203 {
            position: absolute;
            width: 10%;
            top: 30.9%;
            left: 21.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r0203:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .ep {
            position: absolute;
            width: 10%;
            top: 22.0%;
            left: 25.8%;
            color: white;
            font-size: 1.0vw;
            text-align: center;
        }

        .ep:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .kiln {
            position: absolute;
            width: 10%;
            top: 28%;
            left: 24.6%;
            color: white;
            font-size: 1.0vw;
            text-align: center;
        }

        .kiln:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value {
            position: absolute;
            width: 10%;
            top: 31%;
            left: 24.7%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3151802 {
            position: absolute;
            width: 10%;
            top: 25.5%;
            left: 30.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r3151802:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value2 {
            position: absolute;
            width: 10%;
            top: 20.5%;
            left: 35.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value3 {
            position: absolute;
            width: 10%;
            top: 31%;
            left: 35.4%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value4 {
            position: absolute;
            width: 10%;
            top: 23.5%;
            left: 40.7%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value5 {
            position: absolute;
            width: 10%;
            top: 33%;
            left: 43.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value6 {
            position: absolute;
            width: 10%;
            top: 37.5%;
            left: 45%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value7 {
            position: absolute;
            width: 10%;
            top: 64.5%;
            left: 27.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value7:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value8 {
            position: absolute;
            width: 10%;
            top: 68.7%;
            left: 27.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value9 {
            position: absolute;
            width: 10%;
            top: 70.7%;
            left: 29.3%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value9:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value10 {
            position: absolute;
            width: 10%;
            top: 74.7%;
            left: 27.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value10:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value11 {
            position: absolute;
            width: 10%;
            top: 74.7%;
            left: 17.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .value11:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value12 {
            position: absolute;
            width: 10%;
            top: 74.7%;
            left: 14.9%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value12:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value13 {
            position: absolute;
            width: 10%;
            top: 69.7%;
            left: 7%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value13:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value14 {
            position: absolute;
            width: 10%;
            top: 62.3%;
            left: 7%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value14:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value15 {
            position: absolute;
            width: 10%;
            top: 65.3%;
            left: 6%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value15:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value16 {
            position: absolute;
            width: 10%;
            top: 61.3%;
            left: 12.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }

        .value17 {
            position: absolute;
            width: 10%;
            top: 58.3%;
            left: 24.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }


        .value17:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value18 {
            position: absolute;
            width: 10%;
            top: 58.3%;
            left: 22.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }


        .value18:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value19 {
            position: absolute;
            width: 10%;
            top: 62.7%;
            left: 47.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }


        .value19:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value20 {
            position: absolute;
            width: 10%;
            top: 59.7%;
            left: 54.3%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .value20:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value21 {
            position: absolute;
            width: 10%;
            top: 48.3%;
            left: 66.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value21:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value22 {
            position: absolute;
            width: 10%;
            top: 55.1%;
            left: 67.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value22:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value23 {
            position: absolute;
            width: 10%;
            top: 57.5%;
            left: 67.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value23:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value24 {
            position: absolute;
            width: 10%;
            top: 71.5%;
            left: 76.9%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .value24:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value25 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 76%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value25:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value26 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 80.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value26:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value27 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 84.4%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value27:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value28 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 88.6%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value28:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value29 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 92.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value29:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value30 {
            position: absolute;
            width: 10%;
            top: 57.3%;
            left: 97%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value30:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value31 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 70.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value31:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value32 {
            position: absolute;
            width: 10%;
            top: 42.8%;
            left: 74.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value32:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value33 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 79.4%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value33:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value34 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 83.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value34:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value35 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 87.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value35:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value36 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 92%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value36:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value37 {
            position: absolute;
            width: 10%;
            top: 43%;
            left: 96.2%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value37:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value38 {
            position: absolute;
            width: 10%;
            top: 40%;
            left: 71.7%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value38:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value39 {
            position: absolute;
            width: 6%;
            top: 42.5%;
            left: 57.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value39:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value40 {
            position: absolute;
            width: 3%;
            top: 45.8%;
            left: 54.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .value40:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value41 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 69.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value41:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value42 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 139.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value42:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value43 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 277.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value43:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value44 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 421.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value44:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value45 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 562.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value45:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value46 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 707.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value46:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .value47 {
            position: absolute;
            width: 3%;
            top: 49.8%;
            left: 846.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }


        .value47:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3110202 {
            position: absolute;
            width: 10%;
            top: 51.8%;
            left: 58.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .r3110202:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3110202v1 {
            position: absolute;
            top: 103%;
            left: -27.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .r3110202v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3110202v2 {
            position: absolute;
            width: 10%;
            top: 209%;
            left: -27.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .r3110202v2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .no {
            position: absolute;
            width: 10%;
            top: 51.8%;
            left: 63.8%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .rno:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .no1 {
            position: absolute;
            width: 10%;
            top: 53.6%;
            left: 63.8%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .rno1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .no2 {
            position: absolute;
            width: 10%;
            top: 55.3%;
            left: 63.8%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .rno2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .rm {
            position: absolute;
            width: 5%;
            top: 68.3%;
            left: 81.5%;
            color: black;
            font-weight: bold;
            font-size: 0.7vw;
            text-align: center;
        }


        .rm:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .power {
            position: absolute;
            width: 10%;
            top: 79.7%;
            left: 44.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .power:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .run {
            position: absolute;
            width: 10%;
            top: 82%;
            left: 44.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .run:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .power1 {
            position: absolute;
            width: 10%;
            top: 79.7%;
            left: 56.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .power1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .run1 {
            position: absolute;
            width: 10%;
            top: 82%;
            left: 56.7%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .run1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf {
            position: absolute;
            width: 10%;
            top: 28.3%;
            left: 76.8%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf1 {
            position: absolute;
            width: 10%;
            top: 155.3%;
            left: 0.8%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf2 {
            position: absolute;
            width: 10%;
            top: 309.3%;
            left: 0.8%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf3 {
            position: absolute;
            width: 10%;
            top: 28.3%;
            left: 79.8%;
            color: #7CFF00;
            font-weight: bold;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf4 {
            position: absolute;
            width: 10%;
            top: 30.8%;
            left: 79.8%;
            color: #7CFF00;
            font-weight: bold;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .lsf5 {
            position: absolute;
            width: 10%;
            top: 33.7%;
            left: 79.8%;
            color: #7CFF00;
            font-weight: bold;
            font-size: 0.7vw;
            text-align: center;
        }


        .lsf5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1 {
            position: absolute;
            width: 10%;
            top: 28.3%;
            left: 86%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .r1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2 {
            position: absolute;
            width: 10%;
            top: 30.8%;
            left: 86%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .r2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3 {
            position: absolute;
            width: 10%;
            top: 33.7%;
            left: 86%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .r4 {
            position: absolute;
            width: 10%;
            top: 28.3%;
            left: 89.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .r4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r5 {
            position: absolute;
            width: 10%;
            top: 30.8%;
            left: 89.9%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .r5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r6 {
            position: absolute;
            width: 10%;
            top: 33.7%;
            left: 89.9%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .r6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .total {
            position: absolute;
            width: 10%;
            top: 79.7%;
            left: 76.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .total:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .total1 {
            position: absolute;
            width: 10%;
            top: 82%;
            left: 76.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }


        .total1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .total2 {
            position: absolute;
            width: 10%;
            top: 79.7%;
            left: 88.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .total2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .total3 {
            position: absolute;
            width: 10%;
            top: 82%;
            left: 88.5%;
            color: #7CFF00;
            font-size: 0.7vw;
            text-align: center;
        }


        .total3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0503 {
            position: absolute;
            width: 10%;
            top: 38%;
            left: 95.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0503:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0710 {
            position: absolute;
            width: 10%;
            top: 64%;
            left: 68.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0710:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }


        .r0702 {
            position: absolute;
            width: 10%;
            top: 56.9%;
            left: 71.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0702:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0707 {
            position: absolute;
            width: 10%;
            top: 52.8%;
            left: 68%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0707:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }


        .r0401 {
            position: absolute;
            width: 10%;
            top: 48.3%;
            left: 64.1%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0401:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0407 {
            position: absolute;
            width: 10%;
            top: 36.3%;
            left: 66.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }


        .r0407:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2503 {
            position: absolute;
            width: 10%;
            top: 24.2%;
            left: 35.6%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r2503:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2501 {
            position: absolute;
            width: 10%;
            top: 27.1%;
            left: 35.6%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r2501:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2502 {
            position: absolute;
            width: 10%;
            top: 25.3%;
            left: 41%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r2502:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3904 {
            position: absolute;
            width: 10%;
            top: 53.3%;
            left: 41.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r3904:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2002 {
            position: absolute;
            width: 10%;
            top: 54.3%;
            left: 46.7%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r2002:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m2 {
            position: absolute;
            width: 10%;
            top: 54.3%;
            left: 50.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m2v1 {
            position: absolute;
            width: 10%;
            top: 62.3%;
            left: 47.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m2v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m2v2 {
            position: absolute;
            width: 10%;
            top: 36.0%;
            left: 59%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .m2v2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3151801 {
            position: absolute;
            width: 10%;
            top: 30.3%;
            left: 29.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r3151801:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r3160302 {
            position: absolute;
            width: 10%;
            top: 37.7%;
            left: 48.9%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r3160302:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1303 {
            position: absolute;
            width: 10%;
            top: 38.8%;
            left: 0.9%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .r1303:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0202 {
            position: absolute;
            width: 10%;
            top: 34%;
            left: 58.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0202:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m3 {
            position: absolute;
            width: 10%;
            top: 37.9%;
            left: 59%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .m3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0102 {
            position: absolute;
            width: 10%;
            top: 35.7%;
            left: 61.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0102:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0111 {
            position: absolute;
            width: 10%;
            top: 32.5%;
            left: 67.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0111:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0109 {
            position: absolute;
            width: 10%;
            top: 28.2%;
            left: 61.9%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0109:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0303 {
            position: absolute;
            width: 10%;
            top: 31.3%;
            left: 69.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0303:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r0406 {
            position: absolute;
            width: 10%;
            top: 39%;
            left: 66.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r0406:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r2102 {
            position: absolute;
            width: 10%;
            top: 62.7%;
            left: 42.88%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r2102:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1802 {
            position: absolute;
            width: 10%;
            top: 59.9%;
            left: 36.8%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r1802:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1803 {
            position: absolute;
            width: 10%;
            top: 59.9%;
            left: 12.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r1803:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1604 {
            position: absolute;
            width: 10%;
            top: 81.5%;
            left: 22.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r1604:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .r1604v1 {
            position: absolute;
            width: 10%;
            top: 63.7%;
            left: 21.5%;
            color: white;
            font-size: 0.5vw;
            text-align: center;
        }

        .r1604v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m3v1 {
            position: absolute;
            width: 10%;
            top: 69%;
            left: 53.2%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m3v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m5 {
            position: absolute;
            width: 10%;
            top: 81.5%;
            left: 24.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m4 {
            position: absolute;
            width: 10%;
            top: 82.8%;
            left: 22.7%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .m1 {
            position: absolute;
            width: 10%;
            top: 64.2%;
            left: 25.8%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }


        .m2v1 {
            position: absolute;
            width: 10%;
            top: 74.8%;
            left: 25.9%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .m2v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .limestone {
            position: absolute;
            width: 10%;
            top: 42.8%;
            left: 2.5%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .limestone:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .silica2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 17.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .silica2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .clay2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 66.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .clay2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .pasirbesi2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 118.2%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .pasirbesi2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .totalfresh {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 167.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .totalfresh:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .feedrate {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 216.3%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .feedrate:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .limestonev1 {
            position: absolute;
            width: 10%;
            top: 45.5%;
            left: 3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .limestonev1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .silica2v1 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 14.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .silica2v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .clay2v1 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 64.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .clay2v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .pasirbesi2v1 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 115.2%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .pasirbesi2v1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .totalfreshv1 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 162.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .totalfreshv1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .feedratev1 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 211.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .feedratev1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .limestonev2 {
            position: absolute;
            width: 10%;
            top: 48.3%;
            ;
            left: 3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .limestonev2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .silica2v2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 14.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .silica2v2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .clay2v2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 64.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .clay2v2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .pasirbesi2v2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 115.2%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .pasirbesi2v2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .totalfreshv2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 162.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .totalfreshv2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .feedratev2 {
            position: absolute;
            width: 10%;
            top: 1%;
            left: 211.3%;
            color: #7CFF00;
            font-size: 0.6vw;
            text-align: center;
        }

        .feedratev2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">

            <div class="lstone">
                <div class="square-content">
                    <span>LSTONE</span>
                </div>
            </div>
            <div class="silica">
                <div class="square-content">
                    <span>SILICA</span>
                </div>
            </div>
            <div class="pasirbesi">
                <div class="square-content">
                    <span>PASIR BESI</span>
                </div>
            </div>
            <div class="clay">
                <div class="square-content">
                    <span>CLAY</span>
                </div>
            </div>
            <div class="r0603">
                <div class="square-content">
                    <span>0603</span>
                </div>
            </div>
            <div class="r1204">
                <div class="square-content">
                    <span>1204</span>
                </div>
            </div>
            <div class="r0203">
                <div class="square-content">
                    <span>0203</span>
                </div>
            </div>

            <div class="ep">
                <div class="square-content">
                    <span>EP</span>
                </div>
            </div>
            <div class="kiln">
                <div class="square-content">
                    <span>KILN</span>
                </div>
            </div>
            <div class="value">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="r3151802">
                <div class="square-content">
                    <span>3151802</span>
                </div>
            </div>
            <div class="r3160302">
                <div class="square-content">
                    <span>3160302</span>
                </div>
            </div>
            <div class="value2">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="value3">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="value4">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="value5">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="value6">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="value7">
                <div class="square-content">
                    <span>95 A</span>
                </div>
            </div>
            <div class="value8">
                <div class="square-content">
                    <span>339,46°C</span>
                </div>
            </div>
            <div class="value9">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="value10">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="value11">
                <div class="square-content">
                    <span>3441085</span>
                </div>
            </div>
            <div class="value12">
                <div class="square-content">
                    <span>100%</span>
                </div>
            </div>
            <div class="value13">
                <div class="square-content">
                    <span>339,46°C</span>
                </div>
            </div>
            <div class="value14">
                <div class="square-content">
                    <span>339,46°C</span>
                </div>
            </div>
            <div class="value15">
                <div class="square-content">
                    <span>-5,29 mbar</span>
                </div>
            </div>
            <div class="value16">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="value17">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="value18">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="value19">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="value20">
                <div class="square-content">
                    <span>8,02 t/h</span>
                </div>
            </div>
            <div class="value21">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="value22">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="value23">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="value24">
                <div class="square-content">
                    <span>17.8 m</span>
                </div>
            </div>
            <div class="value25">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="value26">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="value27">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="value28">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="value29">
                <div class="square-content">
                    <span>M6</span>
                </div>
            </div>
            <div class="value30">
                <div class="square-content">
                    <span>M7</span>
                </div>
            </div>
            <div class="value31">
                <div class="square-content">
                    <span>0404</span>
                </div>
            </div>
            <div class="value32">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="value33">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="value34">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="value35">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="value36">
                <div class="square-content">
                    <span>M6</span>
                </div>
            </div>
            <div class="value37">
                <div class="square-content">
                    <span>M7</span>
                </div>
            </div>
            <div class="value38">
                <div class="square-content">
                    <span>TO RAW MILL SILO 02</span>
                </div>
            </div>
            <div class="value39">
                <div class="square-content">
                    <span>FROM RM 3 CIRCULATION</span>
                </div>
            </div>
            <div class="value40">
                <div class="square-content">
                    <span>TO 3161302</span>
                </div>
            </div>
            <div class="value41">
                <div class="square-content">
                    <span>BLENDING BIN 1</span>
                    <div class="value42">
                        <div class="square-content">
                            <span>BLENDING BIN 2</span>
                        </div>
                    </div>
                    <div class="value43">
                        <div class="square-content">
                            <span>BLENDING BIN 3</span>
                        </div>
                    </div>
                    <div class="value44">
                        <div class="square-content">
                            <span>BLENDING BIN 4</span>
                        </div>
                    </div>
                    <div class="value45">
                        <div class="square-content">
                            <span>BLENDING BIN 5</span>
                        </div>
                    </div>
                    <div class="value46">
                        <div class="square-content">
                            <span>BLENDING BIN 6</span>
                        </div>
                    </div>
                    <div class="value47">
                        <div class="square-content">
                            <span>BLENDING BIN 7</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="r3110202">
                <div class="square-content">
                    <span>3110202M1</span>
                    <div class="r3110202v1">
                        <div class="square-content">
                            <span>3110202M2</span>
                        </div>
                    </div>
                    <div class="r3110202v2">
                        <div class="square-content">
                            <span>3110202M3</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="no">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="no1">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="no2">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="rm">
                <div class="square-content">
                    <span>RAW MEAL SILO 3</span>
                </div>
            </div>
            <div class="power">
                <div class="square-content">
                    <span>POWER CONSUMPTION</span>
                </div>
            </div>
            <div class="run">
                <div class="square-content">
                    <span>RUN HOURS</span>
                </div>
            </div>
            <div class="power1">
                <div class="square-content">
                    <span>14.200 KWh</span>
                </div>
            </div>
            <div class="run1">
                <div class="square-content">
                    <span>23.08 hour</span>
                </div>
            </div>
            <div class="lsf">
                <div class="square-content">
                    <span>LSF</span>
                </div>
                <div class="lsf1">
                    <div class="square-content">
                        <span>SM</span>
                    </div>
                </div>
                <div class="lsf2">
                    <div class="square-content">
                        <span>AM</span>
                    </div>
                </div>
            </div>
            <div class="lsf3">
                <div class="square-content">
                    <span>115</span>
                </div>
            </div>
            <div class="lsf4">
                <div class="square-content">
                    <span>2,5</span>
                </div>
            </div>
            <div class="lsf5">
                <div class="square-content">
                    <span>1,5</span>
                </div>
            </div>
            <div class="r1">
                <div class="square-content">
                    <span>R90</span>
                </div>
            </div>
            <div class="r2">
                <div class="square-content">
                    <span>R200</span>
                </div>
            </div>
            <div class="r3">
                <div class="square-content">
                    <span>H20</span>
                </div>
            </div>
            <div class="r4">
                <div class="square-content">
                    <span>13,5 %</span>
                </div>
            </div>
            <div class="r5">
                <div class="square-content">
                    <span>3,5 %</span>
                </div>
            </div>
            <div class="r6">
                <div class="square-content">
                    <span>0,5 %</span>
                </div>
            </div>
            <div class="total">
                <div class="square-content">
                    <span>Total Prod Today </span>
                </div>
            </div>
            <div class="total1">
                <div class="square-content">
                    <span>Total Prod Yesterday </span>
                </div>
            </div>
            <div class="total2">
                <div class="square-content">
                    <span>8417,72 ton </span>
                </div>
            </div>
            <div class="total3">
                <div class="square-content">
                    <span>8502,92 ton </span>
                </div>
            </div>
            <div class="r0710">
                <div class="square-content">
                    <span>0710</span>
                </div>
            </div>
            <div class="r0503">
                <div class="square-content">
                    <span>0503</span>
                </div>
            </div>
            <div class="r0710">
                <div class="square-content">
                    <span>0710</span>
                </div>
            </div>
            <div class="r0702">
                <div class="square-content">
                    <span>0702</span>
                </div>
            </div>
            <div class="r0707">
                <div class="square-content">
                    <span>0707</span>
                </div>
            </div>
            <div class="r0401">
                <div class="square-content">
                    <span>0401</span>
                </div>
            </div>
            <div class="r0407">
                <div class="square-content">
                    <span>0407</span>
                </div>
            </div>
            <div class="r2503">
                <div class="square-content">
                    <span>2503</span>
                </div>
            </div>
            <div class="r2501">
                <div class="square-content">
                    <span>2501</span>
                </div>
            </div>
            <div class="r2502">
                <div class="square-content">
                    <span>2502</span>
                </div>
            </div>
            <div class="r3904">
                <div class="square-content">
                    <span>3904</span>
                </div>
            </div>
            <div class="r2002">
                <div class="square-content">
                    <span>2002</span>
                </div>
            </div>
            <div class="m2v1">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="m2v2">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="r3151801">
                <div class="square-content">
                    <span>3151801</span>
                </div>
            </div>
            <div class="r1303">
                <div class="square-content">
                    <span>1303</span>
                </div>
            </div>
            <div class="r0202">
                <div class="square-content">
                    <span>0202</span>
                </div>
            </div>
            <div class="m3">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="r0102">
                <div class="square-content">
                    <span>0102</span>
                </div>
            </div>
            <div class="r0111">
                <div class="square-content">
                    <span>0111</span>
                </div>
            </div>
            <div class="r0109">
                <div class="square-content">
                    <span>0109</span>
                </div>
            </div>
            <div class="r0303">
                <div class="square-content">
                    <span>0303</span>
                </div>
            </div>
            <div class="r0406">
                <div class="square-content">
                    <span>0406</span>
                </div>
            </div>
            <div class="r2102">
                <div class="square-content">
                    <span>2102</span>
                </div>
            </div>
            <div class="r1802">
                <div class="square-content">
                    <span>1802</span>
                </div>
            </div>
            <div class="r1803">
                <div class="square-content">
                    <span>1803</span>
                </div>
            </div>
            <div class="r1604">
                <div class="square-content">
                    <span>1604</span>
                </div>
            </div>
            <div class="r1604v1">
                <div class="square-content">
                    <span>1604</span>
                </div>
            </div>
            <div class="m5">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="m4">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="m1">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="m2">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="m3v1">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="limestone">
                <div class="square-content">
                    <span>LIMESTONE</span>
                    <div class="silica2">
                        <div class="square-content">
                            <span>SILICA</span>
                        </div>
                    </div>
                    <div class="clay2">
                        <div class="square-content">
                            <span>CLAY</span>
                        </div>
                    </div>
                    <div class="pasirbesi2">
                        <div class="square-content">
                            <span>PASIR BESI</span>
                        </div>
                    </div>
                    <div class="totalfresh">
                        <div class="square-content">
                            <span>TOTAL FRESH</span>
                        </div>
                    </div>
                    <div class="feedrate">
                        <div class="square-content">
                            <span>FEED RATE</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="limestonev1">
                <div class="square-content">
                    <span>98.3 t/h</span>
                    <div class="silica2v1">
                        <div class="square-content">
                            <span>0.0 t/h</span>
                        </div>
                    </div>
                    <div class="clay2v1">
                        <div class="square-content">
                            <span>21.6 t/h</span>
                        </div>
                    </div>
                    <div class="pasirbesi2v1">
                        <div class="square-content">
                            <span>0.0 t/h</span>
                        </div>
                    </div>
                    <div class="totalfreshv1">
                        <div class="square-content">
                            <span>124.6 t/h</span>
                        </div>
                    </div>
                    <div class="feedratev1">
                        <div class="square-content">
                            <span>249.9 t/h</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="limestonev2">
                <div class="square-content">
                    <span>98.7 t/h</span>
                    <div class="silica2v2">
                        <div class="square-content">
                            <span>0.0 t/h</span>
                        </div>
                    </div>
                    <div class="clay2v2">
                        <div class="square-content">
                            <span>22.1 t/h</span>
                        </div>
                    </div>
                    <div class="pasirbesi2v2">
                        <div class="square-content">
                            <span>0.0 t/h</span>
                        </div>
                    </div>
                    <div class="totalfreshv2">
                        <div class="square-content">
                            <span>120.8 t/h</span>
                        </div>
                    </div>
                    <div class="feedratev2">
                        <div class="square-content">
                            <span>169.1 t/h</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        var url = 'http://10.15.5.150/dev/par4digma/api/index.php/plant_tonasa/kiln_t5';
        $.ajax({
            url: url,
            type: 'get',
            //dataType: 'json',
            success: function(data) {
                var data1 = data.replace("<title>Json</title>", "");
                var data2 = data1.replace("(", "[");
                var data3 = data2.replace(");", "]");
                var dataJson = JSON.parse(data3);
                get_KILN_44 = parseFloat(dataJson[0].tags[44].props[0].val).toFixed(2) + " A";
                $("#get_KILN_44").html(get_KILN_44);
                tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
                tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
                tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
                tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
                tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
                tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
                tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
                tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
                $("#isisilo10").html(tonageSilo10);
                $("#isisilo11").html(tonageSilo11);
                $("#isisilo12").html(tonageSilo12);
            }
        });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>