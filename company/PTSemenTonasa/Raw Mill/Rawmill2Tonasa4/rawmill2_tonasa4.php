<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('rawmill2_tonasa4.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }

        .span1 {
            position: absolute;
            top: 22%;
            left: 7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span2 {
            position: absolute;
            top: 22%;
            left: 13.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span3 {
            position: absolute;
            top: 22%;
            left: 20.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span4 {
            position: absolute;
            top: 25.1%;
            left: 26.7%;
            width: 2%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span5 {
            position: absolute;
            top: 25.1%;
            left: 32%;
            width: 2%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span6 {
            position: absolute;
            top: 26.1%;
            left: 20.2%;
            width: 2%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span7 {
            position: absolute;
            top: 26.1%;
            left: 13.6%;
            width: 2%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span8 {
            position: absolute;
            top: 26.1%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span9 {
            position: absolute;
            top: 48.4%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span10 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span11 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span12 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span13 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span14 {
            position: absolute;
            top: 52.4%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span15 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span16 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span17 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span18 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span19 {
            position: absolute;
            top: 55.4%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span20 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span21 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span22 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span23 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span24 {
            position: absolute;
            top: 61%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span25 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span26 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span27 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span28 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span29 {
            position: absolute;
            top: 64.8%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span30 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span31 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span32 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span33 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span34 {
            position: absolute;
            top: 68%;
            left: 5.1%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span35 {
            position: absolute;
            top: 48.4%;
            left: 97%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span36 {
            position: absolute;
            top: 48.4%;
            left: 200%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span37 {
            position: absolute;
            top: 48.4%;
            left: 279.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span38 {
            position: absolute;
            top: 48.4%;
            left: 367.1%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* RAW MEAL PROD. */
        .span39 {
            position: absolute;
            top: 75%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        /* RAW MEAL PROD. */
        .span40 {
            position: absolute;
            top: 78%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span41 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span42 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span43 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span44 {
            position: absolute;
            top: 81%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span45 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span46 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span47 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span48 {
            position: absolute;
            top: 84%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span49 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span50 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span51 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span52 {
            position: absolute;
            top: 87%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span53 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span54 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span55 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */
        .span56 {
            position: absolute;
            top: 90%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span57 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span58 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span59 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /*  */

        .span60 {
            position: absolute;
            top: 92.5%;
            left: 5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span61 {
            position: absolute;
            top: 78%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span62 {
            position: absolute;
            top: 78%;
            left: 138%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span63 {
            position: absolute;
            top: 78%;
            left: 202%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        /* MAIN DRIVE */
        .span64 {
            position: absolute;
            top: 88.5%;
            left: 55.1%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span65 {
            position: absolute;
            top: 91.7%;
            left: 55%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span66 {
            position: absolute;
            top: 88.5%;
            left: 63%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span67 {
            position: absolute;
            top: 91.7%;
            left: 63%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        /* ROLLER MILL TEMP */
        .span68 {
            position: absolute;
            top: 59.7%;
            left: 57%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span69 {
            position: absolute;
            top: 62%;
            left: 57%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span70 {
            position: absolute;
            top: 59.7%;
            left: 20%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span71 {
            position: absolute;
            top: 65%;
            left: 57%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span72 {
            position: absolute;
            top: 59.7%;
            left: 20%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span73 {
            position: absolute;
            top: 68.2%;
            left: 57%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span73:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span74 {
            position: absolute;
            top: 59.7%;
            left: 20%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span74:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span75 {
            position: absolute;
            top: 71%;
            left: 57%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span75:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span76 {
            position: absolute;
            top: 59.7%;
            left: 20%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span76:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span77 {
            position: absolute;
            top: 51%;
            left: 78%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span77:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        /* GCT */
        .span78 {
            position: absolute;
            top: 54%;
            left: 78%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span78:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span79 {
            position: absolute;
            top: 51%;
            left: 7%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span79:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span80 {
            position: absolute;
            top: 51%;
            left: 48%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span80:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span81 {
            position: absolute;
            top: 51%;
            left: 98%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span81:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span82 {
            position: absolute;
            top: 56%;
            left: 78%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span82:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span83 {
            position: absolute;
            top: 51%;
            left: 7%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span83:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span84 {
            position: absolute;
            top: 51%;
            left: 49%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span84:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span85 {
            position: absolute;
            top: 51%;
            left: 98%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span85:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span86 {
            position: absolute;
            top: 58%;
            left: 78%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span86:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span87 {
            position: absolute;
            top: 51%;
            left: 7%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span87:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span88 {
            position: absolute;
            top: 51%;
            left: 58%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span88:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span89 {
            position: absolute;
            top: 51%;
            left: 98%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span89:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span90 {
            position: absolute;
            top: 60%;
            left: 78%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span90:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span91 {
            position: absolute;
            top: 51%;
            left: 7%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span91:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span92 {
            position: absolute;
            top: 51%;
            left: 58%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .span92:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span93 {
            position: absolute;
            top: 51%;
            left: 98%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span93:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span94 {
            position: absolute;
            top: 48%;
            left: 42.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span94:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span95 {
            position: absolute;
            top: 51%;
            left: 42.5%;
            width: 10%;
            text-align: left;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span95:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        /* Raw Mill Fan */
        .span96 {
            position: absolute;
            top: 11.6%;
            left: 55.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span96:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        /*  */
        .span97 {
            position: absolute;
            top: 14%;
            left: 55.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span97:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span98 {
            position: absolute;
            top: 11.6%;
            left: 35%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span98:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span99 {
            position: absolute;
            top: 11.6%;
            left: 93%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span99:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span100 {
            position: absolute;
            top: 11.6%;
            left: 158%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span100:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span101 {
            position: absolute;
            top: 17%;
            left: 55.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span101:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span102 {
            position: absolute;
            top: 11.6%;
            left: 35%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span102:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span103 {
            position: absolute;
            top: 11.6%;
            left: 98%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span103:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span104 {
            position: absolute;
            top: 11.6%;
            left: 158%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span104:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span105 {
            position: absolute;
            top: 19.8%;
            left: 55.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span105:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span106 {
            position: absolute;
            top: 11.6%;
            left: 35%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span106:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span107 {
            position: absolute;
            top: 11.6%;
            left: 97%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span107:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span108 {
            position: absolute;
            top: 11.6%;
            left: 158%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span108:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span109 {
            position: absolute;
            top: 11.6%;
            left: 81%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span109:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span110 {
            position: absolute;
            top: 11.6%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span110:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span111 {
            position: absolute;
            top: 14%;
            left: 81%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span111:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span112 {
            position: absolute;
            top: 13.6%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span112:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        /*  */
        .span113 {
            position: absolute;
            top: 16.3%;
            left: 81%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span113:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span114 {
            position: absolute;
            top: 13.6%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span114:before {
            content: "";
            display: block;
            padding-bottom: 0%;
        }

        .span115 {
            position: absolute;
            top: 37%;
            left: 36%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span115:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span116 {
            position: absolute;
            top: 57%;
            left: 44%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span116:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span117 {
            position: absolute;
            top: 73%;
            left: 44.6%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span117:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span118 {
            position: absolute;
            top: 85%;
            left: 44%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            font-size: 0.88vw;
        }

        .span118:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span119 {
            position: absolute;
            top: 81%;
            left: 68.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span119:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span120 {
            position: absolute;
            top: 98%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span120:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span121 {
            position: absolute;
            top: 48%;
            left: 73%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span121:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span122 {
            position: absolute;
            top: 27%;
            left: 71%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span122:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span123 {
            position: absolute;
            top: 27%;
            left: 77%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span123:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span124 {
            position: absolute;
            top: 29%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span124:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span125 {
            position: absolute;
            top: 42%;
            left: 91%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span125:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span126 {
            position: absolute;
            top: 11%;
            left: 94%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span126:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span127 {
            position: absolute;
            top: 35%;
            left: 75.9%;
            width: 1%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .span127:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span128 {
            position: absolute;
            top: 87%;
            left: 82%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .span128:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span129 {
            position: absolute;
            top: 90%;
            left: 82.4%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .span129:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span130 {
            position: absolute;
            top: 97%;
            left: 81.6%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span130:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span131 {
            position: absolute;
            top: 87%;
            left: 89.3%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .span131:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span132 {
            position: absolute;
            top: 90%;
            left: 89.7%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
        }

        .span132:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span133 {
            position: absolute;
            top: 97%;
            left: 89%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span133:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>140 t</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>22 t</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>10 t</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>IRON FB05</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>REJECT FB04</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>CLAY FB03</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>SILICA FB02</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>LIME STONE FB02</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>RM FEED</span>
                    <div class="span10">
                        <div class="square-content">
                            <span>LIMESTONE</span>
                        </div>
                    </div>
                    <div class="span11">
                        <div class="square-content">
                            <span>SILICA</span>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="square-content">
                            <span>CLAY</span>
                        </div>
                    </div>
                    <div class="span13">
                        <div class="square-content">
                            <span>IRON SAND</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>103 t/h</span>
                    <div class="span15">
                        <div class="square-content">
                            <span>103 t/h</span>
                        </div>
                    </div>
                    <div class="span16">
                        <div class="square-content">
                            <span>103 t/h</span>
                        </div>
                    </div>
                    <div class="span17">
                        <div class="square-content">
                            <span>1,2 t/h</span>
                        </div>
                    </div>
                    <div class="span18">
                        <div class="square-content">
                            <span>5,3 t/h</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>350 t/h</span>
                    <div class="span20">
                        <div class="square-content">
                            <span>82,2 %</span>
                        </div>
                    </div>
                    <div class="span21">
                        <div class="square-content">
                            <span>11,3 %</span>
                        </div>
                    </div>
                    <div class="span22">
                        <div class="square-content">
                            <span>1,2 %</span>
                        </div>
                    </div>
                    <div class="span23">
                        <div class="square-content">
                            <span>3,2 % </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>MANUAL SP</span>
                    <div class="span25">
                        <div class="square-content">
                            <span>82,2 %</span>
                        </div>
                    </div>
                    <div class="span26">
                        <div class="square-content">
                            <span>11,3 %</span>
                        </div>
                    </div>
                    <div class="span27">
                        <div class="square-content">
                            <span>1,2 %</span>
                        </div>
                    </div>
                    <div class="span28">
                        <div class="square-content">
                            <span>3,2 % </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>RMP SP</span>
                    <div class="span30">
                        <div class="square-content">
                            <span>82,2 %</span>
                        </div>
                    </div>
                    <div class="span31">
                        <div class="square-content">
                            <span>11,3 %</span>
                        </div>
                    </div>
                    <div class="span32">
                        <div class="square-content">
                            <span>1,2 %</span>
                        </div>
                    </div>
                    <div class="span33">
                        <div class="square-content">
                            <span>3,2 % </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>H2O (%)</span>
                    <div class="span35">
                        <div class="square-content">
                            <span>7,28 %</span>
                        </div>
                    </div>
                    <div class="span36">
                        <div class="square-content">
                            <span>8,12 %</span>
                        </div>
                    </div>
                    <div class="span37">
                        <div class="square-content">
                            <span>13,12 %</span>
                        </div>
                    </div>
                    <div class="span38">
                        <div class="square-content">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>RAW MEAL PROD.</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>Limestone</span>
                    <div class="span41">
                        <div class="square-content">
                            <span>6392,25 ton</span>
                        </div>
                    </div>
                    <div class="span42">
                        <div class="square-content">
                            <span>LSF</span>
                        </div>
                    </div>
                    <div class="span43">
                        <div class="square-content">
                            <span>115</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>Silica</span>
                    <div class="span45">
                        <div class="square-content">
                            <span>131,69 ton</span>
                        </div>
                    </div>
                    <div class="span46">
                        <div class="square-content">
                            <span>SM</span>
                        </div>
                    </div>
                    <div class="span47">
                        <div class="square-content">
                            <span>2,5</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>Clay</span>
                    <div class="span49">
                        <div class="square-content">
                            <span>1594,72 ton</span>
                        </div>
                    </div>
                    <div class="span50">
                        <div class="square-content">
                            <span>AM</span>
                        </div>
                    </div>
                    <div class="span51">
                        <div class="square-content">
                            <span>1,5</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>Iron</span>
                    <div class="span53">
                        <div class="square-content">
                            <span>51,82 ton</span>
                        </div>
                    </div>
                    <div class="span54">
                        <div class="square-content">
                            <span>R90</span>
                        </div>
                    </div>
                    <div class="span55">
                        <div class="square-content">
                            <span>13,5 %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>RM Prod Today</span>
                    <div class="span57">
                        <div class="square-content">
                            <span>8417,72 ton</span>
                        </div>
                    </div>
                    <div class="span58">
                        <div class="square-content">
                            <span>R200</span>
                        </div>
                    </div>
                    <div class="span59">
                        <div class="square-content">
                            <span>3,5 %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>RM Prod Yesterday</span>
                    <div class="span61">
                        <div class="square-content">
                            <span>8502,92 ton</span>
                        </div>
                    </div>
                    <div class="span62">
                        <div class="square-content">
                            <span>H2O</span>
                        </div>
                    </div>
                    <div class="span63">
                        <div class="square-content">
                            <span>0,5 %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>MAIN DRIVE</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">32807 kW</p>
                    <p style="margin-bottom: 1px;">23,77 hours</p>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>HYDRAULIC PRESS.</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">368,29 bar</p>
                    <p style="margin-bottom: 1px;">68,29 bar</p>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>ROLLER MILL TEMP</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>Roller 1</span>
                    <div class="span70">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>Roller 2</span>
                    <div class="span72">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span73">
                <div class="square-content">
                    <span>Roller 3</span>
                    <div class="span74">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span75">
                <div class="square-content">
                    <span>Roller 4</span>
                    <div class="span76">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span77">
                <div class="square-content">
                    <span>GCT</span>
                </div>
            </div>
            <div class="span78">
                <div class="square-content">
                    <span>Air</span>
                    <div class="span79">
                        <div class="square-content">
                            <span>2,32 mbar</span>
                        </div>
                    </div>
                    <div class="span80">
                        <div class="square-content">
                            <span>I/L</span>
                        </div>
                    </div>
                    <div class="span81">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span82">
                <div class="square-content">
                    <span>Water</span>
                    <div class="span83">
                        <div class="square-content">
                            <span>1,22 mbar</span>
                        </div>
                    </div>
                    <div class="span84">
                        <div class="square-content">
                            <span>O/L</span>
                        </div>
                    </div>
                    <div class="span85">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span86">
                <div class="square-content">
                    <span>Outlet</span>
                    <div class="span87">
                        <div class="square-content">
                            <span>1,22 mbar</span>
                        </div>
                    </div>
                    <div class="span88">
                        <div class="square-content">
                            <span>Material</span>
                        </div>
                    </div>
                    <div class="span89">
                        <div class="square-content">
                            <span>82,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span90">
                <div class="square-content">
                    <span>Flow</span>
                    <div class="span91">
                        <div class="square-content">
                            <span>26,01 m3/h</span>
                        </div>
                    </div>
                    <div class="span92">
                        <div class="square-content">
                            <span></span>
                        </div>
                    </div>
                    <div class="span93">
                        <div class="square-content">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span94">
                <div class="square-content">
                    <span>SEPARATOR</span>
                </div>
            </div>
            <div class="span95">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">68,29 rpm</p>
                    <p style="margin-bottom: 1px;">67,11 Amp</p>
                </div>
            </div>
            <div class="span96">
                <div class="square-content">
                    <span>RAW MILL FAN</span>
                </div>
            </div>
            <div class="span97">
                <div class="square-content">
                    <span>Load</span>
                    <div class="span98">
                        <div class="square-content">
                            <span>32.768 KW</span>
                        </div>
                    </div>
                    <div class="span99">
                        <div class="square-content">
                            <span>Bearing Fan</span>
                        </div>
                    </div>
                    <div class="span100">
                        <div class="square-content">
                            <span>32,46°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span101">
                <div class="square-content">
                    <span>Vibration (DE)</span>
                    <div class="span102">
                        <div class="square-content">
                            <span>0,8 mm/s</span>
                        </div>
                    </div>
                    <div class="span103">
                        <div class="square-content">
                            <span>Winding Motor</span>
                        </div>
                    </div>
                    <div class="span104">
                        <div class="square-content">
                            <span>28,42°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span105">
                <div class="square-content">
                    <span>Vibration (NDE)</span>
                    <div class="span106">
                        <div class="square-content">
                            <span>1,1 mm/s</span>
                        </div>
                    </div>
                    <div class="span107">
                        <div class="square-content">
                            <span>Bearing Motor</span>
                        </div>
                    </div>
                    <div class="span108">
                        <div class="square-content">
                            <span>22,21°C</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span109">
                <div class="square-content">
                    <span>68,29 kV</span>
                    <div class="span110">
                        <div class="square-content">
                            <span>68,29 A</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span111">
                <div class="square-content">
                    <span>68,29 kV</span>
                    <div class="span112">
                        <div class="square-content">
                            <span>68,29 A</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span113">
                <div class="square-content">
                    <span>68,29 kV</span>
                    <div class="span114">
                        <div class="square-content">
                            <span>68,29 A</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span115">
                <div class="square-content">
                    <p style="margin-bottom: 1px">144,22 A</p>
                    <p style="margin-bottom: 1px">120 rpm</p>
                </div>
            </div>
            <div class="span116">
                <div class="square-content">
                    <p style="margin-bottom: 1px">82,42°C</p>
                    <p style="margin-bottom: 1px">67,22 mbar</p>
                </div>
            </div>
            <div class="span117">
                <div class="square-content">
                    <p style="margin-bottom: 1px">339,46°C</p>
                    <p style="margin-bottom: 1px">-5,29 mbar</p>
                </div>
            </div>
            <div class="span118">
                <div class="square-content">
                    <span>2.7 mm/s</span>
                </div>
            </div>
            <div class="span119">
                <div class="square-content">
                    <span>KILN FEED</span>
                </div>
            </div>
            <div class="span120">
                <div class="square-content">
                    <span>412BE1</span>
                </div>
            </div>
            <div class="span121">
                <div class="square-content">
                    <span>72,89 A</span>
                </div>
            </div>
            <div class="span122">
                <div class="square-content">
                    <span>PREHEATER</span>
                </div>
            </div>
            <div class="span123">
                <div class="square-content">
                    <span>AIR</span>
                </div>
            </div>
            <div class="span124">
                <div class="square-content">
                    <span>WATER</span>
                </div>
            </div>
            <div class="span125">
                <div class="square-content">
                    <span>KILN FEED</span>
                </div>
            </div>
            <div class="span126">
                <div class="square-content">
                    <span>40 mg/m3</span>
                </div>
            </div>
            <div class="span127">
                <div class="square-content">
                    <span>441 CT01</span>
                </div>
            </div>
            <div class="span128">
                <div class="square-content">
                    <span>413SS01</span>
                </div>
            </div>
            <div class="span129">
                <div class="square-content">
                    <span>88,1 %</span>
                </div>
            </div>
            <div class="span130">
                <div class="square-content">
                    <span>14.770 ton</span>
                </div>
            </div>
            <div class="span131">
                <div class="square-content">
                    <span>414SS01</span>
                </div>
            </div>
            <div class="span132">
                <div class="square-content">
                    <span>81,3 %</span>
                </div>
            </div>
            <div class="span133">
                <div class="square-content">
                    <span>12.123 ton</span>
                </div>
            </div>
        </div>

</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>

</html>