<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('rm_tonasa5.png');

		}

		.pinch:before {
			content: "";
			display: block;
			padding-bottom: 54.5%;
		}

		.labells {
			position: absolute;
			top: 5%;
			left: 1%;
			width: 6.4%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labells:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelsilica {
			position: absolute;
			top: 5%;
			left: 4.1%;
			width: 10%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labelsilica:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labeliron {
			position: absolute;
			top: 5%;
			left: 9%;
			width: 10%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labeliron:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelclay {
			position: absolute;
			top: 5%;
			left: 13.9%;
			width: 10%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labelclay:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcooler {
			position: absolute;
			top: 2%;
			left: 93.5%;
			width: 10%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labelcooler:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcyclonepreheater {
			position: absolute;
			top: 7%;
			left: 91.5%;
			width: 6%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.6vw;
		}

		.labelcyclonepreheater:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531WF01 {
			position: absolute;
			top: 16.2%;
			left: 1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label531WF01 {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531WF02 {
			position: absolute;
			top: 16.2%;
			left: 6.9%;
			width: 10%;
			color: white;
			font-size: 0.6vw;
		}

		.label531WF02 {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531WF03 {
			position: absolute;
			top: 16.2%;
			left: 11.3%;
			width: 10%;
			color: white;
			font-size: 0.6vw
		}

		.label531WF03 {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531WF04 {
			position: absolute;
			top: 16.2%;
			left: 16.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw
		}

		.label531WF04 {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531SX02 {
			position: absolute;
			top: 20%;
			left: 16.9%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label531SX02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531BC02 {
			position: absolute;
			top: 22%;
			left: 24.3%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label531BC02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531BC03 {
			position: absolute;
			top: 28%;
			left: 29.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label531BC03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531BC04 {
			position: absolute;
			top: 52%;
			left: 31%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label531BC04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS01 {
			position: absolute;
			top: 30%;
			left: 40%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS02 {
			position: absolute;
			top: 30%;
			left: 52%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS03 {
			position: absolute;
			top: 33.9%;
			left: 43%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS04 {
			position: absolute;
			top: 38.9%;
			left: 56%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS05 {
			position: absolute;
			top: 43%;
			left: 66%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS05:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532AS06 {
			position: absolute;
			top: 48%;
			left: 85%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532AS06:before {

			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcn01 {
			position: absolute;
			top: 22%;
			left: 34%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.5vw;
			font-weight: bold;
		}

		.labelcn01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcn02 {
			position: absolute;
			top: 22%;
			left: 37.8%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.5vw;
			font-weight: bold;
		}

		.labelcn02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcn03 {
			position: absolute;
			top: 22%;
			left: 45.9%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.5vw;
			font-weight: bold;
		}

		.labelcn03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelcn04 {
			position: absolute;
			top: 22%;
			left: 49.6%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.5vw;
			font-weight: bold;
		}

		.labelcn04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD01 {
			position: absolute;
			top: 18%;
			left: 55.2%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532LD01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD01value {
			position: absolute;
			top: 12%;
			left: 55.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.label532LD01value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532FN01 {
			position: absolute;
			top: 18%;
			left: 62%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532FN01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532FN01value {
			position: absolute;
			top: 8%;
			left: 62.9%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.label532FN01value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532SD03 {
			position: absolute;
			top: 18%;
			left: 69%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532SD03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532SD04 {
			position: absolute;
			top: 35%;
			left: 73%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.label532SD04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelgct {
			position: absolute;
			top: 15.9%;
			left: 73.2%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.7vw;
			font-weight: bold;
		}

		.labelgct:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelep {
			position: absolute;
			top: 16.6%;
			left: 82.4%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.7vw;
			font-weight: bold;
		}

		.labelep:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelepvalue {
			position: absolute;
			top: 18%;
			left: 77.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelepvalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label533FN01 {
			position: absolute;
			top: 20%;
			left: 90%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label533FN01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label533FN01value {
			position: absolute;
			top: 22%;
			left: 89.8%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.label533FN01value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value1 {
			position: absolute;
			top: 22%;
			left: 95%;
			width: 3%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;

		}

		.waterflowtogct {
			position: absolute;
			top: 25%;
			left: 63%;
			width: 9%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.waterflowtogct:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.waterflowtogctvalue {
			position: absolute;
			top: 25%;
			left: 72%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.waterflowtogctvalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BE01 {
			position: absolute;
			top: 42%;
			left: 22%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532BE01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BE02 {
			position: absolute;
			top: 39%;
			left: 75%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.label532BE02:before {
			content: "";
			display: block;
			padding-bottom: 20%;

		}

		.label532DI01 {
			position: absolute;
			top: 55.4%;
			left: 85%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.80vw;
			font-weight: bold;
		}

		.label532DI01:before {
			content: "";
			display: block;
			padding-bottom: 20%;

		}

		.rawmealsilo532si01 {
			position: absolute;
			top: 80%;
			left: 84%;
			width: 7%;
			text-align: center;
			color: black;
			font-size: 0.80vw;
			font-weight: bold;

		}

		.rawmealsilo532si01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.rawmealsilo532si01value {
			position: absolute;
			top: 90%;
			left: 86.7%;
			width: 7%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.rawmealsilo532si01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD02 {
			position: absolute;
			top: 59%;
			left: 70%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.label532LD02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD02value {
			position: absolute;
			top: 59%;
			left: 65%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.label532LD02value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD03 {
			position: absolute;
			top: 86%;
			left: 69%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.label532LD03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532LD03value {
			position: absolute;
			top: 80%;
			left: 70%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.label532LD03value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BE03 {
			position: absolute;
			top: 51.3%;
			left: 64.5%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.label532BE03:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532DG04 {
			position: absolute;
			top: 51.6%;
			left: 57%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.label532DG04:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value2 {
			position: absolute;
			top: 82%;
			left: 60%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value3 {
			position: absolute;
			top: 78%;
			left: 48%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532WI01 {
			position: absolute;
			top: 69%;
			left: 50%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532WI01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532MD01 {
			position: absolute;
			top: 91%;
			left: 52%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.label532MD01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532MD01value {
			position: absolute;
			top: 91%;
			left: 57%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.label532MD01value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value4 {
			position: absolute;
			top: 94%;
			left: 40.6%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.value4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BC02 {
			position: absolute;
			top: 93%;
			left: 39%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.label532BC02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BC02value {
			position: absolute;
			top: 93%;
			left: 26%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.label532BC02value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label344HS01 {
			position: absolute;
			top: 80%;
			left: 34%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.label:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label344HS01value {
			position: absolute;
			top: 84%;
			left: 34%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.label344HS01value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532BC01 {
			position: absolute;
			top: 77%;
			left: 30%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.label532BC01:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelrejectmateral {
			position: absolute;
			top: 70.5%;
			left: 26.8%;
			width: 4%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.labelrejectmateral:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label532DG02 {
			position: absolute;
			top: 60%;
			left: 33%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.label532DG02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label531BC04value {
			position: absolute;
			top: 47%;
			left: 28%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.label531BC04value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value5 {
			position: absolute;
			top: 54%;
			left: 35%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.value5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value6 {
			position: absolute;
			top: 52%;
			left: 44.5%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.value6:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value7 {
			position: absolute;
			top: 52%;
			left: 42%;
			width: 10%;
			text-align: right;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.value7:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.rawmill {
			position: absolute;
			top: 81.5%;
			left: 42.6%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.99vw;
			font-weight: bold;

		}

		.rawmill:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.rawmillvalue {
			position: absolute;
			top: 85%;
			left: 43.9%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;

		}

		.rawmillvalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value8 {
			position: absolute;
			top: 69%;
			left: 35%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.value8:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.tablerawmealproduct {
			position: absolute;
			top: 29%;
			left: 88%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.tablerawmealproduct:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.tablerawmealproductleft {
			position: absolute;
			top: 34%;
			left: 89%;
			width: 10%;
			/* text-align: center; */
			color: white;
			font-size: 0.60vw;
		}

		.tablerawmealproductleft:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.tablerawmealproductright {
			position: absolute;
			top: 34%;
			left: 94%;
			width: 10%;
			/* text-align: center; */
			color: #7CFF00;
			font-size: 0.60vw;
		}

		.tablerawmealproductright:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelrawmillfeed {
			position: absolute;
			top: 31%;
			left: 1.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
			;
		}

		.labelrawmillfeed:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelpv {
			position: absolute;
			top: 36%;
			left: 6%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelpv:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelsv {
			position: absolute;
			top: 35%;
			left: 1.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelsv:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelqcx {
			position: absolute;

			left: 73.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelqcx:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}



		.labells1 {
			position: absolute;
			top: 41%;
			left: 2.5%;
			width: 20%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labells1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labells1value1 {
			position: absolute;
			top: 41%;
			left: -29%;
			width: 20%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labells1value1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labells1value2 {
			position: absolute;
			left: -6%;
			top: 21%;
			width: 20%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labells1value2:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labells1value3 {
			position: absolute;
			top: 21%;
			left: 25%;
			width: 20%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labells1value3:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelclay1 {
			position: absolute;
			top: 46%;
			left: 2.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelclay1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelclay1value1 {
			position: absolute;
			top: 46%;
			left: -7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelclay1value1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelclay1value2 {
			position: absolute;
			top: 46%;
			left: 39%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelclay1value2:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelclay1value3 {
			position: absolute;
			top: 46%;
			left: 100%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelclay1value3:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labeliron1 {
			position: absolute;
			top: 51%;
			left: 2.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labeliron1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labeliron1value1 {
			position: absolute;
			top: 51%;
			left: -6%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labeliron1value1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labeliron1value2 {
			position: absolute;
			top: 51%;
			left: 40%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labeliron1value2:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labeliron1value3 {
			position: absolute;
			top: 51%;
			left: 100%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labeliron1value3:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}


		.labelsilica1 {
			position: absolute;
			top: 56%;
			left: 2.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;

		}

		.labelsilica1:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.labelsilica1value1 {
			position: absolute;
			top: 56%;
			left: -6%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			;
		}

		.labelsilica1value1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelsilica1value2 {
			position: absolute;
			top: 56%;
			left: 41%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelsilica1value2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelsilica1value3 {
			position: absolute;
			top: 56%;
			left: 100%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.labelsilica1value3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroPrecipitator {
			position: absolute;
			top: 66%;
			left: 3%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 1vw;
		}

		.labelElectroPrecipitator:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltage {
			position: absolute;
			top: 69%;
			left: -33%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}


		.labelElectroVoltage:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue1 {
			position: absolute;
			top: 103%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue2 {
			position: absolute;
			top: 193%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}



		.labelElectroVoltageValue2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue3 {
			position: absolute;
			top: 283%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue4 {
			position: absolute;
			top: 373%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.labelElectroVoltageValue4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue5 {
			position: absolute;
			top: 463%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue6 {
			position: absolute;
			top: 553%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue6:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue7 {
			position: absolute;
			top: 643%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue7:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroVoltageValue8 {
			position: absolute;
			top: 733%;
			left: -3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroVoltageValue8:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}


		.labelElectroCurrent {
			position: absolute;
			top: 68%;
			left: 31%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.labelElectroCurrent:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue1 {
			position: absolute;
			top: 103%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue2 {
			position: absolute;
			top: 193%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue3 {
			position: absolute;
			top: 283%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue3:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue4 {
			position: absolute;
			top: 373%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue4:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue5 {
			position: absolute;
			top: 463%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue5:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue6 {
			position: absolute;
			top: 553%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue6:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue7 {
			position: absolute;
			top: 643%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue7:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelElectroCurrentValue8 {
			position: absolute;
			top: 733%;
			left: -1%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}


		.labelElectroCurrentValue8:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}

		.square-box:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="labells">
				<div class="square-content">
					<p style="margin-bottom: 4px ;"> L/S</p>
					<p>B101</p>
				</div>
			</div>
			<div class="labelsilica">
				<div class="square-content">
					<p style="margin-bottom: 4px ;">Silica</p>
					<p>B102</p>
				</div>
			</div>
			<div class="labeliron">
				<div class="square-content">
					<p style="margin-bottom: 4px ;">Iron</p>
					<p>B103</p>
				</div>
			</div>
			<div class="labelclay">
				<div class="square-content">
					<p style="margin-bottom: 4px ;">Clay</p>
					<p>B104</p>
				</div>
			</div>
			<div class="labelcooler">
				<div class="square-content">
					<span>Cooler</span>
				</div>
			</div>
			<div class="labelcyclonepreheater">
				<div class="square-content">
					<span>cyclone preheater</span>
				</div>
			</div>
			<div class="label531WF01">
				<div class="square-content">
					<span>531WF01</span>
				</div>
			</div>
			<div class="label531WF02">
				<div class="square-content">
					<span>531WF02</span>
				</div>
			</div>
			<div class="label531WF03">
				<div class="square-content">
					<span>531WF03</span>
				</div>
			</div>
			<div class="label531WF04">
				<div class="square-content">
					<span>531WF04</span>
				</div>
			</div>
			<div class="label531SX02">
				<div class="square-content">
					<span>531SX02</span>
				</div>
			</div>
			<div class="label531BC02">
				<div class="square-content">
					<span>531BC02</span>
				</div>
			</div>
			<div class="label531BC03">
				<div class="square-content">
					<span>531BC03</span>
				</div>
			</div>
			<div class="label531BC04">
				<div class="square-content">
					<span>531BC04</span>
				</div>
			</div>
			<div class="label532AS01">
				<div class="square-content">
					<span>532AS01</span>
				</div>
			</div>
			<div class="label532AS02">
				<div class="square-content">
					<span>532AS02</span>
				</div>
			</div>
			<div class="label532AS03">
				<div class="square-content">
					<span>532AS03</span>
				</div>
			</div>
			<div class="label532AS04">
				<div class="square-content">
					<span>532AS04</span>
				</div>
			</div>
			<div class="label532AS05">
				<div class="square-content">
					<span>532AS05</span>
				</div>
			</div>
			<div class="labelcn01">
				<div class="square-content">
					<span>CN01</span>
				</div>
			</div>
			<div class="labelcn02">
				<div class="square-content">
					<span>CN02</span>
				</div>
			</div>
			<div class="labelcn03">
				<div class="square-content">
					<span>CN03</span>
				</div>
			</div>
			<div class="labelcn04">
				<div class="square-content">
					<span>CN04</span>
				</div>
			</div>
			<div class="label532LD01">
				<div class="square-content">
					<span>532LD01</span>
				</div>
			</div>
			<div class="label532LD01value">
				<div class="square-content">
					<span>66,2% </span>
				</div>
			</div>
			<div class="label532FN01">
				<div class="square-content">
					<span>532FN01</span>
				</div>
			</div>
			<div class="label532FN01value">
				<div class="square-content">
					<p style="margin-bottom: 1px ;">559 A</p>
					<p style="margin-bottom: 1px ;">5679 KW</p>
				</div>
			</div>
			<div class="label532SD03">
				<div class="square-content">
					<span>532SD03</span>
				</div>
			</div>
			<div class="labelgct">
				<div class="square-content">
					<span>GCT</span>
				</div>
			</div>
			<div class="labelep">
				<div class="square-content">
					<span>EP</span>
				</div>
			</div>
			<div class="labelepvalue">
				<div class="square-content">
					<span>144°C</span>
				</div>
			</div>
			<div class="label533FN01">
				<div class="square-content">
					<span>533FN01</span>
				</div>
			</div>
			<div class="label533FN01value">
				<div class="square-content">
					<span>1400 rpms</span>
				</div>
			</div>
			<div class="value1">
				<div class="square-content">
					<span>46,66 mg/m3</span>
				</div>
			</div>
			<div class="waterflowtogct">
				<div class="square-content">
					<span>Water Flow To GCT : </span>
				</div>
			</div>
			<div class="waterflowtogctvalue">
				<div class="square-content">
					<span>1,91 m3/h </span>
				</div>
			</div>
			<div class="label532SD04">
				<div class="square-content">
					<span>532SD04</span>
				</div>
			</div>
			<div class="label532AS06">
				<div class="square-content">
					<span>532AS06</span>
				</div>
			</div>
			<div class="label532BE01">
				<div class="square-content">
					<span>532BE01</span>
				</div>
			</div>
			<div class="label532BE02">
				<div class="square-content">
					<span>532BE02</span>
				</div>
			</div>
			<div class="label532DI01">
				<div class="square-content">
					<span>532DI01</span>
				</div>
			</div>
			<div class="rawmealsilo532si01">
				<div class="square-content">
					<span>Raw Meal Silo 532SI01</span>
				</div>
			</div>
			<div class="rawmealsilo532si01value">
				<div class="square-content">
					<span>66 %</span>
				</div>
			</div>
			<div class="label532LD02">
				<div class="square-content">
					<span>532LD02</span>
				</div>
			</div>
			<div class="label532LD02value">
				<div class="square-content">
					<span>66,2%</span>
				</div>
			</div>
			<div class="label532LD03">
				<div class="square-content">
					<span>532LD02</span>
				</div>
			</div>
			<div class="label532LD03value">
				<div class="square-content">
					<span>91,2%</span>
				</div>
			</div>
			<div class="label532BE03">
				<div class="square-content">
					<span>532BE03</span>
				</div>
			</div>
			<div class="label532DG04">
				<div class="square-content">
					<span>532DG04</span>
				</div>
			</div>
			<div class="value2">
				<div class="square-content">
					<span>-5,6 bar</span>
				</div>
			</div>
			<div class="value3">
				<div class="square-content">
					<p style="margin-bottom: 1px;">0,4 mm/s</p>
					<p>307°C</p>
				</div>
			</div>
			<div class="label532WI01">
				<div class="square-content">
					<p style="margin-bottom: 1px;">532WI01</p>
					<p style="color:#7CFF00;">41,99 m3/h</p>
				</div>
			</div>
			<div class="label532MD01">
				<div class="square-content">
					<span>532MD01</span>
				</div>
			</div>
			<div class="label532MD01value">
				<div class="square-content">
					<span>4964 KW</span>
				</div>
			</div>
			<div class="value4">
				<div class="square-content">
					<p style="margin-bottom: 1px;">1,0 mm/s</p>
					<p>0,8 mm/s</p>
				</div>
			</div>
			<div class="label532BC02">
				<div class="square-content">
					<span>532BC02</span>
				</div>
			</div>
			<div class="label532BC02value">
				<div class="square-content">
					<span>49%</span>
				</div>
			</div>
			<div class="label344HS01">
				<div class="square-content">
					<span>344HS01</span>
				</div>
			</div>
			<div class="label344HS01value">
				<div class="square-content">
					<span>96 bar</span>
				</div>
			</div>
			<div class="label532BC01">
				<div class="square-content">
					<span>532BC01</span>
				</div>
			</div>
			<div class="labelrejectmateral">
				<div class="square-content">
					<span>Reject Material</span>
				</div>
			</div>
			<div class="label532DG02">
				<div class="square-content">
					<span>532DG02</span>
				</div>
			</div>
			<div class="label531BC04value">
				<div class="square-content">
					<p style="margin-bottom: 1px;">587 t/h</p>
					<p>600 t/h</p>
				</div>
			</div>
			<div class="value5">
				<div class="square-content">
					<p style="margin-bottom: 1px">-68,6 bar</p>
					<p>99°C</p>
				</div>
			</div>
			<div class="value6">
				<div class="square-content">
					<span>76 %</span>
				</div>
			</div>
			<div class="value7">
				<div class="square-content">
					<p style="margin-bottom: 1px;">264 A</p>
					<p style="margin-bottom: 1px;">206 KW</p>
					<p>1386 rpm</p>
				</div>
			</div>
			<div class="rawmill">
				<div class="square-content">
					<span>RAW MILL</span>
				</div>
			</div>
			<div class="rawmillvalue">
				<div class="square-content">
					<span>61,3 mbar</span>
				</div>
			</div>
			<div class="value8">
				<div class="square-content">
					<span>-51,6 bar</span>
				</div>
			</div>
			<div class="tablerawmealproduct">
				<div class="square-content">
					<span>Raw Meal Product</span>
				</div>
			</div>
			<div class="tablerawmealproductleft">
				<div class="square-content">
					<p style="margin-bottom: 2px;">LSF</p>
					<p style="margin-bottom: 2px;">SM</p>
					<p style="margin-bottom: 2px;">AM</p>
					<p style="margin-bottom: 2px;">Mesh</p>
				</div>
			</div>
			<div class="tablerawmealproductright">
				<div class="square-content">
					<p style="margin-bottom: 2px;">102,97</p>
					<p style="margin-bottom: 2px;">2,18</p>
					<p style="margin-bottom: 2px;">1,62</p>
					<p style="margin-bottom: 2px;">17,12</p>
				</div>
			</div>
			<div class="labelrawmillfeed">
				<div class="square-content">
					<span>RAW MILL FEED</span>
				</div>
			</div>
			<div class="labelpv">
				<div class="square-content">
					<span>Pv</span>
					<div class="labelsv">
						<span>Sv</span>
						<div class="labelqcx">
							<span>QCX</span>
						</div>
					</div>
				</div>
			</div>
			<div class="labells1">
				<div class="square-content">
					<span>L/S</span>
					<div class="labells1value1">
						<span>103 t/h</span>
					</div>
					<div class="labells1value2">
						<span>106 t/h</span>
					</div>
					<div class="labells1value3">
						<span>17,8 %</span>
					</div>
				</div>
			</div>
			<div class="labelclay1">
				<div class="square-content">
					<span>Clay</span>
					<div class="labelclay1value1">
						<span>461 t/h</span>
					</div>
					<div class="labelclay1value2">
						<span>472 t/h</span>
					</div>
					<div class="labelclay1value3">
						<span>78,8 %</span>
					</div>
				</div>
			</div>
			<div class="labeliron1">
				<div class="square-content">
					<span>Iron</span>
					<div class="labeliron1value1">
						<span>8,6 t/h</span>
					</div>
					<div class="labeliron1value2">
						<span>8,7 t/h</span>
					</div>
					<div class="labeliron1value3">
						<span>1,4 %</span>
					</div>
				</div>
			</div>
			<div class="labelsilica1">
				<div class="square-content">
					<span>Silica</span>
					<div class="labelsilica1value1">
						<span>12 t/h</span>
					</div>
					<div class="labelsilica1value2">
						<span>12 t/h</span>
					</div>
					<div class="labelsilica1value3">
						<span>2,0 %</span>
					</div>

				</div>
			</div>

			<div class="labelElectroPrecipitator">
				<div class="square-content">
					<div class="labelElectroVoltage">
						<span>Voltage </span>
						<div class="labelElectroVoltageValue1">
							<span>85 KV</span>
						</div>
						<div class="labelElectroVoltageValue2">
							<span>87 KV</span>
						</div>
						<div class="labelElectroVoltageValue3">
							<span>90 KV</span>
						</div>
						<div class="labelElectroVoltageValue4">
							<span>85 KV</span>
						</div>
						<div class="labelElectroVoltageValue5">
							<span>77 KV</span>
						</div>
						<div class="labelElectroVoltageValue6">
							<span>68 KV</span>
						</div>
						<div class="labelElectroVoltageValue7">
							<span>91 KV</span>
						</div>
						<div class="labelElectroVoltageValue8">
							<span>83 KV</span>
						</div>
					</div>
					<div class="labelElectroCurrent">
						<span>Current</span>
						<div class="labelElectroCurrentValue1">
							<span>1012 mA</span>
						</div>
						<div class="labelElectroCurrentValue2">
							<span>998 mA</span>
						</div>
						<div class="labelElectroCurrentValue3">
							<span>1001 mA</span>
						</div>
						<div class="labelElectroCurrentValue4">
							<span>889 mA</span>
						</div>
						<div class="labelElectroCurrentValue5">
							<span>1007 mA</span>
						</div>
						<div class="labelElectroCurrentValue6">
							<span>988 mA</span>
						</div>
						<div class="labelElectroCurrentValue7">
							<span>977 mA</span>
						</div>
						<div class="labelElectroCurrentValue8">
							<span>1200 mA</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<!-- <body>
	<div class="wrapper wrapper-content">
		<div class="container">
			<div class="row">
				<div class="penuh">
					<div class="pinch">
						<div class="cargo">
							<div class="silo10">
								<div class="square-content">
									<span id="isisilo10"></span>
								</div>
							</div>
							<div class="silo11">
								<div class="square-content">
									<span id="isisilo11"></span>
								</div>
							</div>
							<div class="silo12">
								<div class="square-content">
									<span id="isisilo12"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		// var url = 'ISIKAN URL API';
		// $.ajax({
		// 	url: url,
		// 	type: 'get',
		// 	// dataType: 'json',
		// 	success: function(data) {
		// 		var data1 = data.replace("<title>Json</title>", "");
		// 		var data2 = data1.replace("(", "[");
		// 		var data3 = data2.replace(");", "]");
		// 		var dataJson = JSON.parse(data3);
		// 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
		// 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
		// 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
		// 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
		// 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
		// 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
		// 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
		// 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
		// 		$("#isisilo10").html(tonageSilo10);
		// 		$("#isisilo11").html(tonageSilo11);
		// 		$("#isisilo12").html(tonageSilo12);
		// 	}
		// });
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>