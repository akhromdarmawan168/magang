<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('kiln tonasa 3 23.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;

        }

        .span1 {
            position: absolute;
            top: 23.5%;
            left: 2.7%;
            width: 6%;
            font-weight: bold;
            text-align: center;
            color: #333;
            font-size: 0.7vw;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span2 {
            position: absolute;
            top: 19.5%;
            left: 8.7%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span3 {
            position: absolute;
            top: 16%;
            left: 15.7%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span4 {
            position: absolute;
            top: 16%;
            left: 25.7%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span5 {
            position: absolute;
            top: 22%;
            left: 24%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span6 {
            position: absolute;
            top: 25.3%;
            left: 21.3%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span7 {
            position: absolute;
            top: 31%;
            left: 10%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span8 {
            position: absolute;
            top: 33.5%;
            left: 22.2%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span9 {
            position: absolute;
            top: 35.5%;
            left: 19%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span10 {
            position: absolute;
            top: 37.5%;
            left: 7.5%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span11 {
            position: absolute;
            top: 39.5%;
            left: 7.5%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span12 {
            position: absolute;
            top: 51.5%;
            left: 4.5%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span13 {
            position: absolute;
            top: 48.5%;
            left: 13.5%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span14 {
            position: absolute;
            top: 48.5%;
            left: 13.5%;
            width: 6%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span15 {
            position: absolute;
            top: 59.5%;
            left: 7.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span16 {
            position: absolute;
            top: 59.5%;
            left: 14.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span17 {
            position: absolute;
            top: 62.5%;
            left: 14.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span18 {
            position: absolute;
            top: 71.5%;
            left: 8.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span19 {
            position: absolute;
            top: 73.5%;
            left: 8.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span20 {
            position: absolute;
            top: 76.5%;
            left: 6%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span21 {
            position: absolute;
            top: 78.7%;
            left: 11.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span22 {
            position: absolute;
            top: 81.7%;
            left: 11.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span23 {
            position: absolute;
            top: 72.7%;
            left: 19%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span24 {
            position: absolute;
            top: 84.7%;
            left: 19%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span25 {
            position: absolute;
            top: 66.7%;
            left: 37%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span26 {
            position: absolute;
            top: 69%;
            left: 37%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span27 {
            position: absolute;
            top: 72%;
            left: 37%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span28 {
            position: absolute;
            top: 69%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span29 {
            position: absolute;
            top: 66.8%;
            left: 52%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span30 {
            position: absolute;
            top: 68.4%;
            left: 52%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span31 {
            position: absolute;
            top: 69%;
            left: 58%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span32 {
            position: absolute;
            top: 71%;
            left: 58%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span33 {
            position: absolute;
            top: 73%;
            left: 57%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span34 {
            position: absolute;
            top: 70.3%;
            left: 62.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span35 {
            position: absolute;
            top: 63.3%;
            left: 64%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span36 {
            position: absolute;
            top: 62.3%;
            left: 65.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span37 {
            position: absolute;
            top: 69.3%;
            left: 68%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span38 {
            position: absolute;
            top: 74.3%;
            left: 68%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span39 {
            position: absolute;
            top: 83.3%;
            left: 68.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span40 {
            position: absolute;
            top: 82.3%;
            left: 64%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span41 {
            position: absolute;
            top: 91.8%;
            left: 37%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span42 {
            position: absolute;
            top: 88%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span43 {
            position: absolute;
            top: 90%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span44 {
            position: absolute;
            top: 88.3%;
            left: 50.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span45 {
            position: absolute;
            top: 93.3%;
            left: 50.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span46 {
            position: absolute;
            top: 85.3%;
            left: 56.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span47 {
            position: absolute;
            top: 93%;
            left: 55.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span48 {
            position: absolute;
            top: 88%;
            left: 58.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span49 {
            position: absolute;
            top: 90%;
            left: 58.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span50 {
            position: absolute;
            top: 90.3%;
            left: 61.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span51 {
            position: absolute;
            top: 90.3%;
            left: 68.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span52 {
            position: absolute;
            top: 38.5%;
            left: 23.6%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span53 {
            position: absolute;
            top: 44.5%;
            left: 25%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span54 {
            position: absolute;
            top: 48.8%;
            left: 24%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span55 {
            position: absolute;
            top: 51.5%;
            left: 25.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span56 {
            position: absolute;
            top: 54.3%;
            left: 24%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span57 {
            position: absolute;
            top: 54.3%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span58 {
            position: absolute;
            top: 67.3%;
            left: 21.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span59 {
            position: absolute;
            top: 43.6%;
            left: 34%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span60 {
            position: absolute;
            top: 35%;
            left: 36%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span61 {
            position: absolute;
            top: 38%;
            left: 36%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span62 {
            position: absolute;
            top: 44.6%;
            left: 34%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span63 {
            position: absolute;
            top: 43.6%;
            left: 41.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span64 {
            position: absolute;
            top: 48.5%;
            left: 41.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span65 {
            position: absolute;
            top: 48.5%;
            left: 43.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span66 {
            position: absolute;
            top: 48.5%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span67 {
            position: absolute;
            top: 22.5%;
            left: 36%;
            width: 4%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span68 {
            position: absolute;
            top: 42.2%;
            left: 45.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span69 {
            position: absolute;
            top: 35.2%;
            left: 45.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span70 {
            position: absolute;
            top: 30.2%;
            left: 45.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span71 {
            position: absolute;
            top: 25.2%;
            left: 46.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span72 {
            position: absolute;
            top: 30.2%;
            left: 62.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span73 {
            position: absolute;
            top: 17.2%;
            left: 67.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span73:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span74 {
            position: absolute;
            top: 21%;
            left: 65.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span74:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span75 {
            position: absolute;
            top: 24%;
            left: 65.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span75:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span76 {
            position: absolute;
            top: 31.2%;
            left: 64%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span76:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span78 {
            position: absolute;
            top: 30.2%;
            left: 56.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span78:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span79 {
            position: absolute;
            top: 35.9%;
            left: 57.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span79:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span80 {
            position: absolute;
            top: 39.4%;
            left: 57.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span80:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span81 {
            position: absolute;
            top: 39.4%;
            left: 61%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span81:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span82 {
            position: absolute;
            top: 45.4%;
            left: 61%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span82:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span83 {
            position: absolute;
            top: 49%;
            left: 57%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span83:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span84 {
            position: absolute;
            top: 54%;
            left: 50%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span84:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span85 {
            position: absolute;
            top: 59%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span85:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span86 {
            position: absolute;
            top: 66%;
            left: 42%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span86:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span87 {
            position: absolute;
            top: 59%;
            left: 55%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span87:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span88 {
            position: absolute;
            top: 48.2%;
            left: 67.8%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span88:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span89 {
            position: absolute;
            top: 58.2%;
            left: 66%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span89:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span90 {
            position: absolute;
            top: 60.3%;
            left: 68%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span90:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span91 {
            position: absolute;
            top: 31.4%;
            left: 78.5%;
            width: 6%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span91:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span92 {
            position: absolute;
            top: 31.4%;
            left: 84%;
            width: 5%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span92:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span93 {
            position: absolute;
            top: 39%;
            left: 89.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span93:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span94 {
            position: absolute;
            top: 38.9%;
            left: 92.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span94:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span95 {
            position: absolute;
            top: 43.9%;
            left: 96.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span95:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span96 {
            position: absolute;
            top: 45.4%;
            left: 89.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span96:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span97 {
            position: absolute;
            top: 47.4%;
            left: 84.3%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span97:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span98 {
            position: absolute;
            top: 58.1%;
            left: 78.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span98:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span99 {
            position: absolute;
            top: 58.1%;
            left: 84.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span99:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span100 {
            position: absolute;
            top: 59.1%;
            left: 95.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span100:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span101 {
            position: absolute;
            top: 62.1%;
            left: 84.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span101:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span102 {
            position: absolute;
            top: 63.1%;
            left: 77.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span102:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span103 {
            position: absolute;
            top: 70.1%;
            left: 93.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span103:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span104 {
            position: absolute;
            top: 72.8%;
            left: 93.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span104:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span105 {
            position: absolute;
            top: 75.8%;
            left: 96%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span105:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span106 {
            position: absolute;
            top: 75.8%;
            left: 74%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span106:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span106 {
            position: absolute;
            top: 75.8%;
            left: 74%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span106:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span107 {
            position: absolute;
            top: 81.8%;
            left: 77%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span107:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span108 {
            position: absolute;
            top: 81.8%;
            left: 84%;
            width: 4%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span108:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span109 {
            position: absolute;
            top: 81.8%;
            left: 92%;
            width: 4%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }

        .span109:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span110 {
            position: absolute;
            top: 87.8%;
            left: 80%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span110:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span111 {
            position: absolute;
            top: 89%;
            left: 85.8%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span111:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span112 {
            position: absolute;
            top: 89%;
            left: 93.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span112:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span113 {
            position: absolute;
            top: 90.5%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .span113:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span114 {
            position: absolute;
            top: 90.5%;
            left: 4%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span114:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span115 {
            position: absolute;
            top: 1%;
            left: 18%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span115:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span116 {
            position: absolute;
            top: 93%;
            left: 4%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span116:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span117 {
            position: absolute;
            top: 1%;
            left: 18%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span117:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span118 {
            position: absolute;
            top: 14%;
            left: 44.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span118:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span119 {
            position: absolute;
            top: 1%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span119:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span120 {
            position: absolute;
            top: 16%;
            left: 44.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span120:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }

        .span121 {
            position: absolute;
            top: 1%;
            left: 30%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            font-size: 0.6vw;
        }

        .span121:before {
            content: "";
            display: block;
            padding-bottom: 1%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>RAW MEAL SILO 3</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>25.2 m</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>214 °C</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>-53.5 mbar</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>6 %</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>3160502</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>423 °C</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>440 °C</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>-28.8 mbar</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>666 °C</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>664 °C</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>827 °C</span>
                </div>
            </div>
            <!-- <div class="span14">
                <div class="square-content">
                    <span>827 °C</span>
                </div>
            </div> -->
            <div class="span15">
                <div class="square-content">
                    <span>817 °C</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>-60.0 mbar</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>784 °C</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>840 °C</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>-15.6 mbar</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>-3.4 mbar</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>784 °C</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>-3.9 mbar</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>3.3 mm</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>18.6 bar</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>1.75 rpm</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>1.75 rpm</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>618. A</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>366 mbar</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>92.9 mbar</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>13.9 mbar</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>83.4 A</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>218 mbar</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>0 °C</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>0802M2</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>0807</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>15.5 t/h</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>TO TONASA2</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>64 °C</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>"" °C</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>38.2 A</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>0604</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>3172503</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>75 °C</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>79 °C</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>1306</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>1312</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>-65.3 mbar</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>237 °C</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>3160601</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>45 °C</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>3160602</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>TO 3160801</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>0302</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>100 %</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>100 %</span>
                </div>
            </div>
            <div class="span62">
                <div class="square-content">
                    <P style="margin-bottom: 1px;">46 °C</P>
                    <P style="margin-bottom: 1px;">1091 A</P>
                    <P style="margin-bottom: 1px;">999 rpm</P>
                </div>
            </div>
            <div class="span63">
                <div class="square-content">
                    <span>0301</span>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>TO R.MILL</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <span>FROM R.MILL</span>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>1801</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>1803</span>
                </div>
            </div>
            <div class="span70">
                <div class="square-content">
                    <span>1802</span>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>114 °C</span>
                </div>
            </div>
            <div class="span72">
                <div class="square-content">
                    <span>1002</span>
                </div>
            </div>
            <div class="span73">
                <div class="square-content">
                    <span>69 %</span>
                </div>
            </div>
            <div class="span74">
                <div class="square-content">
                    <span>-2.5 mbar</span>
                </div>
            </div>
            <div class="span75">
                <div class="square-content">
                    <span>-2.5 mbar</span>
                </div>
            </div>
            <div class="span76">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">335 A</p>
                    <p style="margin-bottom: 1px;">53 %</p>
                </div>
            </div>
            <div class="span78">
                <div class="square-content">
                    <span>3160101</span>
                </div>
            </div>
            <div class="span79">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span80">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span81">
                <div class="square-content">
                    <span>3160202</span>
                </div>
            </div>
            <div class="span82">
                <div class="square-content">
                    <span>3160301</span>
                </div>
            </div>
            <div class="span83">
                <div class="square-content">
                    <span>3160302</span>
                </div>
            </div>
            <div class="span84">
                <div class="square-content">
                    <span>3160302</span>
                </div>
            </div>
            <div class="span85">
                <div class="square-content">
                    <span>3161402</span>
                </div>
            </div>
            <div class="span86">
                <div class="square-content">
                    <span>TO 3161501</span>
                </div>
            </div>
            <div class="span87">
                <div class="square-content">
                    <span>TO 3102102</span>
                </div>
            </div>
            <div class="span88">
                <div class="square-content">
                    <span>1363 ltr</span>
                </div>
            </div>
            <div class="span89">
                <div class="square-content">
                    <span>0807</span>
                </div>
            </div>
            <div class="span90">
                <div class="square-content">
                    <span>0.0 m3/h</span>
                </div>
            </div>
            <div class="span91">
                <div class="square-content">
                    <span>FROM CL.CRUSHER</span>
                </div>
            </div>
            <div class="span92">
                <div class="square-content">
                    <span>FROM TONASA 2</span>
                </div>
            </div>
            <div class="span93">
                <div class="square-content">
                    <span>40.0 A</span>
                </div>
            </div>
            <div class="span94">
                <div class="square-content">
                    <span>2503</span>
                </div>
            </div>
            <div class="span95">
                <div class="square-content">
                    <span>4104</span>
                </div>
            </div>
            <div class="span96">
                <div class="square-content">
                    <span>1701</span>
                </div>
            </div>
            <div class="span97">
                <div class="square-content">
                    <span>0.0 A</span>
                </div>
            </div>
            <div class="span98">
                <div class="square-content">
                    <span>2700</span>
                </div>
            </div>
            <div class="span99">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="span100">
                <div class="square-content">
                    <span>21703</span>
                </div>
            </div>
            <div class="span101">
                <div class="square-content">
                    <span>3313</span>
                </div>
            </div>
            <div class="span102">
                <div class="square-content">
                    <span>21722</span>
                </div>
            </div>
            <div class="span103">
                <div class="square-content">
                    <span>21706</span>
                </div>
            </div>
            <div class="span104">
                <div class="square-content">
                    <span>3003</span>
                </div>
            </div>
            <div class="span105">
                <div class="square-content">
                    <span>3104</span>
                </div>
            </div>
            <div class="span106">
                <div class="square-content">
                    <span>3603</span>
                </div>
            </div>
            <div class="span107">
                <div class="square-content">
                    <span>LSB</span>
                </div>
            </div>
            <div class="span108">
                <div class="square-content">
                    <span>CLINKER SILO 2</span>
                </div>
            </div>
            <div class="span109">
                <div class="square-content">
                    <span>CLINKER SILO 3</span>
                </div>
            </div>
            <div class="span110">
                <div class="square-content">
                    <span>6.02 M</span>
                </div>
            </div>
            <div class="span111">
                <div class="square-content">
                    <span>11.7 M</span>
                </div>
            </div>
            <div class="span112">
                <div class="square-content">
                    <span>2.16 M</span>
                </div>
            </div>
            <div class="span113">
                <div class="square-content">
                    <span>TO CEMENT MILL</span>
                </div>
            </div>
            <div class="span114">
                <div class="square-content">
                    <span>1643 ppm</span>
                    <div class="span115">
                        <div class="square-content">
                            <span>0.1 %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span116">
                <div class="square-content">
                    <span>0.8 ppm</span>
                    <div class="span117">
                        <div class="square-content">
                            <span>10.0 %</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span118">
                <div class="square-content">
                    <span>68,29 kV</span>
                    <div class="span119">
                        <div class="square-content">
                            <span>68,29 A</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span120">
                <div class="square-content">
                    <span>68,29 kV</span>
                    <div class="span121">
                        <div class="square-content">
                            <span>68,29 A</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>