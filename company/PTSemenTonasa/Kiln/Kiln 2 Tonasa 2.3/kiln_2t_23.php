<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('kiln_2t_23.png');

		}

		.pinch:before {
			content: "";
			display: block;
			padding-bottom: 54.5%;
		}

		.bg1 {
			position: absolute;
			top: 83%;
			left: 65%;
			width: 4%;
			text-align: center;
			background: white;
			font-size: 0.6vw;

		}

		.bg1:before {
			content: "";
			display: block;
			padding-bottom: 39%;
		}


		.rawmealsilo2 {
			position: absolute;
			top: 23%;
			left: 3.6%;
			width: 5%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.rawmealsilo2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.rawmealsilo2value {
			position: absolute;
			top: 18%;
			left: 9.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.rawmealsilo2value:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value1 {
			position: absolute;
			top: 29%;
			left: 10.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value1:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value2 {
			position: absolute;
			top: 29%;
			left: 16.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.value2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value3 {
			position: absolute;
			top: 40%;
			left: 7.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value4 {
			position: absolute;
			top: 47%;
			left: 14.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.value4:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value5 {
			position: absolute;
			top: 55%;
			left: 8.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.value5:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value6 {
			position: absolute;
			top: 58%;
			left: 5.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value6:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value7 {
			position: absolute;
			top: 70%;
			left: 5.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;

		}

		.value7:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value8 {
			position: absolute;
			top: 73%;
			left: 5.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value8:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value9 {
			position: absolute;
			top: 77%;
			left: 12.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value9:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value10 {
			position: absolute;
			top: 79.5%;
			left: 12.6%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value10:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value11 {
			position: absolute;
			top: 59%;
			left: 14%;
			width: 5%;
			text-align: left;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value11:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value12 {
			position: absolute;
			top: 71%;
			left: 19%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value12:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value13 {
			position: absolute;
			top: 83%;
			left: 18%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value13:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value14 {
			position: absolute;
			top: 64%;
			left: 27.3%;
			width: 5%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value14:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k04m5 {
			position: absolute;
			top: 66.5%;
			left: 27.5%;
			width: 5%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k04m5:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value15 {
			position: absolute;
			top: 66%;
			left: 34.3%;
			width: 5%;
			text-align: left;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value15:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value16 {
			position: absolute;
			top: 86%;
			left: 47.3%;
			width: 5%;
			text-align: left;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value16:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.totonasa3 {
			position: absolute;
			top: 90%;
			left: 37%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.totonasa3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3151303 {
			position: absolute;
			top: 87.5%;
			left: 55.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3151303:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3151309 {
			position: absolute;
			top: 87.5%;
			top: 87.5%;
			left: 62%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3151309:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k06m2 {
			position: absolute;
			top: 81.8%;
			left: 60.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k06m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k09m2 {
			position: absolute;
			top: 66.8%;
			left: 64.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k09m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k09m6 {
			position: absolute;
			top: 63.8%;
			left: 62.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k09m6:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k9 {
			position: absolute;
			top: 69.4%;
			left: 61.4%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k9:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k6 {
			position: absolute;
			top: 71.2%;
			left: 60%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k6:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k09m4 {
			position: absolute;
			top: 68.4%;
			left: 56.6%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k09m4:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value17 {
			position: absolute;
			top: 65.4%;
			left: 60.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value17:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value18 {
			position: absolute;
			top: 65.4%;
			left: 53%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value18:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value19 {
			position: absolute;
			top: 16%;
			left: 27.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value19:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value20 {
			position: absolute;
			top: 23%;
			left: 33.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value20:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value21 {
			position: absolute;
			top: 20%;
			left: 21%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value21:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value22 {
			position: absolute;
			top: 30%;
			left: 20.8%;
			width: 10%;
			text-align: left;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value22:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21610 {
			position: absolute;
			top: 37%;
			left: 20.4%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21610:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21610m2 {
			position: absolute;
			top: 45%;
			left: 19.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21610m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value23 {
			position: absolute;
			top: 46%;
			left: 26.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value23:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21614 {
			position: absolute;
			top: 48%;
			left: 24.4%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21614:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21613 {
			position: absolute;
			top: 51.6%;
			left: 24.8%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21613:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21614m2 {
			position: absolute;
			top: 57.6%;
			left: 25.8%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21614m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m3 {
			position: absolute;
			top: 60.4%;
			left: 25.8%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.tolabel {
			position: absolute;
			top: 64.4%;
			left: 18.1%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.tolabel:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m2 {
			position: absolute;
			top: 38%;
			left: 36%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value24 {
			position: absolute;
			top: 43%;
			left: 35.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value24:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k03 {
			position: absolute;
			top: 38%;
			left: 41%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k03:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m31 {
			position: absolute;
			top: 47%;
			left: 40.8%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m31:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m4 {
			position: absolute;
			top: 47%;
			left: 42.8%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m4:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.claydryer {
			position: absolute;
			top: 24%;
			left: 41.4%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.claydryer:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value25 {
			position: absolute;
			top: 27%;
			left: 40.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value25:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value26 {
			position: absolute;
			top: 29%;
			left: 40.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value26:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value27 {
			position: absolute;
			top: 34%;
			left: 40.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value27:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value28 {
			position: absolute;
			top: 36%;
			left: 40.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value28:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k03m5 {
			position: absolute;
			top: 31%;
			left: 45%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k03m5:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k03m7 {
			position: absolute;
			top: 34%;
			left: 45%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k03m7:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k03m6 {
			position: absolute;
			top: 37.7%;
			left: 47%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k03m6:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.tolstonedryer {
			position: absolute;
			top: 47.7%;
			left: 45%;
			width: 6%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.tolstonedryer:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value29 {
			position: absolute;
			top: 25.6%;
			left: 46%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value29:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m21 {
			position: absolute;
			top: 24%;
			left: 62.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m21:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21615 {
			position: absolute;
			top: 24%;
			left: 64.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21615:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value30 {
			position: absolute;
			top: 30%;
			left: 64.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value30:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value31 {
			position: absolute;
			top: 32%;
			left: 64.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value31:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value32 {
			position: absolute;
			top: 33.9%;
			left: 64.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value32:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21605 {
			position: absolute;
			top: 32.6%;
			left: 59.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21605:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m23 {
			position: absolute;
			top: 35%;
			left: 56.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m23:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m24 {
			position: absolute;
			top: 38%;
			left: 56.2%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m24:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21606 {
			position: absolute;
			top: 38.6%;
			left: 59.3%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21606:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21608 {
			position: absolute;
			top: 42.3%;
			left: 59.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21608:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080504 {
			position: absolute;
			top: 45.3%;
			left: 60.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080504:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080505 {
			position: absolute;
			top: 49.3%;
			left: 59%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080505:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080402 {
			position: absolute;
			top: 55.3%;
			left: 54%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080402:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3081903 {
			position: absolute;
			top: 54.3%;
			left: 65%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3081903:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080302 {
			position: absolute;
			top: 58.3%;
			left: 59%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080302:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080402m2 {
			position: absolute;
			top: 61.3%;
			left: 66.2%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080402m2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3080501 {
			position: absolute;
			top: 62.3%;
			left: 49%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3080501:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.to21204 {
			position: absolute;
			top: 52%;
			left: 42%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.to21204:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.K3081303 {
			position: absolute;
			top: 60.3%;
			left: 41.3%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.K3081303:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.fromclcrusher {
			position: absolute;
			top: 30%;
			left: 77.8%;
			width: 6%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.fromclcrusher:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.fromtonasa3 {
			position: absolute;
			top: 30%;
			left: 83%;
			width: 6%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.fromtonasa3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value33 {
			position: absolute;
			top: 35.9%;
			left: 88.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value33:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k2503 {
			position: absolute;
			top: 37.3%;
			left: 91%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k2503:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k4104 {
			position: absolute;
			top: 42.3%;
			left: 95%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k4104:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k1701 {
			position: absolute;
			top: 43.3%;
			left: 88%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k1701:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value34 {
			position: absolute;
			top: 47.9%;
			left: 82.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value34:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k2700 {
			position: absolute;
			top: 56.3%;
			left: 77.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k2700:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.m32 {
			position: absolute;
			top: 56.3%;
			left: 82.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.m32:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21703 {
			position: absolute;
			top: 57%;
			left: 94.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21703:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3313 {
			position: absolute;
			top: 60%;
			left: 83.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3313:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21722 {
			position: absolute;
			top: 61.8%;
			left: 76.5%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21722:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k21706 {
			position: absolute;
			top: 67.8%;
			left: 92%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k21706:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3003 {
			position: absolute;
			top: 70.8%;
			left: 92.7%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3003:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3104 {
			position: absolute;
			top: 73.5%;
			left: 95%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3104:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3603 {
			position: absolute;
			top: 73.5%;
			left: 73%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.k3603:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value35 {
			position: absolute;
			top: 85.5%;
			left: 79%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value35:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value36 {
			position: absolute;
			top: 87.5%;
			left: 84.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value36:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value37 {
			position: absolute;
			top: 87.5%;
			left: 92%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value37:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.tocementmill {
			position: absolute;
			top: 88.6%;
			left: 78%;
			width: 10%;
			text-align: center;
			color: white;
			font-size: 0.6vw;
		}

		.tocementmill:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value38 {
			position: absolute;
			top: 14.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value38:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value39 {
			position: absolute;
			top: 14.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value39:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value38 {
			position: absolute;
			top: 14.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value38:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value39 {
			position: absolute;
			top: 14.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value39:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}


		.value40 {
			position: absolute;
			top: 16.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value40:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value41 {
			position: absolute;
			top: 16.8%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value41:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value42 {
			position: absolute;
			top: 19%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value42:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value43 {
			position: absolute;
			top: 19%;
			left: 39.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value43:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value44 {
			position: absolute;
			top: 86.3%;
			left: 5.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value44:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value45 {
			position: absolute;
			top: 1%;
			left: 28%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value45:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value46 {
			position: absolute;
			top: 89%;
			left: 5.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value46:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.value47 {
			position: absolute;
			top: 1%;
			left: 28%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value47:before {
			content: "";
			display: block;
			padding-bottom: 1%;
		}

		.k1_1 {
			position: absolute;
			top: 34%;
			left: 12.6%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.k1_1:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k1_2 {
			position: absolute;
			top: 34%;
			left: 16.2%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.k1_2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k2 {
			position: absolute;
			top: 44%;
			left: 6.9%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.k2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k3 {
			position: absolute;
			top: 52%;
			left: 12.9%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.k3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.k4 {
			position: absolute;
			top: 63%;
			left: 6.9%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.k4:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.condtower {
			position: absolute;
			top: 34%;
			left: 28%;
			width: 5%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.condtower:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.lsb {
			position: absolute;
			top: 80%;
			left: 75.9%;
			width: 10%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.lsb:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.clinkersilo2 {
			position: absolute;
			top: 79%;
			left: 83.3%;
			width: 4%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.clinkersilo2:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.clinkersilo3 {
			position: absolute;
			top: 79%;
			left: 90.9%;
			width: 4%;
			text-align: center;
			color: black;
			font-size: 0.6vw;
			font-weight: bold;
		}

		.clinkersilo3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

















		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}

		.square-box:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="rawmealsilo2">
				<div class="square-content">
					<span>RAW MEAL SILO 2</span>
				</div>
			</div>
			<div class="rawmealsilo2value">
				<div class="square-content">
					<span>25.2 m</span>
				</div>
			</div>
			<div class="value1">
				<div class="square-content">
					<span>27 ° C</span>
				</div>
			</div>
			<div class="value2">
				<div class="square-content">
					<span>28 ° C</span>
				</div>
			</div>
			<div class="value3">
				<div class="square-content">
					<span>28 ° C</span>
				</div>
			</div>
			<div class="value4">
				<div class="square-content">
					<span>29 ° C</span>
				</div>
			</div>
			<div class="value5">
				<div class="square-content">
					<span>-0.44 mbar</span>
				</div>
			</div>
			<div class="value6">
				<div class="square-content">
					<span>29 ° C</span>
				</div>
			</div>
			<div class="value7">
				<div class="square-content">
					<span>8E-4 mbar</span>
				</div>
			</div>
			<div class="value8">
				<div class="square-content">
					<span>29 ° C</span>
				</div>
			</div>
			<div class="value9">
				<div class="square-content">
					<span>29 ° C</span>
				</div>
			</div>
			<div class="value10">
				<div class="square-content">
					<span>0.2 mbar</span>
				</div>
			</div>
			<div class="value11">
				<div class="square-content">
					<p style="margin-bottom: 1px;">-0.22 mbar</p>
					<p>29 ° C</p>
				</div>
			</div>
			<div class="value12">
				<div class="square-content">
					<span>1.65 mm</span>
				</div>
			</div>
			<div class="value13">
				<div class="square-content">
					<span>0.40 Bar</span>
				</div>
			</div>
			<div class="value14">
				<div class="square-content">
					<span>0.7 A</span>
				</div>
			</div>
			<div class="k04m5">
				<div class="square-content">
					<span>04M5</span>
				</div>
			</div>
			<div class="value15">
				<div class="square-content">
					<p style="margin-bottom: 1px;">-0.6 A</p>
					<p>0.00 rpm</p>
				</div>
			</div>
			<div class="value16">
				<div class="square-content">
					<p style="margin-bottom: 1px;">-0.6 A</p>
					<p>0.00 rpm</p>
				</div>
			</div>
			<div class="totonasa3">
				<div class="square-content">
					<span>TO TONASA 3</span>
				</div>
			</div>
			<div class="k3151303">
				<div class="square-content">
					<span>3151303</span>
				</div>
			</div>
			<div class="k3151309">
				<div class="square-content">
					<span>3151309</span>
				</div>
			</div>
			<div class="k06m2">
				<div class="square-content">
					<span>06M2</span>
				</div>
			</div>
			<div class="k09m2">
				<div class="square-content">
					<span>09M2</span>
				</div>
			</div>
			<div class="k09m6">
				<div class="square-content">
					<span>09M6</span>
				</div>
			</div>
			<div class="k9">
				<div class="square-content">
					<span>09</span>
				</div>
			</div>
			<div class="k6">
				<div class="square-content">
					<span>06</span>
				</div>
			</div>
			<div class="k09m4">
				<div class="square-content">
					<span>09M4</span>
				</div>
			</div>
			<div class="value17">
				<div class="square-content">
					<span>0 %</span>
				</div>
			</div>
			<div class="value18">
				<div class="square-content">
					<span>7E-2 A</span>
				</div>
			</div>
			<div class="value19">
				<div class="square-content">
					<span>18 ° C</span>
				</div>
			</div>
			<div class="value20">
				<div class="square-content">
					<span>-0.6 mbar</span>
				</div>
			</div>
			<div class="value21">
				<div class="square-content">
					<span>1.0 %</span>
				</div>
			</div>
			<div class="value22">
				<div class="square-content">
					<p style="margin-bottom: 1px;">0.0 bar</p>
					<p>0.0 i/min</p>
				</div>
			</div>
			<div class="k21610">
				<div class="square-content">
					<span>21610</span>
				</div>
			</div>
			<div class="k21610m2">
				<div class="square-content">
					<span>21610M4</span>
				</div>
			</div>
			<div class="value23">
				<div class="square-content">
					<span>18 ° C</span>
				</div>
			</div>
			<div class="k21614">
				<div class="square-content">
					<span>21614</span>
				</div>
			</div>
			<div class="k21613">
				<div class="square-content">
					<span>21613</span>
				</div>
			</div>
			<div class="k21614m2">
				<div class="square-content">
					<span>21614M2</span>
				</div>
			</div>
			<div class="m3">
				<div class="square-content">
					<span>M3</span>
				</div>
			</div>
			<div class="tolabel">
				<div class="square-content">
					<p style="margin-bottom: 1px;">TO21213</p>
					<p>TO21211</p>
				</div>
			</div>
			<div class="m2">
				<div class="square-content">
					<span>M2</span>
				</div>
			</div>
			<div class="value24">
				<div class="square-content">
					<span>100 %</span>
				</div>
			</div>
			<div class="k03">
				<div class="square-content">
					<span>03</span>
				</div>
			</div>
			<div class="m31">
				<div class="square-content">
					<span>M3</span>
				</div>
			</div>
			<div class="m4">
				<div class="square-content">
					<span>M4</span>
				</div>
			</div>
			<div class="claydryer">
				<div class="square-content">
					<span>CLAY DRYER</span>
				</div>
			</div>
			<div class="value25">
				<div class="square-content">
					<span>26 ° C</span>
				</div>
			</div>
			<div class="value26">
				<div class="square-content">
					<span>48 ° C</span>
				</div>
			</div>
			<div class="value27">
				<div class="square-content">
					<span>17 rpm</span>
				</div>
			</div>
			<div class="value28">
				<div class="square-content">
					<span>1 A</span>
				</div>
			</div>
			<div class="k03m5">
				<div class="square-content">
					<span>03M5</span>
				</div>
			</div>
			<div class="k03m7">
				<div class="square-content">
					<span>03M7</span>
				</div>
			</div>
			<div class="k03m6">
				<div class="square-content">
					<span>03M6</span>
				</div>
			</div>
			<div class="tolstonedryer">
				<div class="square-content">
					<span>TO L.STONE DRYER</span>
				</div>
			</div>
			<div class="value29">
				<div class="square-content">
					<span>-29 mbar</span>
				</div>
			</div>
			<div class="m21">
				<div class="square-content">
					<span>M2</span>
				</div>
			</div>
			<div class="k21615">
				<div class="square-content">
					<span>21615</span>
				</div>
			</div>
			<div class="value30">
				<div class="square-content">
					<span>174 kwh</span>
				</div>
			</div>
			<div class="value31">
				<div class="square-content">
					<span>50 ° C</span>
				</div>
			</div>
			<div class="value32">
				<div class="square-content">
					<span>97 ° C</span>
				</div>
			</div>
			<div class="k21605">
				<div class="square-content">
					<span>21605</span>
				</div>
			</div>
			<div class="m23">
				<div class="square-content">
					<span>M2</span>
				</div>
			</div>
			<div class="m24">
				<div class="square-content">
					<span>M2</span>
				</div>
			</div>
			<div class="k21606">
				<div class="square-content">
					<span>21606</span>
				</div>
			</div>
			<div class="k21608">
				<div class="square-content">
					<span>21608</span>
				</div>
			</div>
			<div class="k3080504">
				<div class="square-content">
					<span>3080504</span>
				</div>
			</div>
			<div class="k3080505">
				<div class="square-content">
					<span>3080505</span>
				</div>
			</div>
			<div class="k3080402">
				<div class="square-content">
					<span>3080402</span>
				</div>
			</div>
			<div class="k3081903">
				<div class="square-content">
					<span>3081903</span>
				</div>
			</div>
			<div class="k3080302">
				<div class="square-content">
					<span>3080302 </span>
				</div>
			</div>
			<div class="k3080402m2">
				<div class="square-content">
					<span>3080402M2</span>
				</div>
			</div>
			<div class="k3080501">
				<div class="square-content">
					<span>3080501</span>
				</div>
			</div>
			<div class="to21204">
				<div class="square-content">
					<span>TO 212041</span>
				</div>
			</div>
			<div class="k3081303">
				<div class="square-content">
					<span>3081303 </span>
				</div>
			</div>
			<div class="fromclcrusher">
				<div class="square-content">
					<span>FROM CL.CRUSHER</span>
				</div>
			</div>
			<div class="fromtonasa3">
				<div class="square-content">
					<span>FROM TONASA 3</span>
				</div>
			</div>
			<div class="value33">
				<div class="square-content">
					<span>40.0 A</span>
				</div>
			</div>
			<div class="k2503">
				<div class="square-content">
					<span>2503</span>
				</div>
			</div>
			<div class="k4104">
				<div class="square-content">
					<span>4104</span>
				</div>
			</div>
			<div class="k1701">
				<div class="square-content">
					<span>1701</span>
				</div>
			</div>
			<div class="value34">
				<div class="square-content">
					<span>0.0 A</span>
				</div>
			</div>
			<div class="k2700">
				<div class="square-content">
					<span>2700</span>
				</div>
			</div>
			<div class="m32">
				<div class="square-content">
					<span>M3</span>
				</div>
			</div>
			<div class="k21703 ">
				<div class="square-content">
					<span>21703</span>
				</div>
			</div>
			<div class="k3313">
				<div class="square-content">
					<span>3313</span>
				</div>
			</div>
			<div class="k21722">
				<div class="square-content">
					<span>21722</span>
				</div>
			</div>
			<div class="k21706">
				<div class="square-content">
					<span>21722</span>
				</div>
			</div>
			<div class="k3003">
				<div class="square-content">
					<span>3003</span>
				</div>
			</div>
			<div class="k3104">
				<div class="square-content">
					<span>3104</span>
				</div>
			</div>
			<div class="k3603">
				<div class="square-content">
					<span>3603</span>
				</div>
			</div>
			<div class="value35">
				<div class="square-content">
					<span>6.02 M</span>
				</div>
			</div>
			<div class="value36">
				<div class="square-content">
					<span>11.7 M</span>
				</div>
			</div>
			<div class="value37">
				<div class="square-content">
					<span>21.6 M</span>
				</div>
			</div>
			<div class="tocementmill">
				<div class="square-content">
					<span>TO CEMENT MILL</span>
				</div>
			</div>
			<div class="value38">
				<div class="square-content">
					<span>68,29 kV</span>
					<div class="value39">
						<span>68.29 kV</span>
					</div>
				</div>
			</div>
			<div class="value40">
				<div class="square-content">
					<span>68,29 kV</span>
					<div class="value41">
						<span>68.29 kV</span>
					</div>
				</div>
			</div>
			<div class="value42">
				<div class="square-content">
					<span>68,29 kV</span>
					<div class="value43">
						<span>68.29 kV</span>
					</div>
				</div>
			</div>
			<div class="value44">
				<div class="square-content">
					<span>0 ppm</span>
					<div class="value45">
						<span>0.09 %</span>
					</div>
				</div>
			</div>
			<div class="value46">
				<div class="square-content">
					<span>"</span>
					<div class="value47">
						<span>10.0 %</span>
					</div>
				</div>
			</div>
			<div class="k1_1">
				<div class="square-content">
					<span>1.1</span>
				</div>
			</div>
			<div class="k1_2">
				<div class="square-content">
					<span>1.2</span>
				</div>
			</div>
			<div class="k2">
				<div class="square-content">
					<span>2</span>
				</div>
			</div>
			<div class="k3">
				<div class="square-content">
					<span>3</span>
				</div>
			</div>
			<div class="k4">
				<div class="square-content">
					<span>4</span>
				</div>
			</div>
			<div class="condtower">
				<div class="square-content">
					<span>COND TOWER</span>
				</div>
			</div>
			<div class="lsb">
				<div class="square-content">
					<span>LSB</span>
				</div>
			</div>
			<div class="clinkersilo2">
				<div class="square-content">
					<span>CLINKER SILO 2</span>
				</div>
			</div>
			<div class="clinkersilo3">
				<div class="square-content">
					<span>CLINKER SILO 3</span>
				</div>
			</div>
			<div class="bg1">
				<div class='square-content'>

					<div>
						<span>
							<a style="color: red; font-weight: bold;" href="">PFISTER</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Progress -->
	<!-- <progress id="file" max="100" value="100"> </progress> -->
</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		var url = 'http://par4digma.semenindonesia.com/api/index.php/plant_tuban/raw_mill_tb4';
		$.ajax({
			url: url,
			type: 'get',
			// dataType: 'json',
			success: function(data) {
				var data1 = data.replace("<title>Json</title>", "");
				var data2 = data1.replace("(", "[");
				var data3 = data2.replace(");", "]");
				var dataJson = JSON.parse(data3);
				get_span57 = parseFloat(dataJson[0].tags[57].props[0].val).toFixed(2) + " ";
				$("#get_span57").html(get_span57);
				// tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
				// tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
				// tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
				// tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
				// tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
				// tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
				// tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
				// tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
				// $("#isisilo10").html(tonageSilo10);
				// $("#isisilo11").html(tonageSilo11);
				// $("#isisilo12").html(tonageSilo12);
			}
		});
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>