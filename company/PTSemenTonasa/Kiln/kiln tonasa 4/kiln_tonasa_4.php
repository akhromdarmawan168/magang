<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('kiln_tonasa_4.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }

        .span1 {
            position: absolute;
            width: 10%;
            top: 28.5%;
            left: 31%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span1:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span2 {
            position: absolute;
            width: 10%;
            top: 29.5%;
            left: 38%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span2:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span3 {
            position: absolute;
            width: 10%;
            top: 34.9%;
            left: 38%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span3:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span4 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 41%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span4:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span5 {
            position: absolute;
            width: 10%;
            top: 66.7%;
            left: 35%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span5:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span6 {
            position: absolute;
            width: 10%;
            top: 69.7%;
            left: 35%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span6:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span7 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 41%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span7:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span8 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 36.5%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span8:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span9 {
            position: absolute;
            width: 10%;
            top: 47.7%;
            left: 34.7%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span9:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span10 {
            position: absolute;
            width: 10%;
            top: 58.3%;
            left: 35%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span10:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span11 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 46.2%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span11:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span12 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 51.4%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span12:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span13 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 56.3%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span13:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span14 {
            position: absolute;
            width: 10%;
            top: 41.7%;
            left: 61%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span14:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span15 {
            position: absolute;
            width: 10%;
            top: 34.9%;
            left: 59%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span15:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span16 {
            position: absolute;
            width: 10%;
            top: 28.5%;
            left: 65%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span16:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span17 {
            position: absolute;
            width: 10%;
            top: 58.3%;
            left: 63%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span17:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span18 {
            position: absolute;
            width: 10%;
            top: 47.9%;
            left: 63%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span18:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span19 {
            position: absolute;
            width: 10%;
            top: 29.9%;
            left: 59%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span19:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span20 {
            position: absolute;
            width: 10%;
            top: 56%;
            left: 45.5%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span20:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span21 {
            position: absolute;
            width: 10%;
            top: 56%;
            left: 51.8%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span21:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span22 {
            position: absolute;
            width: 10%;
            top: 60.5%;
            left: 49.5%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span22:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span23 {
            position: absolute;
            width: 10%;
            top: 66.7%;
            left: 61.5%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span23:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span24 {
            position: absolute;
            width: 10%;
            top: 70%;
            left: 61.5%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span24:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span25 {
            position: absolute;
            width: 10%;
            top: 75.5%;
            left: 51.9%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span25:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span26 {
            position: absolute;
            width: 10%;
            top: 78.5%;
            left: 51.9%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span26:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span27 {
            position: absolute;
            width: 10%;
            top: 78.5%;
            left: 544.9%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span27:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span28 {
            position: absolute;
            width: 10%;
            top: 78.5%;
            left: 67.9%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span28:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span29 {
            position: absolute;
            width: 10%;
            top: 77.5%;
            left: 88.9%;
            color: green;
            font-size: 0.6vw;
            text-align: center;
        }

        .span29:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span30 {
            position: absolute;
            width: 10%;
            top: 63.9%;
            left: 1.9%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .span30:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span31 {
            position: absolute;
            width: 10%;
            top: 63.9%;
            left: 7.4%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .span31:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span32 {
            position: absolute;
            width: 10%;
            top: 81.9%;
            left: 11.7%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .span32:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span33 {
            position: absolute;
            width: 10%;
            top: 81.9%;
            left: 18.4%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .span33:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span34 {
            position: absolute;
            width: 10%;
            top: 81.9%;
            left: 24.9%;
            color: white;
            font-size: 0.6vw;
            text-align: center;
        }

        .span34:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span35 {
            position: absolute;
            width: 10%;
            top: 75.9%;
            left: 1.9%;
            color: #00B050;
            font-size: 0.6vw;
            text-align: center;
        }

        .span35:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span36 {
            position: absolute;
            width: 10%;
            top: 75.9%;
            left: 7.4%;
            color: #00B050;
            font-size: 0.6vw;
            text-align: center;
        }

        .span36:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span37 {
            position: absolute;
            width: 10%;
            top: 83.5%;
            left: 12%;
            color: #00B050;
            font-size: 0.6vw;
            text-align: center;
        }

        .span37:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span38 {
            position: absolute;
            width: 10%;
            top: 83.5%;
            left: 18.6%;
            color: #00B050;
            font-size: 0.6vw;
            text-align: center;
        }

        .span38:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span39 {
            position: absolute;
            width: 10%;
            top: 83.5%;
            left: 25.2%;
            color: #00B050;
            font-size: 0.6vw;
            text-align: center;
        }

        .span39:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span40 {
            position: absolute;
            width: 10%;
            top: 87.3%;
            left: 32.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span40:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span41 {
            position: absolute;
            width: 10%;
            top: 89.5%;
            left: 32.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span41:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span42 {
            position: absolute;
            width: 10%;
            top: 91.5%;
            left: 32.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span42:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span43 {
            position: absolute;
            width: 10%;
            top: 93.5%;
            left: 32.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span43:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span44 {
            position: absolute;
            width: 10%;
            top: 89.5%;
            left: 36.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span44:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span45 {
            position: absolute;
            width: 10%;
            top: 91.5%;
            left: 36.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span45:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span46 {
            position: absolute;
            width: 10%;
            top: 93.5%;
            left: 36.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span46:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span47 {
            position: absolute;
            width: 10%;
            top: 89.5%;
            left: 68.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span47:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span48 {
            position: absolute;
            width: 10%;
            top: 91.5%;
            left: 68.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span48:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span49 {
            position: absolute;
            width: 10%;
            top: 93.5%;
            left: 68.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span49:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span50 {
            position: absolute;
            width: 10%;
            top: 64.3%;
            left: 89%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span50:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span51 {
            position: absolute;
            width: 10%;
            top: 66.5%;
            left: 89%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span51:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span52 {
            position: absolute;
            width: 10%;
            top: 69.5%;
            left: 89%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span52:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span53 {
            position: absolute;
            width: 10%;
            top: 72.5%;
            left: 89%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span53:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span54 {
            position: absolute;
            width: 10%;
            top: 66.5%;
            left: 95%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span54:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span55 {
            position: absolute;
            width: 10%;
            top: 69.5%;
            left: 95%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span55:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span56 {
            position: absolute;
            width: 10%;
            top: 72.5%;
            left: 96.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span56:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span57 {
            position: absolute;
            width: 10%;
            top: 14.5%;
            left: 43%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span57:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span58 {
            position: absolute;
            width: 10%;
            top: 17%;
            left: 43%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span58:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span59 {
            position: absolute;
            width: 10%;
            top: 19%;
            left: 43%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span59:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span60 {
            position: absolute;
            width: 10%;
            top: 21%;
            left: 43%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span60:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span61 {
            position: absolute;
            width: 10%;
            top: 17%;
            left: 47.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span61:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span62 {
            position: absolute;
            width: 10%;
            top: 19%;
            left: 47.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span62:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span63 {
            position: absolute;
            width: 10%;
            top: 21%;
            left: 47.5%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span63:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span64 {
            position: absolute;
            width: 10%;
            top: 14.5%;
            left: 53.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span64:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span65 {
            position: absolute;
            width: 10%;
            top: 17%;
            left: 53.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span65:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span66 {
            position: absolute;
            width: 10%;
            top: 19%;
            left: 53.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span66:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span67 {
            position: absolute;
            width: 10%;
            top: 21%;
            left: 53.5%;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span67:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span68 {
            position: absolute;
            width: 10%;
            top: 17%;
            left: 58%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span68:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span69 {
            position: absolute;
            width: 10%;
            top: 19%;
            left: 58%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span69:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span70 {
            position: absolute;
            width: 10%;
            top: 21%;
            left: 58%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span70:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span71 {
            position: absolute;
            width: 10%;
            top: 39.5%;
            left: 64%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span71:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span72 {
            position: absolute;
            width: 10%;
            top: 40.5%;
            left: 33%;
            color: #00B050;
            font-size: 0.7vw;
            text-align: center;
        }

        .span72:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span73 {
            position: absolute;
            width: 10%;
            top: 40.2%;
            left: 42.3%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span73:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span74 {
            position: absolute;
            width: 10%;
            top: 40.2%;
            left: 47.1%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span74:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span75 {
            position: absolute;
            width: 10%;
            top: 48%;
            left: 39.6%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span75:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span76 {
            position: absolute;
            width: 10%;
            top: 53.8%;
            left: 44%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span76:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span77 {
            position: absolute;
            width: 10%;
            top: 58.5%;
            left: 39.6%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span77:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span78 {
            position: absolute;
            width: 10%;
            top: 40.2%;
            left: 52.2%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span78:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span79 {
            position: absolute;
            width: 10%;
            top: 40.2%;
            left: 57%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span79:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span80 {
            position: absolute;
            width: 10%;
            top: 48%;
            left: 59.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span80:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span81 {
            position: absolute;
            width: 10%;
            top: 53.6%;
            left: 55.2%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span81:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span82 {
            position: absolute;
            width: 10%;
            top: 58.5%;
            left: 59.5%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span82:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span83 {
            position: absolute;
            width: 10%;
            top: 62.5%;
            left: 50%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span83:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span84 {
            position: absolute;
            width: 10%;
            top: 83.3%;
            left: 58.5%;
            font-weight: bold;
            color: black;
            font-size: 1vw;
            text-align: center;
        }

        .span84:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span85 {
            position: absolute;
            width: 10%;
            top: 70.5%;
            left: 2.7%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span85:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span86 {
            position: absolute;
            width: 10%;
            top: 70.5%;
            left: 8%;
            font-weight: bold;
            color: black;
            font-size: 0.6vw;
            text-align: center;
        }

        .span86:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }

        .span87 {
            position: absolute;
            width: 10%;
            top: 34.5%;
            left: 49.9%;
            font-weight: bold;
            color: white;
            font-size: 0.7vw;
            text-align: center;
        }

        .span87:before {
            content: "";
            display: block;
            padding-bottom: 10%;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>-3.38 °C</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>22.27 °C</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>0.20 %</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>-0.41 mbar</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>-0.44 mbar</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>-1.03 mbar</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>-0.41 mbar</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>101.98 °C</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>-0.57 mbar</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>97.16 °C</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>-0.50 mbar</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>-0.51 mbar</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>-0.42 mbar</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>105.43 °C</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>0.23 %</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>-3.97 %</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>94.58 °C</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>0.55 mbar</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>23.67 °C</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>-0.63 mbar</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>-0.15 mbar</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>86.99 °C</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>-0.05 mbar</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>-0.07 mbar</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>44.15 °C</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>60.88 °C</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>-0.88 mbar</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>-229.09 °C</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>-0.00 °C</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>413SS01</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>414SS01</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>413BE01</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>413BE02</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>414BE01</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>14.770 ton</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>12.123 ton</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>72.89 A</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>42.85 A</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>71.32 A</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>415GS01</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>O2</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>CO</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>NO</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>2,17 %</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>0,03 %</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>603 ppm</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>603 rpm</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>32768 KW</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>463,41 %</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>COAL</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>CF1</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>CF2</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>CF3</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>17,27 t/h</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>19,88 t/h</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>0 t/h</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>415AE01</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>O2</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>CO</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>NO</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>2,17 %</span>
                </div>
            </div>
            <div class="span62">
                <div class="square-content">
                    <span>0,03 %</span>
                </div>
            </div>
            <div class="span63">
                <div class="square-content">
                    <span>603 ppm</span>
                </div>
            </div>
            <div class="span64">
                <div class="square-content">
                    <span>415AE02</span>
                </div>
            </div>
            <div class="span65">
                <div class="square-content">
                    <span>O2</span>
                </div>
            </div>
            <div class="span66">
                <div class="square-content">
                    <span>CO</span>
                </div>
            </div>
            <div class="span67">
                <div class="square-content">
                    <span>NO</span>
                </div>
            </div>
            <div class="span68">
                <div class="square-content">
                    <span>2,17 %</span>
                </div>
            </div>
            <div class="span69">
                <div class="square-content">
                    <span>0,03 %</span>
                </div>
            </div>
            <div class="span70">
                <div class="square-content">
                    <span>603 ppm</span>
                </div>
            </div>
            <div class="span71">
                <div class="square-content">
                    <span>300 t/h</span>
                </div>
            </div>
            <div class="span72">
                <div class="square-content">
                    <span>300 t/h</span>
                </div>
            </div>
            <div class="span73">
                <div class="square-content">
                    <span>CY01</span>
                </div>
            </div>
            <div class="span74">
                <div class="square-content">
                    <span>CY02</span>
                </div>
            </div>
            <div class="span75">
                <div class="square-content">
                    <span>CY03</span>
                </div>
            </div>
            <div class="span76">
                <div class="square-content">
                    <span>CY04</span>
                </div>
            </div>
            <div class="span77">
                <div class="square-content">
                    <span>CY05</span>
                </div>
            </div>
            <div class="span78">
                <div class="square-content">
                    <span>CY06</span>
                </div>
            </div>
            <div class="span79">
                <div class="square-content">
                    <span>CY07</span>
                </div>
            </div>
            <div class="span80">
                <div class="square-content">
                    <span>CY08</span>
                </div>
            </div>
            <div class="span81">
                <div class="square-content">
                    <span>CY09</span>
                </div>
            </div>
            <div class="span82">
                <div class="square-content">
                    <span>CY10</span>
                </div>
            </div>
            <div class="span83">
                <div class="square-content">
                    <span>CA01</span>
                </div>
            </div>
            <div class="span84">
                <div class="square-content">
                    <span>KILN</span>
                </div>
            </div>
            <div class="span85">
                <div class="square-content">
                    <span>88,1 %</span>
                </div>
            </div>
            <div class="span86">
                <div class="square-content">
                    <span>81,3 %</span>
                </div>
            </div>
            <div class="span87">
                <div class="square-content">
                    <span>AIR</span>
                </div>
            </div>
        </div>
    </div>
</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        var url = 'http://10.15.5.150/dev/par4digma/api/index.php/plant_tonasa/kiln_t5';
        $.ajax({
            url: url,
            type: 'get',
            //dataType: 'json',
            success: function(data) {
                var data1 = data.replace("<title>Json</title>", "");
                var data2 = data1.replace("(", "[");
                var data3 = data2.replace(");", "]");
                var dataJson = JSON.parse(data3);
                get_KILN_44 = parseFloat(dataJson[0].tags[44].props[0].val).toFixed(2) + " A";
                $("#get_KILN_44").html(get_KILN_44);
                tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
                tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
                tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
                tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
                tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
                tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
                tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
                tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
                $("#isisilo10").html(tonageSilo10);
                $("#isisilo11").html(tonageSilo11);
                $("#isisilo12").html(tonageSilo12);
            }
        });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>