<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.kilnShelltittle {
			position: absolute;
			width: 10%;
			top: 6%;
			left: 84.5%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
		}

		.kilnShelltittle :before {
			content: " ";
			display: block;
			padding-bottom: 10%;
		}

		.kilnShellPos {
			position: absolute;
			top: 12%;
			left: 85%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
		}

		.kilnShellTemp {
			position: absolute;
			top: 12%;
			left: 92%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
		}

		.kilnShellPos1 {
			position: absolute;
			top: 17%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos2 {
			position: absolute;
			top: 20%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos3 {
			position: absolute;
			top: 23%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos4 {
			position: absolute;
			top: 26%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos5 {
			position: absolute;
			top: 29%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos6 {
			position: absolute;
			top: 32%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos7 {
			position: absolute;
			top: 35%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos8 {
			position: absolute;
			top: 38%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellPos9 {
			position: absolute;
			top: 41%;
			left: 86%;
			font-size: 0.6vw;
			color: white;
			line-height: 50%;
		}

		.kilnShellTemp1 {
			position: absolute;
			top: 17%;
			left: 92%;
			font-size: 18px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp2 {
			position: absolute;
			top: 20%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp3 {
			position: absolute;
			top: 23%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp4 {
			position: absolute;
			top: 26%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp5 {
			position: absolute;
			top: 29%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp6 {
			position: absolute;
			top: 32%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp7 {
			position: absolute;
			top: 35%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp8 {
			position: absolute;
			top: 38%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.kilnShellTemp9 {
			position: absolute;
			top: 41%;
			left: 92%;
			font-size: 18px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 50%;
		}

		.clinkerQuality {
			position: absolute;
			top: 24.5%;
			left: 71.5%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
		}

		.clinkerQualityId {
			position: absolute;
			top: 28.5%;
			left: 72.5%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
			line-height: 0%;
		}

		.clinkerQualityValue {
			position: absolute;
			top: 28.5%;
			right: 22%;
			font-size: 18px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 0%;
		}

		.kilnFeed {
			position: absolute;
			top: 9%;
			left: 71.5%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;

		}

		.kilnFeedName {
			position: absolute;
			top: 13%;
			left: 72.5%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
			line-height: 15%;
		}

		.kilnFeedValue {
			position: absolute;
			top: 13%;
			right: 21%;
			font-size: 18px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 15%;
		}


		.kilnInlet {
			position: absolute;
			top: 80%;
			left: 30%;
			font-size: 18px;
			font-size: 0.6vw;
			color: white;
			text-decoration: underline;
		}

		.kilnInletTittle {
			position: absolute;
			top: 84%;
			left: 30%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
			line-height: 20%;
		}

		.kilnInletValue {
			position: absolute;
			top: 84%;
			left: 36.2%;
			font-size: 18px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 20%;
		}

		.kilnInletTittle2 {
			position: absolute;
			top: 92%;
			left: 30%;
			font-size: 18px;
			font-size: 0.5vw;
			color: white;
			line-height: 20%;
		}

		.kilnInletValue2 {
			position: absolute;
			top: 92%;
			left: 36.2%;
			font-size: 18px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 20%;
		}


		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('tonasa5.png');

		}

		.pinch:before {
			content: " ";
			display: block;
			padding-bottom: 54.5%;
		}

		.RF01 {
			position: absolute;
			width: 10%;
			left: 25.8%;
			top: 31.7%;
			font-weight: bold;
			overflow: hidden;
			font-size: 6px;
			font-size: 0.6vw;
		}

		.RF01:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RF01Value {
			position: absolute;
			width: 4.8%;
			left: 29%;
			top: 34%;
			font-size: 6px;
			font-size: 0.6vw;
			color: #7CFF00;
		}


		.RF02 {
			position: absolute;
			width: 10%;
			left: 25.8%;
			top: 26%;
			font-weight: bold;
			overflow: hidden;
			font-size: 5px;
			font-size: 0.6vw;
		}

		.RF02:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}


		.RF02Value {
			position: absolute;
			width: 4.8%;
			left: 32%;
			top: 28%;
			font-size: 5px;
			font-size: 0.6vw;
			color: #7CFF00;
		}


		.RF03 {
			position: absolute;
			width: 10%;
			left: 61.2%;
			top: 28%;
			font-weight: bold;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
		}

		.RF03:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RF03Value {
			position: absolute;
			width: 4.8%;
			left: 59.5%;
			top: 34%;
			font-size: 0.6vw;
			color: #7CFF00;
		}


		.RF04 {
			position: absolute;
			width: 10%;
			left: 61.2%;
			top: 22.3%;
			font-weight: bold;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
		}

		.RF04:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RF04Value {
			position: absolute;
			width: 5%;
			left: 55.5%;
			top: 23.2%;
			font-size: 0.6vw;
			color: #7CFF00;
		}


		.RL01 {
			position: absolute;
			width: 10%;
			left: 67.6%;
			top: 54%;
			font-weight: bold;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
		}

		.RL01:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RL01Value {
			position: absolute;
			width: auto;
			left: 72%;
			top: 53.8%;
			font-size: 6px;
			font-size: 0.5vw;
			color: #7CFF00;
		}


		.RL02 {
			position: absolute;
			width: 10%;
			left: 67.6%;
			top: 59%;
			font-weight: bold;
			font-size: 8px;
			font-size: 0.6vw;
		}

		.RL02:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RL02Value {
			position: absolute;
			width: auto;
			left: 72%;
			top: 58.8%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
		}

		.id541AS01 {
			position: absolute;
			width: 5%;
			left: 4%;
			top: 67%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541DG01 {
			position: absolute;
			width: 5%;
			left: 14.5%;
			top: 68%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541AS08 {
			position: absolute;
			width: 5%;
			left: 6.8%;
			top: 72.4%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541AS09 {
			position: absolute;
			width: 5%;
			left: 14.7%;
			top: 72.4%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.airCondition {
			position: absolute;
			width: 5%;
			left: 52.5%;
			top: 15%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
			line-height: 5%;
		}

		.airConditionValue {
			position: absolute;
			width: 5%;
			right: 39%;
			top: 15%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 5%;
		}

		.kilnValue1 {
			position: absolute;
			width: 5%;
			left: 71.5%;
			top: 88.7%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
			text-align: center;
		}


		.kilnValue2 {
			position: absolute;
			width: 5%;
			left: 71.5%;
			top: 90.2%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
			text-align: center;
		}

		.kilnValue3 {
			position: absolute;
			width: 5%;
			left: 68%;
			top: 75%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.kilnValue4 {
			position: absolute;
			width: 5%;
			left: 57.5%;
			top: 77%;
			font-size: 8px;
			font-size: 0.6vw;
			line-height: 0%;
			color: #7CFF00;
			text-align: right;
		}

		.kilnValue4Par {
			position: absolute;
			width: 5%;
			left: 61%;
			top: 77%;
			font-size: 8px;
			font-size: 0.6vw;
			line-height: 0%;
			color: #7CFF00;
			text-align: right;
		}

		.kilnValue5 {
			position: absolute;
			width: 5%;
			left: 57.5%;
			top: 91.2%;
			font-size: 8px;
			font-size: 0.6vw;
			line-height: 0%;
			color: #7CFF00;
			text-align: right;
		}

		.kilnValue5Par {
			position: absolute;
			width: 5%;
			left: 61%;
			top: 91.2%;
			font-size: 8px;
			font-size: 0.6vw;
			line-height: 0%;
			color: #7CFF00;
			text-align: right;
		}

		.value1 {
			position: absolute;
			width: 5%;
			left: 41.5%;
			top: 75%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.value2 {
			position: absolute;
			width: auto;
			left: 26.5%;
			top: 40.5%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: right;
		}

		.value3 {
			position: absolute;
			width: auto;
			right: 36.5%;
			top: 40.5%;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: left;
		}

		.value4 {
			position: absolute;
			width: auto;
			left: 39%;
			top: 48%;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: left;
		}

		.value5 {
			position: absolute;
			width: auto;
			left: 45.8%;
			top: 48%;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: right;
		}

		.value6 {
			position: absolute;
			width: auto;
			left: 26.5%;
			top: 68%;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: right;
		}

		.value7 {
			position: absolute;
			width: auto;
			right: 36.5%;
			top: 68%;
			font-size: 0.6vw;
			color: #7CFF00;
			line-height: 5%;
			text-align: left;
		}


		.RL03 {
			position: absolute;
			width: 10%;
			left: 88.9%;
			top: 89.3%;
			font-weight: bold;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
		}

		.RL03:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.RL03Value {
			position: absolute;
			width: 10%;
			left: 82%;
			top: 90.5%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.RL03Value:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.id543FN04 {
			position: absolute;
			width: 10%;
			left: 83.5%;
			top: 71%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		/* .id543FN04:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		} */

		.id543FN04Value {
			position: absolute;
			width: 5%;
			left: 84%;
			top: 63%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 10%;
		}

		.id543FN04Value2 {
			position: absolute;
			width: 5%;
			left: 80%;
			top: 71%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}


		.id431FN570 {
			position: absolute;
			width: 5%;
			left: 83.4%;
			top: 80.2%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id431FN570Value {
			position: absolute;
			width: auto;
			left: 81%;
			top: 83%;
			font-size: 0.5vw;
			color: #7CFF00;
			text-align: right;
			line-height: 5%;
		}


		.siloTittle {
			position: absolute;
			width: 8%;
			left: 2.5%;
			top: 40%;
			font-weight: bold;
			font-size: 8px;
			font-size: 0.55vw;
		}

		.siloTittle:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.siloNumber1 {
			position: absolute;
			width: 8%;
			left: 3.8%;
			top: 42%;
			font-weight: bold;
			font-size: 8px;
			font-size: 0.55vw;
		}

		.siloNumber1:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.siloValue {
			position: absolute;
			width: 8%;
			left: 4.3%;
			top: 45%;
			font-weight: bold;
			font-size: 8px;
			font-size: 0.55vw;
			color: #7CFF00;
		}

		.siloValue:before {
			content: " ";
			display: block;
			padding-bottom: 10%;
		}


		.siloNumber2 {
			position: absolute;
			width: 10%;
			left: 3.3%;
			top: 55.5%;
			font-weight: bold;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
			text-align: center;
		}

		.siloNumber2:before {
			content: " ";
			display: block;
			padding-bottom: 10%;
		}

		.id532BE03 {
			position: absolute;
			width: 10%;
			left: 5.5%;

			top: 6%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
			text-align: center;
		}

		.id532BE03:before {
			content: " ";
			display: block;
			padding-bottom: 10%;
		}

		.id532BE03Value {
			position: absolute;
			width: 5%;
			left: 11.5%;
			top: 2.3%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.id541BE01 {
			position: absolute;
			width: 5%;
			left: 14.5%;
			top: 6%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541BE01Value {
			position: absolute;
			width: 5%;
			left: 19.3%;
			top: 2.3%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.id541AS02 {
			position: absolute;
			width: 5%;
			left: 12%;
			top: 16.9%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541AS03 {
			position: absolute;
			width: 5%;
			left: 19.1%;
			top: 16.9%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541AS04 {
			position: absolute;
			width: 5%;
			left: 14%;
			top: 21.3%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541DG04 {
			position: absolute;
			width: 5%;
			left: 20.5%;
			top: 21.3%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541DG04value {
			position: absolute;
			width: 5%;
			left: 23%;
			top: 22%;
			overflow: hidden;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.id541DG05 {
			position: absolute;
			width: 5%;
			left: 24%;
			top: 20%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id541DG05Value {
			position: absolute;
			width: 5%;
			left: 26.5%;
			top: 20.7%;
			font-size: 8px;
			font-size: 0.6vw;
			color: #7CFF00;
		}

		.id533FN03 {
			position: absolute;
			width: 5%;
			left: 34.2%;
			top: 19%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id533FN03Value {
			position: absolute;
			width: 5%;
			left: 32.3%;
			top: 20.7%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
		}


		.id533FN03Value2 {
			position: absolute;
			width: auto;
			left: 35%;
			top: 20.7%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
		}

		.id533FN02 {
			position: absolute;
			width: 5%;
			left: 44.1%;
			top: 16.5%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.id533FN02Value {
			position: absolute;
			width: 5%;
			left: 42.1%;
			top: 17.5%;
			font-size: 0.5vw;
			color: #7CFF00;
		}

		.id533FN02Value2 {
			position: absolute;
			width: 5%;
			left: 45.5%;
			top: 17.5%;
			font-size: 0.5vw;
			color: #7CFF00;
		}

		.idValue1 {
			position: absolute;
			width: 5%;
			left: 40.5%;
			top: 21%;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 0%;
			text-align: left;
		}

		.idValue2 {
			position: absolute;
			width: 5%;
			left: 45%;
			top: 21%;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 0%;
			text-align: right;
		}


		.clinkerProd {
			position: absolute;
			width: auto;
			left: 71.5%;
			top: 37%;
			font-size: 8px;
			font-size: 0.5vw;
			color: white;
		}

		.clinkerProdValue {
			position: absolute;
			width: auto;
			left: 71.5%;
			top: 40.2%;
			font-size: 8px;
			font-size: 0.5vw;
			color: #7CFF00;
			line-height: 20%;
		}

		.spanbutton1 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 92%;
			top: 1%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;

			color: #42ff00;
		}

		.spanbutton1:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.spanbutton2 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 84%;
			top: 1%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;
			color: #42ff00;
		}

		.spanbutton2:before {
			content: " ";
			display: block;
			padding-bottom: 20%;
		}

		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}


		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="RF01">
				<div class="square-content">
					<span>541RF01</span>
				</div>
			</div>
			<div class="RF02">
				<div class="square-content">
					<span>541RF02</span>
				</div>
			</div>
			<div class="RF03">
				<div class="square-content">
					<span>541RF03</span>
				</div>
			</div>
			<div class="RF04">
				<div class="square-content">
					<span>541RF04</span>
				</div>
			</div>
			<div class="RL01">
				<div class="square-content">
					<span>545RL01</span>
				</div>
			</div>
			<div class="RL01Value">
				<p id="get_KILN_59"></p>
			</div>
			<div class="RL02">
				<div class="square-content">
					<span>545RL02</span>
				</div>
			</div>
			<div class="RL02Value">
				<p id="get_KILN_58"></p>
			</div>
			<div class="RL03">
				<div class="square-content">
					<span>545RL03</span>
				</div>
			</div>
			<div class="RL03Value">
				<div class="square-content">
					<span id="get_KILN_51"></span>
				</div>
			</div>
			<div class="siloTittle">
				<div class="square-content">
					<span>RAW MEAL SILO</span>
				</div>
			</div>
			<div class="siloNumber1">
				<div class="square-content">
					<span>532SI01</span>
				</div>
			</div>
			<div class="siloValue">
				<div class="square-content">
					<span id="get_KILN_1 "></span>
				</div>
			</div>
			<div class="siloNumber2">
				<div class="square-content">
					<span>541BI01</span>
				</div>
			</div>
			<div class="kilnShelltittle">
				<div class="square-content">
					<span> KILN SHELL</span>
				</div>
			</div>
			<div class="kilnShellPos">
				<p>Position(m)</p>
			</div>
			<div class="kilnShellTemp">
				<p>Temp. (°C)</p>
			</div>
			<div class="kilnShellPos1">
				<span>0 - 10</span>
			</div>
			<div class="kilnShellPos2">
				<span>10 - 20</span>
			</div>
			<div class="kilnShellPos3">
				<span>20 - 30</span>
			</div>
			<div class="kilnShellPos4">
				<span>30 - 40</span>

			</div>
			<div class="kilnShellPos5">
				<span>40 - 50</span>
			</div>
			<div class="kilnShellPos6">
				<span>50 - 60</span>

			</div>
			<div class="kilnShellPos7">
				<span>60 - 70</span>
			</div>
			<div class="kilnShellPos8">
				<span>70 - 80</span>
			</div>
			<div class="kilnShellPos9">
				<span>80 - 86</span>
			</div>

			<div class="kilnShellTemp1">
				<span id="get_KILN_69"></span>
			</div>

			<div class="kilnShellTemp2">
				<span id="get_KILN_70"></span>
			</div>

			<p class="kilnShellTemp3">
				<span id="get_KILN_71"></span>
			</p>

			<div class="kilnShellTemp4">
				<span id="get_KILN_72"></span>
			</div>

			<div class="kilnShellTemp5">
				<span id="get_KILN_73"></span>
			</div>

			<div class="kilnShellTemp6">
				<span id="get_KILN_74"></span>
			</div>

			<div class="kilnShellTemp7">
				<span id="get_KILN_75"></span>
			</div>

			<div class="kilnShellTemp8">
				<span id="get_KILN_76"></span>
			</div>

			<div class="kilnShellTemp9">
				<span id="get_KILN_77"></span>
			</div>
			<div class="kilnFeedValue">
				<p id="get_KILN_62"></p>
				<p id="get_KILN_63"></p>
				<p id="get_KILN_64"></p>
				<p>620 t/h</p>
			</div>
			<div class="kilnFeed">
				<p> KILN FEED</p>
			</div>
			<div class="kilnFeedName">
				<p>LSF</p>
				<p>SM</p>
				<p>AM</p>
				<p>Rate</p>
			</div>
			<div class="clinkerQuality">
				<p>CLINKER QUALITY</p>

			</div>
			<div class="clinkerQualityId">
				<p>FL</p>
				<p>C3S</p>
				<p>TEMP</p>
			</div>

			<div class="clinkerQualityValue">
				<p id="get_KILN_66"></p>
				<p id="get_KILN_67"></p>
				<p id="get_KILN_68"></p>
			</div>
			<div class="kilnInlet">
				<p>KILN INLET</p>
			</div>
			<div class="kilnInletValue">
				<p>-2,5 mbar</p>
				<p> 6,07%</p>
			</div>
			<div class="kilnInletValue2">
				<p>3,52 rpm</p>
				<p> 42%</p>
			</div>
			<div class="kilnInletTittle">
				<p>Pressure</p>
				<p>O2</p>
			</div>
			<div class="kilnInletTittle2">
				<p>Kiln Speed</p>
				<p>Kiln Torque</p>
			</div>
			<div class="id532BE03">
				<div class="square-content">
					<span>532BE03</span>
				</div>
			</div>
			<div class="id532BE03Value">
				<p id="get_KILN_2"></p>
			</div>
			<div class="id541BE01">
				<p>541BE01</p>
			</div>
			<div class="id541BE01Value">
				<p id="get_KILN_4"></p>
			</div>
			<div class="id541AS01">
				<p>541AS01</p>
			</div>
			<div class="id541DG01">
				<p>541DG01</p>
			</div>
			<div class="id541AS08">
				<p>541AS08</p>
			</div>
			<div class="id541AS09">
				<p>541AS09</p>
			</div>
			<div class="airCondition">
				<p>O2</p>
				<p>CO</p>
				<p>NO</p>
			</div>
			<div class="airConditionValue">
				<p id="get_KILN_14"></p>
				<p id="get_KILN_15"></p>
				<p id="get_KILN_16"></p>
			</div>
			<div class="id541AS02">
				<p>541AS02</p>
			</div>
			<div class="id541AS03">
				<p>541AS03</p>
			</div>
			<div class="id541AS04">
				<p>541AS04</p>
			</div>
			<div class="id541DG04">
				<p>541DG04</p>
			</div>
			<div class="id541DG04value">
				<p id="get_KILN_6"> </p>
			</div>
			<div class="id541DG05">
				<p>541DG05</p>
			</div>
			<div class="id541DG05Value">
				<p id="get_KILN_7"></p>
			</div>
			<div class="clinkerProd">
				<p>CLINKER PROD</p>
			</div>
			<div class="clinkerProdValue">
				<p>8417 ton</p>
				<p>3317 ton</p>
			</div>
			<div class="id543FN04">
				<p>543FN04</p>
			</div>
			<div class="id543FN04Value">
				<p>75 %</p>
				<p>210 mbar</p>
			</div>
			<div class="id543FN04Value2">
				<p>58 %</p>
			</div>
			<div class="id431FN570">
				<p>431FN570</p>
			</div>
			<div class="id431FN570Value">
				<p>75 %</p>
				<p>310 Nm3/min</p>
				<p>210 mbar</p>
			</div>
			<div class="kilnValue1">
				<p>960°C</p>
			</div>
			<div class="kilnValue2">
				<p>-0,44 mbar</p>
			</div>
			<div class="kilnValue3">
				<p>900°C</p>
			</div>
			<div class="kilnValue4">
				<p>490</p>
				<p>406</p>
				<p>1025</p>
			</div>
			<div class="kilnValue4Par">
				<p>A</p>
				<p>KW</p>
				<p>rpm</p>
			</div>
			<div class="kilnValue5">
				<p>492</p>
				<p>406</p>
				<p>1021</p>
			</div>
			<div class="kilnValue5Par">
				<p>A</p>
				<p>KW</p>
				<p>rpm</p>
			</div>
			<div class="value1">
				<p>976°C</p>
			</div>
			<div class="id533FN03">
				<p>533FN03</p>
			</div>
			<div class="id533FN03Value">
				<p>90 %</p>
			</div>
			<div class="id533FN03Value2">
				<p id="get_KILN_10"></p>
			</div>
			<div class="id533FN02">
				<p>533FN02</p>
			</div>
			<div class="id533FN02Value">
				<p>90 %</p>
			</div>
			<div class="id533FN02Value2">
				<p id="get_KILN_12"></p>
			</div>
			<div class="idValue1">
				<p>407°C</p>
				-52 mbar
			</div>
			<div class="idValue2">
				<p>409°C</p>
				-58 mbar
			</div>
			<div class="RF01Value">
				<p id="get_KILN_8"></p>
			</div>
			<div class="RF02Value">
				<p id="get_KILN_9"></p>
			</div>
			<div class="RF03Value">
				<p id="get_KILN_18"></p>
			</div>
			<div class="RF04Value">
				<p id="get_KILN_17"></p>
			</div>
			<div class="value2">
				<p>607°C</p>
				<p>-35,8 mbar</p>
			</div>
			<div class="value3">
				<p>601°C</p>
				<p>-40,6 mbar</p>
			</div>
			<div class="value4">
				<p>787°C</p>
				<p>-25,2 mbar</p>
			</div>
			<div class="value5">
				<p>778°C</p>
				<p>-24,6 mbar</p>
			</div>
			<div class="value6">
				<p>888°C</p>
				<p>-16,0 mbar</p>
			</div>
			<div class="value7">
				<p id="get_KILN_44"></p>
				<p>-18,6 mbar</p>
			</div>
			<div class="spanButton1">
				<div class="square-content">
					<a href=" ">
						<button type="button" class="btn btn-default" style="width: 100%; margin-top: 10px;">Cooler</button></a>
				</div>
			</div>
			<div class="spanButton2">
				<div class="square-content">
					<a href=" ">
						<button disabled type="button" class="btn btn-default" style="width: 100%; margin-top: 10px;">Kiln</button></a>
				</div>
			</div>
		</div>
	</div>
</body>


<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		var url = 'http://10.15.5.150/dev/par4digma/api/index.php/plant_tonasa/kiln_t5';
		$.ajax({
			url: url,
			type: 'get',
			// dataType: 'json',
			success: function(data) {
				var data1 = data.replace("<title>Json</title>", " ");
				var data2 = data1.replace("(", "[");
				var data3 = data2.replace(");", "]");
				var dataJson = JSON.parse(data3);

				get_KILN_1 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2) + " %";
				$("#get_KILN_1").html(get_KILN_1);

				get_KILN_2 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2) + " %";
				$("#get_KILN_2").html(get_KILN_2);

				get_KILN_3 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2) + " ";
				$("#get_KILN_3").html(get_KILN_3);

				get_KILN_4 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2) + " %";
				$("#get_KILN_4").html(get_KILN_4);

				get_KILN_5 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2) + " %";
				$("#get_KILN_5").html(get_KILN_5);

				get_KILN_6 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2) + " %";
				$("#get_KILN_6").html(get_KILN_6);

				get_KILN_7 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2) + " %";
				$("#get_KILN_7").html(get_KILN_7);

				get_KILN_8 = parseFloat(dataJson[0].tags[8].props[0].val).toFixed(2) + " %";
				$("#get_KILN_8").html(get_KILN_8);

				get_KILN_9 = parseFloat(dataJson[0].tags[9].props[0].val).toFixed(2) + " %";
				$("#get_KILN_9").html(get_KILN_9);

				get_KILN_10 = parseFloat(dataJson[0].tags[10].props[0].val).toFixed(2) + " mm/s";
				$("#get_KILN_10").html(get_KILN_10);

				get_KILN_12 = parseFloat(dataJson[0].tags[12].props[0].val).toFixed(2) + " mm/s";
				$("#get_KILN_12").html(get_KILN_12);

				get_KILN_14 = parseFloat(dataJson[0].tags[14].props[0].val).toFixed(2) + " %";
				$("#get_KILN_14").html(get_KILN_14);

				get_KILN_15 = parseFloat(dataJson[0].tags[15].props[0].val).toFixed(2) + " %";
				$("#get_KILN_15").html(get_KILN_15);

				get_KILN_16 = parseFloat(dataJson[0].tags[16].props[0].val).toFixed(2) + " ppm";
				$("#get_KILN_16").html(get_KILN_16);

				get_KILN_17 = parseFloat(dataJson[0].tags[17].props[0].val).toFixed(2) + " %";
				$("#get_KILN_17").html(get_KILN_17);

				get_KILN_18 = parseFloat(dataJson[0].tags[18].props[0].val).toFixed(2) + " %";
				$("#get_KILN_18").html(get_KILN_18);

				get_KILN_19 = parseFloat(dataJson[0].tags[19].props[0].val).toFixed(2) + " ";
				$("#get_KILN_19").html(get_KILN_19);

				get_KILN_20 = parseFloat(dataJson[0].tags[20].props[0].val).toFixed(2) + " ";
				$("#get_KILN_20").html(get_KILN_20);

				get_KILN_21 = parseFloat(dataJson[0].tags[21].props[0].val).toFixed(2) + " ";
				$("#get_KILN_21").html(get_KILN_21);

				get_KILN_22 = parseFloat(dataJson[0].tags[22].props[0].val).toFixed(2) + " ";
				$("#get_KILN_22").html(get_KILN_22);



				get_KILN_23 = parseFloat(dataJson[0].tags[23].props[0].val).toFixed(2) + " ";
				$("#get_KILN_23").html(get_KILN_23);

				get_KILN_24 = parseFloat(dataJson[0].tags[24].props[0].val).toFixed(2) + " ";
				$("#get_KILN_24").html(get_KILN_24);

				get_KILN_25 = parseFloat(dataJson[0].tags[25].props[0].val).toFixed(2) + " ";
				$("#get_KILN_25").html(get_KILN_25);

				get_KILN_26 = parseFloat(dataJson[0].tags[26].props[0].val).toFixed(2) + " ";
				$("#get_KILN_26").html(get_KILN_26);

				get_KILN_27 = parseFloat(dataJson[0].tags[27].props[0].val).toFixed(2) + " ";
				$("#get_KILN_27").html(get_KILN_27);

				get_KILN_28 = parseFloat(dataJson[0].tags[28].props[0].val).toFixed(2) + " ";
				$("#get_KILN_28").html(get_KILN_28);

				get_KILN_29 = parseFloat(dataJson[0].tags[29].props[0].val).toFixed(2) + " ";
				$("#get_KILN_29").html(get_KILN_29);

				get_KILN_30 = parseFloat(dataJson[0].tags[30].props[0].val).toFixed(2) + " ";
				$("#get_KILN_30").html(get_KILN_30);

				get_KILN_31 = parseFloat(dataJson[0].tags[31].props[0].val).toFixed(2) + " ";
				$("#get_KILN_31").html(get_KILN_31);

				get_KILN_32 = parseFloat(dataJson[0].tags[32].props[0].val).toFixed(2) + " ";
				$("#get_KILN_32").html(get_KILN_32);

				get_KILN_33 = parseFloat(dataJson[0].tags[33].props[0].val).toFixed(2) + " ";
				$("#get_KILN_33").html(get_KILN_33);

				get_KILN_34 = parseFloat(dataJson[0].tags[34].props[0].val).toFixed(2) + " ";
				$("#get_KILN_34").html(get_KILN_34);

				get_KILN_35 = parseFloat(dataJson[0].tags[35].props[0].val).toFixed(2) + " ";
				$("#get_KILN_35").html(get_KILN_35);

				get_KILN_36 = parseFloat(dataJson[0].tags[36].props[0].val).toFixed(2) + " ";
				$("#get_KILN_36").html(get_KILN_36);

				get_KILN_37 = parseFloat(dataJson[0].tags[37].props[0].val).toFixed(2) + " ";
				$("#get_KILN_37").html(get_KILN_37);

				get_KILN_38 = parseFloat(dataJson[0].tags[38].props[0].val).toFixed(2) + " ";
				$("#get_KILN_38").html(get_KILN_38);

				get_KILN_39 = parseFloat(dataJson[0].tags[39].props[0].val).toFixed(2) + " ";
				$("#get_KILN_39").html(get_KILN_39);

				get_KILN_40 = parseFloat(dataJson[0].tags[40].props[0].val).toFixed(2) + " ";
				$("#get_KILN_40").html(get_KILN_40);

				get_KILN_41 = parseFloat(dataJson[0].tags[41].props[0].val).toFixed(2) + " ";
				$("#get_KILN_41").html(get_KILN_41);

				get_KILN_42 = parseFloat(dataJson[0].tags[42].props[0].val).toFixed(2) + " ";
				$("#get_KILN_42").html(get_KILN_42);

				get_KILN_43 = parseFloat(dataJson[0].tags[43].props[0].val).toFixed(2) + " ";
				$("#get_KILN_43").html(get_KILN_43);

				get_KILN_44 = parseFloat(dataJson[0].tags[44].props[0].val).toFixed(2) + " ";
				$("#get_KILN_44").html(get_KILN_44);

				get_KILN_45 = parseFloat(dataJson[0].tags[45].props[0].val).toFixed(2) + " ";
				$("#get_KILN_45").html(get_KILN_45);

				get_KILN_46 = parseFloat(dataJson[0].tags[46].props[0].val).toFixed(2) + " ";
				$("#get_KILN_46").html(get_KILN_46);

				get_KILN_47 = parseFloat(dataJson[0].tags[47].props[0].val).toFixed(2) + " ";
				$("#get_KILN_47").html(get_KILN_47);

				get_KILN_48 = parseFloat(dataJson[0].tags[48].props[0].val).toFixed(2) + " ";
				$("#get_KILN_48").html(get_KILN_48);

				get_KILN_49 = parseFloat(dataJson[0].tags[49].props[0].val).toFixed(2) + " ";
				$("#get_KILN_49").html(get_KILN_49);


				get_KILN_50 = parseFloat(dataJson[0].tags[50].props[0].val).toFixed(2) + " ";
				$("#get_KILN_50").html(get_KILN_50);
				// 
				get_KILN_51 = parseFloat(dataJson[0].tags[51].props[0].val).toFixed(2) + " t/h";
				$("#get_KILN_51").html(get_KILN_51);

				get_KILN_52 = parseFloat(dataJson[0].tags[52].props[0].val).toFixed(2) + " ";
				$("#get_KILN_52").html(get_KILN_52);

				get_KILN_53 = parseFloat(dataJson[0].tags[53].props[0].val).toFixed(2) + " ";
				$("#get_KILN_53").html(get_KILN_53);

				get_KILN_54 = parseFloat(dataJson[0].tags[54].props[0].val).toFixed(2) + " ";
				$("#get_KILN_54").html(get_KILN_54);

				get_KILN_55 = parseFloat(dataJson[0].tags[55].props[0].val).toFixed(2) + " ";
				$("#get_KILN_55").html(get_KILN_55);

				get_KILN_56 = parseFloat(dataJson[0].tags[56].props[0].val).toFixed(2) + " ";
				$("#get_KILN_56").html(get_KILN_56);

				get_KILN_57 = parseFloat(dataJson[0].tags[57].props[0].val).toFixed(2) + " ";
				$("#get_KILN_57").html(get_KILN_57);

				get_KILN_58 = parseFloat(dataJson[0].tags[58].props[0].val).toFixed(2) + " t/h";
				$("#get_KILN_58").html(get_KILN_58);

				get_KILN_59 = parseFloat(dataJson[0].tags[59].props[0].val).toFixed(2) + " t/h";
				$("#get_KILN_59").html(get_KILN_59);

				get_KILN_62 = parseFloat(dataJson[0].tags[62].props[0].val).toFixed(2) + " ";
				$("#get_KILN_62").html(get_KILN_62);

				get_KILN_63 = parseFloat(dataJson[0].tags[63].props[0].val).toFixed(2) + " ";
				$("#get_KILN_63").html(get_KILN_63);

				get_KILN_64 = parseFloat(dataJson[0].tags[64].props[0].val).toFixed(2) + " ";
				$("#get_KILN_64").html(get_KILN_64);

				get_KILN_66 = parseFloat(dataJson[0].tags[66].props[0].val).toFixed(2) + " ";
				$("#get_KILN_66").html(get_KILN_66);

				get_KILN_67 = parseFloat(dataJson[0].tags[67].props[0].val).toFixed(2) + " ";
				$("#get_KILN_67").html(get_KILN_67);

				get_KILN_68 = parseFloat(dataJson[0].tags[68].props[0].val).toFixed(2) + " ";
				$("#get_KILN_68").html(get_KILN_68);

				get_KILN_69 = parseFloat(dataJson[0].tags[69].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_69").html(get_KILN_69);

				get_KILN_70 = parseFloat(dataJson[0].tags[70].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_70").html(get_KILN_70);

				get_KILN_71 = parseFloat(dataJson[0].tags[71].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_71").html(get_KILN_71);

				get_KILN_72 = parseFloat(dataJson[0].tags[72].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_72").html(get_KILN_72);

				get_KILN_73 = parseFloat(dataJson[0].tags[73].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_73").html(get_KILN_73);

				get_KILN_74 = parseFloat(dataJson[0].tags[74].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_74").html(get_KILN_74);

				get_KILN_75 = parseFloat(dataJson[0].tags[75].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_75").html(get_KILN_75);

				get_KILN_76 = parseFloat(dataJson[0].tags[76].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_76").html(get_KILN_76);

				get_KILN_77 = parseFloat(dataJson[0].tags[77].props[0].val).toFixed(2) + " °C";
				$("#get_KILN_77").html(get_KILN_77);

				get_KILN_78 = parseFloat(dataJson[0].tags[78].props[0].val).toFixed(2) + " ";
				$("#get_KILN_78").html(get_KILN_78);

				get_KILN_79 = parseFloat(dataJson[0].tags[79].props[0].val).toFixed(2) + " ";
				$("#get_KILN_79").html(get_KILN_79);

				get_KILN_80 = parseFloat(dataJson[0].tags[80].props[0].val).toFixed(2) + " ";
				$("#get_KILN_80").html(get_KILN_80);

				get_KILN_81 = parseFloat(dataJson[0].tags[81].props[0].val).toFixed(2) + " ";
				$("#get_KILN_81").html(get_KILN_81);

				get_KILN_82 = parseFloat(dataJson[0].tags[82].props[0].val).toFixed(2) + " ";
				$("#get_KILN_82").html(get_KILN_82);

				get_KILN_83 = parseFloat(dataJson[0].tags[83].props[0].val).toFixed(2) + " ";
				$("#get_KILN_83").html(get_KILN_83);

				get_KILN_84 = parseFloat(dataJson[0].tags[84].props[0].val).toFixed(2) + " ";
				$("#get_KILN_84").html(get_KILN_84);

				get_KILN_85 = parseFloat(dataJson[0].tags[85].props[0].val).toFixed(2) + " ";
				$("#get_KILN_85").html(get_KILN_85);

				get_KILN_86 = parseFloat(dataJson[0].tags[86].props[0].val).toFixed(2) + " ";
				$("#get_KILN_86").html(get_KILN_86);

				get_KILN_87 = parseFloat(dataJson[0].tags[87].props[0].val).toFixed(2) + " ";
				$("#get_KILN_87").html(get_KILN_87);

				get_KILN_88 = parseFloat(dataJson[0].tags[88].props[0].val).toFixed(2) + " ";
				$("#get_KILN_88").html(get_KILN_88);



				// tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
				// tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
				// tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
				// tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
				// tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
				// tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
				// tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
				// tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
				// $("#isisilo10").html(tonageSilo10);
				// $("#isisilo11").html(tonageSilo11);
				// $("#isisilo12").html(tonageSilo12);
			}
		});
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>