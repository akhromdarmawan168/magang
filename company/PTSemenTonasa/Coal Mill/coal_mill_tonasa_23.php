<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('coal_mill_tonasa_23.png');
        }

        .bg1 {
            position: absolute;
            top: 30%;
            left: 2.5%;
            width: 5.6%;
            text-align: center;
            background: orange;
            font-size: 0.6vw;

        }

        .bg1:before {
            content: "";
            display: block;
            padding-bottom: 30%;
        }


        .reclaimert4 {
            position: absolute;
            top: 9.5%;
            left: 9.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .reclaimert4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M4 {
            position: absolute;
            top: 14.5%;
            left: 13.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M5 {
            position: absolute;
            top: 17.9%;
            left: 15.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M2 {
            position: absolute;
            top: 21.9%;
            left: 12.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M3 {
            position: absolute;
            top: 24.9%;
            left: 8.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2501 {
            position: absolute;
            top: 18.9%;
            left: 8.9%;
            width: 10%;
            text-align: left;
            color: white;
            font-size: 0.6vw;
        }

        .c2501:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c218200m1 {
            position: absolute;
            top: 26%;
            left: 16%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c218200m1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1401 {
            position: absolute;
            top: 29%;
            left: 14%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1401:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c141a {
            position: absolute;
            top: 31%;
            left: 9.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c141a:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c141b {
            position: absolute;
            top: 34.5%;
            left: 5.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c141b:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1402 {
            position: absolute;
            top: 36.9%;
            left: 5.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1402:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M21 {
            position: absolute;
            top: 44%;
            left: 4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M31 {
            position: absolute;
            top: 47%;
            left: 4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M11 {
            position: absolute;
            top: 50%;
            left: 4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value1 {
            position: absolute;
            top: 51%;
            left: -0.1%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1501 {
            position: absolute;
            top: 54%;
            left: 10.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1501:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1502m1 {
            position: absolute;
            top: 52.3%;
            left: 24%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1502m1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c251 {
            position: absolute;
            top: 56%;
            left: 20%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c251:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .c2502m2 {
            position: absolute;
            top: 59.6%;
            left: 3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2502m2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M41 {
            position: absolute;
            top: 79.9%;
            left: 8.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value2 {
            position: absolute;
            top: 76.9%;
            left: 11.9%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M51 {
            position: absolute;
            top: 79.9%;
            left: 15.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .coalmill1 {
            position: absolute;
            top: 71.8%;
            left: 11.8%;
            width: 10%;
            text-align: center;
            color: blue;
            font-size: 0.70vw;
            font-weight: bold;
        }

        .coalmill1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .coalmill1value {
            position: absolute;
            top: 68.1%;
            left: 14.5%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.70vw;
        }

        .coalmill1value:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2604 {
            position: absolute;
            top: 81.5%;
            left: 18.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2604:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .M23 {
            position: absolute;
            top: 85%;
            left: 19%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .M23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2603 {
            position: absolute;
            top: 76.3%;
            left: 27.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2603:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2602 {
            position: absolute;
            top: 81.3%;
            left: 28.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2602:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value3 {
            position: absolute;
            top: 86.3%;
            left: 28.5%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1804 {
            position: absolute;
            top: 27%;
            left: 28.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1804:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c184c {
            position: absolute;
            top: 27%;
            left: 33.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c184c:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1801m1 {
            position: absolute;
            top: 24%;
            left: 35.1%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1801m1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1801m2 {
            position: absolute;
            top: 24%;
            left: 41%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1801m2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c1904 {
            position: absolute;
            top: 25%;
            left: 51%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c1904:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c194a {
            position: absolute;
            top: 27.8%;
            left: 46.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c194a:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value8 {
            position: absolute;
            top: 27.8%;
            left: 50.4%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .fa01 {
            position: absolute;
            top: 26.8%;
            left: 55.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .fa01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2503m8 {
            position: absolute;
            top: 34%;
            left: 39%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2503m8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2503m6 {
            position: absolute;
            top: 54%;
            left: 39%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2503m6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c194am3 {
            position: absolute;
            top: 61.5%;
            left: 39%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c194am3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .cbc01 {
            position: absolute;
            top: 63%;
            left: 35%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .cbc01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .cbc02 {
            position: absolute;
            top: 69%;
            left: 34%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .cbc02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .coalmill2 {
            position: absolute;
            top: 71.8%;
            left: 42%;
            width: 10%;
            text-align: center;
            color: blue;
            font-size: 0.70vw;
            font-weight: bold;
        }

        .coalmill2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .coalmill2value {
            position: absolute;
            top: 68.1%;
            left: 44.5%;
            width: 10%;
            text-align: left;
            color: red;
            font-size: 0.70vw;
        }

        .coalmill2value:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .cop01 {
            position: absolute;
            top: 79.8%;
            left: 38.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .cop01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value4 {
            position: absolute;
            top: 77.8%;
            left: 41.7%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .cm02 {
            position: absolute;
            top: 80.8%;
            left: 45.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .cm02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .fa03 {
            position: absolute;
            top: 82%;
            left: 49.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .fa03:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .fa04 {
            position: absolute;
            top: 85.4%;
            left: 49.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .fa04:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value5 {
            position: absolute;
            top: 86.4%;
            left: 58.2%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .fk01 {
            position: absolute;
            top: 81.4%;
            left: 59%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .fk01:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3001 {
            position: absolute;
            top: 82.4%;
            left: 62%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3001:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3002 {
            position: absolute;
            top: 82.4%;
            left: 64.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3002:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3003 {
            position: absolute;
            top: 82.4%;
            left: 66.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3003:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c194bm3 {
            position: absolute;
            top: 56%;
            left: 64%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c194bm3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .sc01b1 {
            position: absolute;
            top: 66%;
            left: 65%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .sc01b1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .sc01b2 {
            position: absolute;
            top: 70%;
            left: 61%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .sc01b2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .sc02 {
            position: absolute;
            top: 74%;
            left: 58%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .sc02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .sc03 {
            position: absolute;
            top: 79%;
            left: 53%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .sc03:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .fa02 {
            position: absolute;
            top: 65.4%;
            left: 57%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .fa02:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c4202y1 {
            position: absolute;
            top: 16%;
            left: 83%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c4202y1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3401 {
            position: absolute;
            top: 20%;
            left: 83.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3401:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3403 {
            position: absolute;
            top: 26%;
            left: 75.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3403:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3405 {
            position: absolute;
            top: 30%;
            left: 75.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3405:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3404 {
            position: absolute;
            top: 34.1%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3404:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3406 {
            position: absolute;
            top: 36.9%;
            left: 81%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3406:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value6 {
            position: absolute;
            top: 39.9%;
            left: 63%;
            width: 10%;
            text-align: right;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .value7 {
            position: absolute;
            top: 39.9%;
            left: 86.8%;
            width: 10%;
            text-align: center;
            color: #7CFF00;
            ;
            font-size: 0.6vw;
        }

        .value7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3608 {
            position: absolute;
            top: 60%;
            left: 82.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3608:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3607 {
            position: absolute;
            top: 62.5%;
            left: 79.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3607:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3609 {
            position: absolute;
            top: 62.5%;
            left: 82.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3609:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3604 {
            position: absolute;
            top: 68%;
            left: 79.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3604:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3605 {
            position: absolute;
            top: 68%;
            left: 82.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3605:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3606 {
            position: absolute;
            top: 68%;
            left: 84.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3606:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3601 {
            position: absolute;
            top: 74%;
            left: 78.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3601:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3602 {
            position: absolute;
            top: 74%;
            left: 81%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3602:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3603 {
            position: absolute;
            top: 74%;
            left: 83.6%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3603:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c2151001 {
            position: absolute;
            top: 75%;
            left: 72.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c2151001:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .c3151001 {
            position: absolute;
            top: 75%;
            left: 87.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .c3151001:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .totonasa2_1 {
            position: absolute;
            top: 80%;
            left: 69.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .totonasa2_1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .totonasa2_2 {
            position: absolute;
            top: 80%;
            left: 88.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .totonasa2_2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5007 {
            position: absolute;
            top: 79%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5007:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5008 {
            position: absolute;
            top: 79%;
            left: 82.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5008:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5004 {
            position: absolute;
            top: 86.4%;
            left: 78.9%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5004:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5005 {
            position: absolute;
            top: 86.4%;
            left: 82.3%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5005:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5006 {
            position: absolute;
            top: 86.4%;
            left: 86%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5006:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5001 {
            position: absolute;
            top: 94.5%;
            left: 77.4%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5001:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5002 {
            position: absolute;
            top: 94.5%;
            left: 80.8%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5002:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .C5003 {
            position: absolute;
            top: 94.5%;
            left: 84.3%;

            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;
        }

        .C5003:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelt2 {
            position: absolute;
            top: 42%;
            left: 76.8%;
            width: 10%;
            text-align: center;
            color: blue;
            font-size: 0.88vw;
            font-weight: bold;
        }

        .labelt2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .labelt3 {
            position: absolute;
            top: 42%;
            left: 85%;
            width: 10%;
            text-align: center;
            color: blue;
            font-size: 0.88vw;
            font-weight: bold;
        }

        .labelt3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        *,
        *:before,
        *:after {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        /* Vertical */

        .vertical .progress-bar {
            float: left;
            height: 300px;
            width: 40px;
            margin-right: 25px;
        }

        .vertical .progress-track {
            position: relative;
            width: 40px;
            height: 100%;
            background: #ebebeb;
        }

        .vertical .progress-fill {
            position: relative;
            background: red;
            height: 50%;
            width: 40px;
            color: #fff;
            text-align: center;
            font-family: "Lato", "Verdana", sans-serif;
            font-size: 12px;
            line-height: 20px;
        }

        .rounded .progress-track,
        .rounded .progress-fill {
            box-shadow: inset 0 0 5px rgba(0, 0, 0, .2);
            border-radius: 3px;
        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }

        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }

        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="reclaimert4">
                <div class="square-content">
                    <span>RECLAIMER T.IV</span>
                </div>
            </div>
            <div class="M4">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="M5">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="M2">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="M3">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="c2501">
                <div class="square-content">
                    <span>2501</span>
                </div>
            </div>
            <div class="c218200m1">
                <div class="square-content">
                    <span>218200M1</span>
                </div>
            </div>
            <div class="c1401">
                <div class="square-content">
                    <span>1401</span>
                </div>
            </div>
            <div class="c141a">
                <div class="square-content">
                    <span>141A</span>
                </div>
            </div>
            <div class="c141b">
                <div class="square-content">
                    <span>141B</span>
                </div>
            </div>
            <div class="c1402">
                <div class="square-content">
                    <span>1402</span>
                </div>
            </div>
            <div class="M21">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="M31">
                <div class="square-content">
                    <span>M3</span>
                </div>
            </div>
            <div class="M11">
                <div class="square-content">
                    <span>M1</span>
                </div>
            </div>
            <div class="value1">
                <div class="square-content">
                    <p style="margin-bottom: 1px">98 %</p>
                    <p>10.0 rpm</p>
                </div>
            </div>
            <div class="c1501">
                <div class="square-content">
                    <span>1501</span>
                </div>
            </div>
            <div class="c1502m1">
                <div class="square-content">
                    <span>1502M1</span>
                </div>
            </div>
            <div class="c251">
                <div class="square-content">
                    <span>251</span>
                </div>
            </div>
            <div class="c2502m2">
                <div class="square-content">
                    <span>2502M2</span>
                </div>
            </div>
            <div class="M41">
                <div class="square-content">
                    <span>M4</span>
                </div>
            </div>
            <div class="value2">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="M51">
                <div class="square-content">
                    <span>M5</span>
                </div>
            </div>
            <div class="coalmill1 ">
                <div class="square-content">
                    <span>COAL MILL 1</span>
                </div>
            </div>
            <div class="coalmill1value ">
                <div class="square-content">
                    <span>99.2%</span>
                </div>
            </div>
            <div class="c2604">
                <div class="square-content">
                    <span>2604</span>
                </div>
            </div>
            <div class="M23">
                <div class="square-content">
                    <span>M2</span>
                </div>
            </div>
            <div class="c2603">
                <div class="square-content">
                    <span>2603</span>
                </div>
            </div>
            <div class="c2602">
                <div class="square-content">
                    <span>2602</span>
                </div>
            </div>
            <div class="value3">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="c1804">
                <div class="square-content">
                    <span>1804</span>
                </div>
            </div>
            <div class="c184c">
                <div class="square-content">
                    <span>184A</span>
                </div>
            </div>
            <div class="c1801m1">
                <div class="square-content">
                    <span>1801M1</span>
                </div>
            </div>
            <div class="c1801m2">
                <div class="square-content">
                    <span>1801M2</span>
                </div>
            </div>
            <div class="c1904">
                <div class="square-content">
                    <span>1904</span>
                </div>
            </div>
            <div class="c194a">
                <div class="square-content">
                    <span>194 A</span>
                </div>
            </div>
            <div class="value8">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="fa01">
                <div class="square-content">
                    <span>FA01</span>
                </div>
            </div>
            <div class="c2503m8">
                <div class="square-content">
                    <span>2503M8</span>
                </div>
            </div>
            <div class="c2503m6">
                <div class="square-content">
                    <span>2503M6</span>
                </div>
            </div>
            <div class="c194am3">
                <div class="square-content">
                    <span>194AM3</span>
                </div>
            </div>
            <div class="cbc01">
                <div class="square-content">
                    <span>BC01</span>
                </div>
            </div>
            <div class="cbc02">
                <div class="square-content">
                    <span>BC02</span>
                </div>
            </div>
            <div class="coalmill2">
                <div class="square-content">
                    <span>COAL MILL 2</span>
                </div>
            </div>
            <div class="coalmill2value">
                <div class="square-content">
                    <span>99.8%</span>
                </div>
            </div>
            <div class="cop01">
                <div class="square-content">
                    <span>OP01</span>
                </div>
            </div>
            <div class="value4">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="cm02">
                <div class="square-content">
                    <span>CM02</span>
                </div>
            </div>
            <div class="fa03">
                <div class="square-content">
                    <span>FA03</span>
                </div>
            </div>
            <div class="fa04">
                <div class="square-content">
                    <span>FA04</span>
                </div>
            </div>
            <div class="value5">
                <div class="square-content">
                    <span>91 A</span>
                </div>
            </div>
            <div class="fk01">
                <div class="square-content">
                    <span>FK01</span>
                </div>
            </div>
            <div class="c3001">
                <div class="square-content">
                    <span>3001</span>
                </div>
            </div>
            <div class="c3002">
                <div class="square-content">
                    <span>3002</span>
                </div>
            </div>
            <div class="c3003">
                <div class="square-content">
                    <span>3003</span>
                </div>
            </div>
            <div class="c194BM3">
                <div class="square-content">
                    <span>194BM3</span>
                </div>
            </div>
            <div class="sc01b1">
                <div class="square-content">
                    <span>SC01B</span>
                </div>
            </div>
            <div class="sc01b2">
                <div class="square-content">
                    <span>SC01B</span>
                </div>
            </div>
            <div class="sc02">
                <div class="square-content">
                    <span>SC02</span>
                </div>
            </div>
            <div class="sc03">
                <div class="square-content">
                    <span>SC03</span>
                </div>
            </div>
            <div class="fa02">
                <div class="square-content">
                    <span>FA02</span>
                </div>
            </div>
            <div class="c4202y1">
                <div class="square-content">
                    <span>4202Y1</span>
                </div>
            </div>
            <div class="c3401">
                <div class="square-content">
                    <span>3401</span>
                </div>
            </div>
            <div class="c3403">
                <div class="square-content">
                    <span>3403</span>
                </div>
            </div>
            <div class="c3405">
                <div class="square-content">
                    <span>3405</span>
                </div>
            </div>
            <div class="c3404">
                <div class="square-content">
                    <span>3404</span>
                </div>
            </div>
            <div class="c3406">
                <div class="square-content">
                    <span>3406</span>
                </div>
            </div>
            <div class="value6">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">30.0 ton</p>
                    <p>39,46°C</p>
                </div>
            </div>
            <div class="value7">
                <div class="square-content">
                    <p style="margin-bottom: 1px;">30.0 ton</p>
                    <p>39,46°C</p>
                </div>
            </div>
            <div class="c3608">
                <div class="square-content">
                    <span>3608</span>
                </div>
            </div>
            <div class="c3607">
                <div class="square-content">
                    <span>3607</span>
                </div>
            </div>
            <div class="c3609">
                <div class="square-content">
                    <span>3609</span>
                </div>
            </div>
            <div class="c3604">
                <div class="square-content">
                    <span>3604</span>
                </div>
            </div>
            <div class="c3605">
                <div class="square-content">
                    <span>3605</span>
                </div>
            </div>
            <div class="c3606">
                <div class="square-content">
                    <span>3606</span>
                </div>
            </div>
            <div class="c3601">
                <div class="square-content">
                    <span>3601</span>
                </div>
            </div>
            <div class="c3602">
                <div class="square-content">
                    <span>3602</span>
                </div>
            </div>
            <div class="c3603">
                <div class="square-content">
                    <span>3603</span>
                </div>
            </div>
            <div class="c2151001">
                <div class="square-content">
                    <span>2151001</span>
                </div>
            </div>
            <div class="c3151001">
                <div class="square-content">
                    <span>3151001</span>
                </div>
            </div>
            <div class="totonasa2_1">
                <div class="square-content">
                    <span>TO TONASA 2</span>
                </div>
            </div>
            <div class="totonasa2_2">
                <div class="square-content">
                    <span>TO TONASA 2</span>
                </div>
            </div>
            <div class="c5007">
                <div class="square-content">
                    <span>5007</span>
                </div>
            </div>
            <div class="c5008">
                <div class="square-content">
                    <span>5008</span>
                </div>
            </div>
            <div class="c5004">
                <div class="square-content">
                    <span>5004</span>
                </div>
            </div>
            <div class="c5005">
                <div class="square-content">
                    <span>5005</span>
                </div>
            </div>
            <div class="c5006">
                <div class="square-content">
                    <span>5006</span>
                </div>
            </div>
            <div class="c5001">
                <div class="square-content">
                    <span>5001</span>
                </div>
            </div>
            <div class="c5002">
                <div class="square-content">
                    <span>5002</span>
                </div>
            </div>
            <div class="c5003">
                <div class="square-content">
                    <span>5003</span>
                </div>
            </div>
            <div class="labelt2">
                <div class="square-content">
                    <span>T2</span>
                </div>
            </div>
            <div class="labelt3">
                <div class="square-content">
                    <span>T3</span>
                </div>
            </div>
            <!-- 
        <div class="container vertical flat">
            <div class="progress-bar">
                <div class="progress-track">
                    <div class="progress-fill">
                        <span>12%</span>
                    </div>
                </div>
            </div>
        </div> -->
            <div class="bg1">
                <div class='square-content'>
                    <div>
                        <span>
                            <a style="color: white;font-weight: bold;" href="">HOT GAS</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        var url = 'ISIKAN URL API';
        $.ajax({
            url: url,
            type: 'get',
            // dataType: 'json',
            success: function(data) {
                var data1 = data.replace("<title>Json</title>", "");
                var data2 = data1.replace("(", "[");
                var data3 = data2.replace(");", "]");
                var dataJson = JSON.parse(data3);
                tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
                tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
                tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
                tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
                tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
                tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
                tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
                tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
                $("#isisilo10").html(tonageSilo10);
                $("#isisilo11").html(tonageSilo11);
                $("#isisilo12").html(tonageSilo12);
            }
        });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
    // $('.horizontal .progress-fill span').each(function() {
    //     var percent = $(this).html();
    //     $(this).parent().css('width', percent);
    // });


    // $('.vertical .progress-fill span').each(function() {
    //     var percent = $(this).html();
    //     var pTop = 100 - (percent.slice(0, percent.length - 1)) + "%";
    //     $(this).parent().css({
    //         'height': percent,
    //         'top': pTop
    //     });
    // });
</script>

</body>

</html>

</html>