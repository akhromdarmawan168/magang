<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: auto;
            border: 1px solid #ddd;
        }


        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('coal_mill tonasa5.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }


        .tittle1 {
            position: absolute;
            width: auto;
            top: 39.8%;
            left: 6%;
            font-size: 0.7vw;
            color: white;
        }

        .tittle2 {
            position: absolute;
            width: auto;
            top: 26%;
            left: 54.1%;
            font-size: 0.7vw;
            color: white;

        }

        .tittle3 {
            position: absolute;
            width: auto;
            top: 26%;
            left: 59.1%;
            font-size: 0.7vw;
            color: white;

        }

        .tittle4 {
            position: absolute;
            width: auto;
            top: 26%;
            left: 64%;
            font-size: 0.7vw;
            color: white;

        }

        .tittle5 {
            position: absolute;
            width: auto;
            top: 32%;
            left: 52.5%;
            font-size: 0.7vw;
            color: white;

        }

        .tittle6 {
            position: absolute;
            width: auto;
            top: 46.2%;
            left: 73%;
            font-size: 0.7vw;
            color: white;

        }


        .value1 {
            position: absolute;
            width: auto;
            top: 45.5%;
            left: 20%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value2 {
            position: absolute;
            width: auto;
            top: 47.5%;
            left: 28%;
            font-size: 0.7vw;
            color: #13962B;
        }


        .value3 {
            position: absolute;
            width: auto;
            top: 73.5%;
            left: 21%;
            font-size: 0.7vw;
            color: #13962B;
        }


        .value4 {
            position: absolute;
            width: auto;
            top: 87.5%;
            left: 22%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value5 {
            position: absolute;
            width: auto;
            top: 40%;
            left: 29.5%;
            font-size: 0.7vw;
            color: #13962B;



        }



        .value6 {
            position: absolute;
            width: auto;
            top: 79.4%;
            left: 29%;
            font-size: 0.7vw;
            color: #13962B;
        }



        .value7 {
            position: absolute;
            width: auto;
            top: 71.5%;
            left: 38%;
            font-size: 0.7vw;
            color: #13962B;

        }


        .value8 {
            position: absolute;
            width: auto;
            top: 20.5%;
            left: 54%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value9 {
            position: absolute;
            width: auto;
            top: 20.5%;
            left: 59%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value10 {
            position: absolute;
            width: auto;
            top: 17.5%;
            left: 63.7%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value11 {
            position: absolute;
            width: auto;
            top: 32%;
            left: 56%;
            font-size: 0.7vw;
            color: #13962B;
        }


        .value12 {
            position: absolute;
            width: auto;
            top: 46%;
            left: 76.7%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value13 {
            position: absolute;
            width: auto;
            top: 80%;
            left: 67%;
            font-size: 0.7vw;
            color: #13962B;
            text-align: right;
        }

        .value14 {
            position: absolute;
            width: auto;
            top: 80%;
            left: 78.5%;
            font-size: 0.7vw;
            color: #13962B;
            text-align: right;
        }

        .value15 {
            position: absolute;
            width: auto;
            top: 80%;
            left: 89%;
            font-size: 0.7vw;
            color: #13962B;
            text-align: right;
        }

        .value16 {
            position: absolute;
            width: auto;
            top: 86%;
            left: 68%;
            font-size: 0.7vw;
            color: #13962B;
            text-align: right;

        }

        .value17 {
            position: absolute;
            width: auto;
            top: 86%;
            left: 82.6%;
            font-size: 0.7vw;
            color: #13962B;

        }

        .value18 {
            position: absolute;
            width: auto;
            top: 85%;
            left: 93%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value19 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 69%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value20 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 74%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value21 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 80%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value22 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 86%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value23 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 90.5%;
            font-size: 0.7vw;
            color: #13962B;
        }

        .value24 {
            position: absolute;
            width: auto;
            top: 97.5%;
            left: 95%;
            font-size: 0.7vw;
            color: #13962B;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">


    <div class="penuh">
        <div class="pinch">
            <div class="tittle1">
                <p>140 t</p>
            </div>
            <div class="tittle2">
                <p>BV02</p>
            </div>
            <div class="tittle3">
                <p>LD01</p>
            </div>
            <div class="tittle4">
                <p>545FN04</p>
            </div>
            <div class="tittle5">
                <p style="margin-bottom: 2px">02</p>
                <p>CO2</p>
            </div>
            <div class="tittle6">
                <p style="margin-bottom: 2px">02</p>
                <p>CO2</p>
            </div>
            <div class="value1">
                <p>40 m3/h</p>
            </div>
            <div class="value2">
                <p>76%</p>
            </div>
            <div class="value3">
                <p>96 bar</p>
            </div>
            <div class="value4">
                <p>4964 KW</p>
            </div>
            <div class="value5">
                <p style="margin-bottom: 2px"> 99°C</p>
                <p>-68,6 bar</p>
            </div>
            <div class="value6">
                <p>61,3 mbar</p>
            </div>
            <div class="value7">
                <p style="margin-bottom: 2px">07°C</p>
                <p>-5,6 bar</p>
            </div>
            <div class="value8">
                <p>94,6 %</p>
            </div>
            <div class="value9">
                <p>94,6 %</p>
            </div>
            <div class="value10">
                <p style="margin-bottom: 2px">0,2 mm/s</p>
                <p>0,4 mm/s</p>
            </div>
            <div class="value11">
                <p style="margin-bottom: 2px">2,17 mm/s</p>
                <p>0,03 ppm</p>
            </div>
            <div class="value12">
                <p style="margin-bottom: 2px">2,17 %</p>
                <p>0,03 ppm</p>
            </div>
            <div class="value13">
                <p style="margin-bottom: 2px">0,2 t</p>
                <p>0,4 t/h</p>
            </div>
            <div class="value14">
                <p style="margin-bottom: 2px">0,2 t</p>
                <p>0,4 t/h</p>
            </div>
            <div class="value15">
                <p style="margin-bottom: 2px">0,2 t</p>
                <p>0,4 t/h</p>
            </div>
            <div class="value16">
                <p>307°C</p>
            </div>
            <div class="value17">
                <p>307°C</p>
            </div>
            <div class="value18">
                <p>307°C</p>
            </div>
            <div class="value19">
                <p>94,6 %</p>
            </div>
            <div class="value20">
                <p>94,6 %</p>
            </div>
            <div class="value21">
                <p>94,6 %</p>
            </div>
            <div class="value22">
                <p>94,6 %</p>
            </div>
            <div class="value23">
                <p>94,6 %</p>
            </div>
            <div class="value24">
                <p>94,6 %</p>
            </div>
        </div>
    </div>

</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        var url = 'ISIKAN URL API';
        $.ajax({
            url: url,
            type: 'get',
            // dataType: 'json',
            success: function(data) {
                var data1 = data.replace("<title>Json</title>", "");
                var data2 = data1.replace("(", "[");
                var data3 = data2.replace(");", "]");
                var dataJson = JSON.parse(data3);
                tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
                tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
                tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
                tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
                tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
                tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
                tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
                tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
                $("#isisilo10").html(tonageSilo10);
                $("#isisilo11").html(tonageSilo11);
                $("#isisilo12").html(tonageSilo12);
            }
        });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>

</html>