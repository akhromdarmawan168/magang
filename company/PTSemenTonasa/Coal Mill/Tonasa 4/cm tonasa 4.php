<html>

<head>
    <title>HMI Interface</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/magang/css/style.css" rel="stylesheet">
    <script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
    <style>
        .penuh {
            position: relative;
            width: 93%;
            height: 100%;
            padding-bottom: 0%;
            left: 3%;
            overflow: hidden;
        }

        .pinch {
            position: absolute;
            width: 100%;
            overflow: hidden;
            background-size: 100% 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-image: url('cm tonasa 4.png');

        }

        .pinch:before {
            content: "";
            display: block;
            padding-bottom: 54.5%;
        }


        .span1 {
            position: absolute;
            top: 10%;
            left: 17%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span1:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span2 {
            position: absolute;
            top: 10%;
            left: 60.35%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span2:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span3 {
            position: absolute;
            top: 35%;
            left: 59.15%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span3:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span4 {
            position: absolute;
            top: 57.5%;
            left: 74.9%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span4:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span5 {
            position: absolute;
            top: 74.5%;
            left: 85.3%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span5:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span6 {
            position: absolute;
            top: 91%;
            left: 78.8%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span6:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span7 {
            position: absolute;
            top: 80.5%;
            left: 35.8%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.7vw;
            font-weight: bold;
        }


        .span7:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span8 {
            position: absolute;
            top: 80.5%;
            left: 25.4%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.7vw;
            font-weight: bold;
        }


        .span8:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span9 {
            position: absolute;
            top: 80.5%;
            left: 16.7%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.7vw;
            font-weight: bold;
        }


        .span9:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span10 {
            position: absolute;
            top: 35%;
            left: 3%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span10:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span11 {
            position: absolute;
            top: 37%;
            left: 3%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span11:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span12 {
            position: absolute;
            top: 37%;
            left: 9%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span12:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span13 {
            position: absolute;
            top: 35%;
            left: 9%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span13:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span14 {
            position: absolute;
            top: 32%;
            left: 35%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span14:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span15 {
            position: absolute;
            top: 29%;
            left: 47.8%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span15:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span16 {
            position: absolute;
            top: 35.3%;
            left: 42.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span16:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span17 {
            position: absolute;
            top: 35.3%;
            left: 49.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span17:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span18 {
            position: absolute;
            top: 36.5%;
            left: 49.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span18:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span19 {
            position: absolute;
            top: 7%;
            left: 72.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span19:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span20 {
            position: absolute;
            top: 9%;
            left: 72.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span20:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span21 {
            position: absolute;
            top: 36%;
            left: 67.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span21:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span22 {
            position: absolute;
            top: 43%;
            left: 66%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span22:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span23 {
            position: absolute;
            top: 41%;
            left: 56%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span23:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span24 {
            position: absolute;
            top: 45.5%;
            left: 58.2%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span24:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span25 {
            position: absolute;
            top: 51.5%;
            left: 50.2%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span25:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span26 {
            position: absolute;
            top: 54%;
            left: 46%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;
        }


        .span26:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span27 {
            position: absolute;
            top: 60%;
            left: 48%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span27:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span28 {
            position: absolute;
            top: 69.6%;
            left: 52%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span28:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span29 {
            position: absolute;
            top: 65%;
            left: 52.5%;
            width: 10%;
            text-align: center;
            color: black;
            font-size: 0.6vw;
            font-weight: bold;
        }


        .span29:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span30 {
            position: absolute;
            top: 68%;
            left: 70.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span30:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span31 {
            position: absolute;
            top: 73%;
            left: 68.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span31:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span32 {
            position: absolute;
            top: 73%;
            left: 73.4%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span32:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span33 {
            position: absolute;
            top: 78%;
            left: 85.7%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span33:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span34 {
            position: absolute;
            top: 83%;
            left: 56%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;


        }


        .span34:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span35 {
            position: absolute;
            top: 85%;
            left: 56%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span35:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span36 {
            position: absolute;
            top: 83%;
            left: 36.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span36:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span37 {
            position: absolute;
            top: 83%;
            left: 26%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span37:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span38 {
            position: absolute;
            top: 83%;
            left: 17.3%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span38:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span39 {
            position: absolute;
            top: 29%;
            left: 42.7%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span39:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span40 {
            position: absolute;
            top: 29%;
            left: 67.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span40:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span41 {
            position: absolute;
            top: 39.5%;
            left: 66%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span41:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span42 {
            position: absolute;
            top: 66.5%;
            left: 68.5%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span42:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span43 {
            position: absolute;
            top: 67.5%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span43:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span44 {
            position: absolute;
            top: 72.2%;
            left: 79%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span44:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span45 {
            position: absolute;
            top: 73%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span45:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span46 {
            position: absolute;
            top: 75%;
            left: 60%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span46:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span47 {
            position: absolute;
            top: 75%;
            left: 63.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span47:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span48 {
            position: absolute;
            top: 73%;
            left: 63.5%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span48:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span49 {
            position: absolute;
            top: 87%;
            left: 44%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span49:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span50 {
            position: absolute;
            top: 89%;
            left: 44%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span50:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span51 {
            position: absolute;
            top: 91%;
            left: 44%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span52 {
            position: absolute;
            top: 93%;
            left: 44%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span52:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span53 {
            position: absolute;
            top: 93%;
            left: 48%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span53:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span54 {
            position: absolute;
            top: 91%;
            left: 48%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span54:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span55 {
            position: absolute;
            top: 89%;
            left: 48%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span55:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .span56 {
            position: absolute;
            top: 91%;
            left: 57.8%;
            width: 13%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span56:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }



        .span57 {
            position: absolute;
            top: 94%;
            left: 61.2%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span57:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span58 {
            position: absolute;
            top: 39.5%;
            left: 39.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span57:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span59 {
            position: absolute;
            top: 42%;
            left: 39.2%;
            width: 10%;
            text-align: center;
            color: white;
            font-size: 0.6vw;

        }


        .span59:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span60 {
            position: absolute;
            top: 42%;
            left: 42%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span59:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .span61 {
            position: absolute;
            top: 39.5%;
            left: 42%;
            width: 10%;
            text-align: center;
            color: #7cff00;
            font-size: 0.6vw;

        }


        .span51:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }


        .square-box {
            position: absolute;
            width: 10%;
            left: 25%;
            top: 20%;
            overflow: hidden;
            background: #fff;
        }

        .square-box:before {
            content: "";
            display: block;
            padding-bottom: 20%;
        }

        .square-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            border-color: black;
        }

        .square-content div {
            display: table;
            width: 100%;
            height: 100%;
        }

        .square-content span {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }
    </style>

</head>

<body style="margin: 0;background-color: #292852">
    <div class="penuh">
        <div class="pinch">
            <div class="span1">
                <div class="square-content">
                    <span>SS01</span>
                </div>
            </div>
            <div class="span2">
                <div class="square-content">
                    <span>SS02</span>
                </div>
            </div>
            <div class="span3">
                <div class="square-content">
                    <span>430WB01</span>
                </div>
            </div>
            <div class="span4">
                <div class="square-content">
                    <span>TK01</span>
                </div>
            </div>
            <div class="span5">
                <div class="square-content">
                    <span>BIN AFR</span>
                </div>
            </div>
            <div class="span6">
                <div class="square-content">
                    <span>CALCINER</span>
                </div>
            </div>
            <div class="span7">
                <div class="square-content">
                    <span>CALC FB01</span>
                </div>
            </div>
            <div class="span8">
                <div class="square-content">
                    <span>CALC FB02</span>
                </div>
            </div>
            <div class="span9">
                <div class="square-content">
                    <span>CALC FB03</span>
                </div>
            </div>
            <div class="span10">
                <div class="square-content">
                    <span>68,29 kV</span>
                </div>
            </div>
            <div class="span11">
                <div class="square-content">
                    <span>68,29 kV</span>
                </div>
            </div>
            <div class="span12">
                <div class="square-content">
                    <span>68,29 A</span>
                </div>
            </div>
            <div class="span13">
                <div class="square-content">
                    <span>68,29 A</span>
                </div>
            </div>
            <div class="span14">
                <div class="square-content">
                    <span>82,42°C</span>
                </div>
            </div>
            <div class="span15">
                <div class="square-content">
                    <span>144,22 A</span>
                </div>
            </div>
            <div class="span16">
                <div class="square-content">
                    <span>90.0 %</span>
                </div>
            </div>
            <div class="span17">
                <div class="square-content">
                    <span>12 mm/s</span>
                </div>
            </div>
            <div class="span18">
                <div class="square-content">
                    <span>12 mm/s</span>
                </div>
            </div>
            <div class="span19">
                <div class="square-content">
                    <span>40 mg/m3</span>
                </div>
            </div>
            <div class="span20">
                <div class="square-content">
                    <span>30.8 %</span>
                </div>
            </div>
            <div class="span21">
                <div class="square-content">
                    <span>90.0 %</span>
                </div>
            </div>
            <div class="span22">
                <div class="square-content">
                    <span>90.0 %</span>
                </div>
            </div>
            <div class="span23">
                <div class="square-content">
                    <span>2,1 A</span>
                </div>
            </div>
            <div class="span24">
                <div class="square-content">
                    <span>2,1 A</span>
                </div>
            </div>
            <div class="span25">
                <div class="square-content">
                    <span>42,1 A</span>
                </div>
            </div>
            <div class="span26">
                <div class="square-content">
                    <span>82,42°C</span>
                </div>
            </div>
            <div class="span27">
                <div class="square-content">
                    <span>68.8 bar</span>
                </div>
            </div>
            <div class="span28">
                <div class="square-content">
                    <span>2.7 mm/s</span>
                </div>
            </div>
            <div class="span29">
                <div class="square-content">
                    <span>DOWN</span>
                </div>
            </div>
            <div class="span30">
                <div class="square-content">
                    <span>82,42°C</span>
                </div>
            </div>
            <div class="span31">
                <div class="square-content">
                    <span>90.0 %</span>
                </div>
            </div>
            <div class="span32">
                <div class="square-content">
                    <span>2,1 A</span>
                </div>
            </div>
            <div class="span33">
                <div class="square-content">
                    <span>6,97 t</span>
                </div>
            </div>
            <div class="span34">
                <div class="square-content">
                    <span>32807 kW</span>
                </div>
            </div>
            <div class="span35">
                <div class="square-content">
                    <span>89.0 %</span>
                </div>
            </div>
            <div class="span36">
                <div class="square-content">
                    <span>46,96 ton</span>
                </div>
            </div>
            <div class="span37">
                <div class="square-content">
                    <span>46,96 ton</span>
                </div>
            </div>
            <div class="span38">
                <div class="square-content">
                    <span>46,96 ton</span>
                </div>
            </div>
            <div class="span39">
                <div class="square-content">
                    <span>DA02</span>
                </div>
            </div>
            <div class="span40">
                <div class="square-content">
                    <span>DA03</span>
                </div>
            </div>
            <div class="span41">
                <div class="square-content">
                    <span>DA04</span>
                </div>
            </div>
            <div class="span42">
                <div class="square-content">
                    <span>DA06</span>
                </div>
            </div>
            <div class="span43">
                <div class="square-content">
                    <span>From Preduster</span>
                </div>
            </div>
            <div class="span44">
                <div class="square-content">
                    <span>From ID Fan</span>
                </div>
            </div>
            <div class="span45">
                <div class="square-content">
                    <span>O2</span>
                </div>
            </div>
            <div class="span46">
                <div class="square-content">
                    <span>CO</span>
                </div>
            </div>
            <div class="span47">
                <div class="square-content">
                    <span>0,03 ppm</span>
                </div>
            </div>
            <div class="span48">
                <div class="square-content">
                    <span>2,17 %</span>
                </div>
            </div>
            <div class="span49">
                <div class="square-content">
                    <span>PEMAKAIAN COAL TO KILN</span>
                </div>
            </div>
            <div class="span50">
                <div class="square-content">
                    <span>CF01</span>
                </div>
            </div>
            <div class="span51">
                <div class="square-content">
                    <span>CF02</span>
                </div>
            </div>
            <div class="span52">
                <div class="square-content">
                    <span>CF03</span>
                </div>
            </div>
            <div class="span53">
                <div class="square-content">
                    <span>27.03 %</span>
                </div>
            </div>
            <div class="span54">
                <div class="square-content">
                    <span>27.03 %</span>
                </div>
            </div>
            <div class="span55">
                <div class="square-content">
                    <span>27.03 %</span>
                </div>
            </div>
            <div class="span56">
                <div class="square-content">
                    <span>TOTAL PEMAKAIAN FINE COAL</span>
                </div>
            </div>
            <div class="span57">
                <div class="square-content">
                    <span>27.03 t/h</span>
                </div>
            </div>
            <div class="span58">
                <div class="square-content">
                    <span>O2</span>
                </div>
            </div>
            <div class="span59">
                <div class="square-content">
                    <span>CO</span>
                </div>
            </div>
            <div class="span60">
                <div class="square-content">
                    <span>2,17 %</span>
                </div>
            </div>
            <div class="span61">
                <div class="square-content">
                    <span>0,03 ppm</span>
                </div>
            </div>
        </div>
    </div>

</body>

<!-- <body>
	<div class="wrapper wrapper-content">
		<div class="container">
			<div class="row">
				<div class="penuh">
					<div class="pinch">
						<div class="cargo">
							<div class="silo10">
								<div class="square-content">
									<span id="isisilo10"></span>
								</div>
							</div>
							<div class="silo11">
								<div class="square-content">
									<span id="isisilo11"></span>
								</div>
							</div>
							<div class="silo12">
								<div class="square-content">
									<span id="isisilo12"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
    function load() {
        // var url = 'ISIKAN URL API';
        // $.ajax({
        // 	url: url,
        // 	type: 'get',
        // 	// dataType: 'json',
        // 	success: function(data) {
        // 		var data1 = data.replace("<title>Json</title>", "");
        // 		var data2 = data1.replace("(", "[");
        // 		var data3 = data2.replace(");", "]");
        // 		var dataJson = JSON.parse(data3);
        // 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
        // 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
        // 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
        // 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
        // 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
        // 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
        // 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
        // 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
        // 		$("#isisilo10").html(tonageSilo10);
        // 		$("#isisilo11").html(tonageSilo11);
        // 		$("#isisilo12").html(tonageSilo12);
        // 	}
        // });
    }
    $(function() {
        setInterval(load, 1000);
        $('.pinch').each(function() {
            new RTP.PinchZoom($(this), {});
        });
    });
</script>
</body>

</html>