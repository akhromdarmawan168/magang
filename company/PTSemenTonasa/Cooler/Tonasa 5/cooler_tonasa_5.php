<html>

<head>
	<title>HMI Interface</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<link href="http://localhost/magang/css/bootstrap.css" rel="stylesheet">
	<link href="http://localhost/magang/css/style.css" rel="stylesheet">
	<script src="http://localhost/magang/js/jquery-2.1.1.js"></script>
	<style>
		.penuh {
			position: relative;
			width: 93%;
			height: 100%;
			padding-bottom: 0%;
			left: 3%;
			overflow: hidden;
		}

		.pinch {
			position: absolute;
			width: 100%;
			overflow: hidden;
			background-size: 100% 100%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url('cooler_tonasaa_5.png');

		}

		.pinch:before {
			content: "";
			display: block;
			padding-bottom: 54.5%;
		}

		.name542fn01 {
			position: absolute;
			top: 8%;
			left: 11.4%;
			color: white;
			font-size: 0.7vw;
		}



		.name542fn01value {
			position: absolute;
			top: 14%;
			left: 12%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.ldo2 {
			position: absolute;
			top: 14%;
			left: 18%;
			color: white;
			font-size: 0.7vw;
		}

		.cn01 {
			position: absolute;
			top: 21%;
			left: 21.3%;
			color: black;
			font-size: 0.7vw;
			font-weight: bold;

		}

		.cn02 {
			position: absolute;
			top: 21%;
			left: 27.9%;
			color: black;
			font-size: 0.7vw;
			font-weight: bold;

		}

		.name544CV01 {
			position: absolute;
			top: 42%;
			left: 24.3%;
			color: white;
			font-size: 0.7vw;
		}

		.name544CV03 {
			position: absolute;
			top: 24%;
			left: 42%;
			color: white;
			font-size: 0.7vw;
		}

		.name544CV04 {
			position: absolute;
			top: 28.4%;
			left: 55%;
			color: white;
			font-size: 0.7vw;
		}

		.name544CV05 {
			position: absolute;
			top: 32.3%;
			left: 41%;
			color: white;
			font-size: 0.7vw;
		}


		.name544CV06 {
			position: absolute;
			top: 37.9%;
			left: 39%;
			color: white;
			font-size: 0.7vw;
		}

		.name544CV07 {
			position: absolute;
			top: 42.5%;
			left: 49%;
			color: white;
			font-size: 0.7vw;
		}

		.name544CV02 {
			position: absolute;
			top: 50%;
			left: 70.5%;
			color: white;
			font-size: 0.7vw;
		}

		.name546AC01 {
			position: absolute;
			top: 54%;
			left: 84.5%;
			color: white;
			font-size: 0.7vw;
		}

		.name546AC02 {
			position: absolute;
			top: 60%;
			left: 86%;
			color: white;
			font-size: 0.7vw;
		}

		.nameCR01 {
			top: 57%;
			left: 56%;
			color: black;
			font-size: 0.7vw;
			font-weight: bold;
		}

		.value1 {
			position: absolute;
			top: 17%;
			left: 40%;
			color: #7CFF00;
			font-size: 0.7vw;

		}

		.nameld01 {
			position: absolute;
			top: 22%;
			left: 61%;
			color: white;
			font-size: 0.7vw;
		}

		.nameld01value {
			position: absolute;
			top: 16%;
			left: 61%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.name544FN02 {
			position: absolute;
			top: 22%;
			left: 67.2%;
			color: white;
			font-size: 0.7vw;
		}

		.name544FN02value {
			position: absolute;
			top: 9%;
			left: 67.5%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value2 {
			position: absolute;
			top: 9%;
			left: 78%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.tabelelectrostaticprecitator {
			position: absolute;
			top: 16%;
			left: 82%;
			width: 15%;
			overflow: hidden;
			color: white;
			font-size: 0.6vw;
		}

		.tabelelectrostaticprecitator::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.voltagetitile {
			position: absolute;
			top: 20%;
			left: 84%;
			right: 1%;
			color: white;
			font-size: 0.7vw;
		}

		.voltagevalue {
			position: absolute;
			top: 98%;
			left: 6;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.currenttitle {
			position: absolute;
			top: 20%;
			left: 90%;
			right: 1%;
			color: white;
			font-size: 0.7vw;
		}

		.currentvalue {
			position: absolute;
			top: 98%;
			left: 4;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.finishmill1 {
			position: absolute;
			top: 34%;
			left: 76%;
			color: white;
			font-size: 0.7vw;
		}

		.finishmill2 {
			position: absolute;
			top: 39%;
			left: 76%;
			color: white;
			font-size: 0.7vw;
		}

		.value3 {
			position: absolute;
			top: 47%;
			left: 12%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value3:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value4 {

			position: absolute;
			top: 49.5%;
			left: 11%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value4:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.value5 {
			position: absolute;
			top: 58%;
			left: 11%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		#margin2x {
			margin-bottom: 2px;
		}

		#margin7x {
			margin-bottom: 7px;
		}

		.value6 {
			position: absolute;
			top: 73%;
			left: 10%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value7 {
			position: absolute;
			top: 73%;
			left: 15%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value8 {
			position: absolute;
			top: 73%;
			left: 20%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value9 {
			position: absolute;
			top: 73%;
			left: 25%;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value10 {
			position: absolute;
			top: 58%;
			left: 11%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value11 {
			position: absolute;
			top: 57%;
			left: 33.4%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value12 {
			position: absolute;
			top: 57%;
			left: 39.3%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.value13 {
			position: absolute;
			top: 66%;
			left: 45.5%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value13::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label1 {
			position: absolute;
			top: 57%;
			left: 52%;
			width: 10%;
			font-weight: bold;
			text-align: center;
			color: black;
			font-size: 0.7vw;
		}

		.label1::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.label1value {
			position: absolute;
			top: 57%;
			left: 56.3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.label1value::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn03 {
			position: absolute;
			top: 83%;
			left: 6.7%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn03::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn04 {
			position: absolute;
			top: 83%;
			left: 11.2%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn04::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn05 {
			position: absolute;
			top: 83%;
			left: 16.6%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn05::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn06 {
			position: absolute;
			top: 83%;
			left: 21.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn06::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn07 {
			position: absolute;
			top: 83%;
			left: 26.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn07::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn08 {
			position: absolute;
			top: 83%;
			left: 31.5%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn08::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn09 {
			position: absolute;
			top: 83%;
			left: 36.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn09::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn10 {
			position: absolute;
			top: 83%;
			left: 41.3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn10::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn11 {
			position: absolute;
			top: 83%;
			left: 46.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn11::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn12 {
			position: absolute;
			top: 83%;
			left: 51.3%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn12::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.fn13 {
			position: absolute;
			top: 83%;
			left: 56.4%;
			width: 10%;
			text-align: center;
			color: #7CFF00;
			font-size: 0.7vw;
		}

		.fn13::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelfan {
			position: absolute;
			top: 86%;
			left: 1%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
		}

		.labelfan:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name544MD01 {
			position: absolute;
			top: 77%;
			left: 65%;
			width: 10%;
			color: white;
			font-size: 0.7vw;
		}

		.name544MD01::before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name551SI02 {
			position: absolute;
			top: 74.4%;
			left: 81.3%;
			width: 10%;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.name551SI02:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.name551SI02value {
			position: absolute;
			top: 72.1%;
			left: 81.7%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.name551SI02value:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.labelclinkerstorege {
			position: absolute;
			top: 73%;
			left: 89.5%;
			width: 8%;
			color: black;
			font-weight: bold;
			font-size: 0.6vw;
		}

		.labelclinkerstorege:before {
			content: "";
			display: block;
			padding-bottom: 10%;
		}

		.clinkerstoragevalue {
			position: absolute;
			top: 69%;
			left: 92.8%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.clinkerstoragevalue:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value14 {
			position: absolute;
			top: 53%;
			left: 71.4%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.vlaue14:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.value15 {
			position: absolute;
			top: 43%;
			left: 90%;
			width: 10%;
			color: #7CFF00;
			font-size: 0.6vw;
		}

		.value15:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.spanbutton1 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 92%;
			top: 4%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;
			color: #42ff00;
		}

		.spanbutton1:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.spanbutton2 {
			position: absolute;
			width: 7%;
			height: 7%;
			left: 84%;
			top: 4%;
			font-weight: normal;
			overflow: hidden;
			/* font-size: 10px; */
			font-size: 0.6vw;
			text-align: center;
			color: #42ff00;
		}

		.spanbutton2:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-box {
			position: absolute;
			width: 10%;
			left: 25%;
			top: 20%;
			overflow: hidden;
			background: #fff;
		}

		.square-box:before {
			content: "";
			display: block;
			padding-bottom: 20%;
		}

		.square-content {
			position: absolute;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
			border-color: black;
		}

		.square-content div {
			display: table;
			width: 100%;
			height: 100%;
		}

		.square-content span {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>

<body style="margin: 0;background-color: #292852">
	<div class="penuh">
		<div class="pinch">
			<div class="name542fn01">
				<p>542FN01</p>
			</div>
			<div class="name542fn01value">
				<p>26 %</p>
			</div>
			<div class="ldo2">
				<p>LD02</p>
			</div>
			<div class="cn01">
				<p>CN01</p>
			</div>
			<div class="cn02">
				<p>CN02</p>
			</div>
			<div class="name544CV01">
				<p>544CV01</p>
			</div>
			<div class="name544CV03">
				<p>544CV03</p>
			</div>
			<div class="name544CV04">
				<p>544CV04</p>
			</div>
			<div class="name544CV05">
				<p>544CV05</p>
			</div>
			<div class="name544CV06">
				<p>55CV06</p>
			</div>
			<div class="name544CV07">
				<p>544CV07</p>
			</div>
			<div class="name544CV02">
				<p>544CV02</p>
			</div>
			<div class="name546AC01">
				<p>546AC01</p>
			</div>
			<div class="name546AC02">
				<p>546AC02</p>
			</div>
			<div class="value1">
				<p>240 ° C</p>
			</div>
			<div class="nameld01">
				<p>LD01</p>
			</div>
			<div class="nameld01value">
				<p>94,6 %</p>
			</div>
			<div class="name544FN02">
				<p>544FN02</p>
			</div>
			<div class="name544FN02value ">
				<p style="margin-bottom: 2px">224 A</p>
				<p style="margin-bottom: 2px">29 KW</p>
				<p>240 rpm</p>
			</div>
			<div class="value2">
				<p>22 mg/m3</p>
			</div>
			<div class="tabelelectrostaticprecitator">
				<div class="square-content">
					<!-- <p>ELECTROSTATIC PRECIPITATOR</p> -->
					<span id="tabelelectrostaticprecitator">ELECTROSTATIC PRECIPITATOR</span>
				</div>
			</div>
			<div class="voltagetitile">
				<p>Voltage</p>
				<div class="voltagevalue">
					<p style="margin-bottom: 2px">85 kV</p>
					<p style="margin-bottom: 2px">87 kV</p>
					<p style="margin-bottom: 2px">90 kV</p>

				</div>
			</div>
			<div class="currenttitle">
				<p>Current</p>
				<div class="currentvalue">
					<p id="margin2x">1012 mA</p>
					<p id="margin2x">998 mA</p>
					<p id="margin2x">1001 mA</p>
				</div>
			</div>
			<div class="finishmill1">
				<p>Finish Mill 1</p>
			</div>
			<div class="finishmill2">
				<p>Finish Mill 2</p>
			</div>
			<div class="value3">
				<div class="square-content">
					<span> 960°C</span>
				</div>
			</div>
			<div class="value4">
				<div class="square-content">
					<span>- 0,44 mbar</span>
				</div>
			</div>
			<div class="value5">
				<p style="margin-bottom: 8px;">60 mbar</p>
				<p style="margin-bottom: 7px;">13,5st/min</p>
				<p>3,28 kwh/ton</p>
			</div>
			<div class="value6">
				<p>54 mbar </p>
			</div>
			<div class="value7">
				<p>74 mbar </p>
			</div>
			<div class="value8">
				<p>71 mbar </p>
			</div>
			<div class="value9">
				<p>65 mbar </p>
			</div>
			<div class="value11">
				<p style="margin-bottom: 3px">125 mbar</p>
				<p style="margin-bottom: 4px">125 mbar</p>
				<p style="margin-bottom: 4px">125 mbar</p>
				<p style="margin-bottom: 3px">125 mbar</p>
			</div>
			<div class="value12">
				<p style="margin-bottom: 3px">96 mbar</p>
				<p style="margin-bottom: 4px">95 mbar</p>
				<p style="margin-bottom: 4px">94 mbar</p>
				<p style="margin-bottom: 3px">105 mbar</p>
			</div>
			<div class="value13">
				<div class="square-content">
					<span>2,19 kg.air/kg.cl</span>
				</div>
			</div>
			<div class="label1">
				<div class="square-content">
					<p style="margin-bottom: 3px">CR01</p>
					<p style="margin-bottom: 3px">CR02</p>
					<p style="margin-bottom: 3px">TR01</p>
					<p style="margin-bottom: 3px">TR01</p>

				</div>
			</div>
			<div class="label1value">
				<div class="square-content">
					<p style="margin-bottom: 3px">30 A</p>
					<p style="margin-bottom: 3px">31 A</p>
					<p style="margin-bottom: 3px">17 A</p>
					<p style="margin-bottom: 3px">21 A</p>
				</div>
			</div>
			<div class="labelfan">
				<div class="square-content">
					<p style="margin-bottom: 3px">Fan Power</p>
					<p style="margin-bottom: 3px">Fan Speed</p>
					<p style="margin-bottom: 3px">Fan Flow (Nm3/min)</p>
				</div>
			</div>
			<div class="fn03">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN03</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn04">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN04</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn05">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN05</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn06">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN06</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn07">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN07</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn08">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN08</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn09">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN09</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn10">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN10</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn11">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN11</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn12">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN12</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="fn13">
				<div class="square-content">
					<p style="margin-bottom: 3px;color: white;">FN12</p>
					<p style="margin-bottom: 3px">109 KW</p>
					<p style="margin-bottom: 3px">78 %</p>
					<p style="margin-bottom: 3px">1038</p>
				</div>
			</div>
			<div class="name544MD01">
				<div class="square-content">
					<span>544MD01</span>
				</div>
			</div>
			<div class="name551SI02">
				<div class="square-content">
					<span>551SI02</span>
				</div>
			</div>
			<div class="name551SI02value">
				<div class="square-content">
					<span>60 %</span>
				</div>
			</div>
			<div class="labelclinkerstorege">
				<div class="square-content">
					<span>CLINKER STORAGE 551SI01</span>
				</div>
			</div>
			<div class="clinkerstoragevalue">
				<div class="square-content">
					<span>88 %</span>
				</div>
			</div>
			<div class="value14">
				<div class="square-content">
					<span>56 %</span>
				</div>
			</div>
			<div class="value15">
				<div class="square-content">
					<span>300 t/h</span>
				</div>
			</div>
			<!-- Button -->
			<div class="spanButton1">
				<div class="square-content">
					<a href="">
						<button disabled="" type="button" class="btn btn-default" style="width: 100%;">Cooler</button></a>
				</div>
			</div>
			<div class="spanButton2">
				<div class="square-content">
					<a href="">
						<button type="button" class="btn btn-default" style="width: 100%;">Kiln</button></a>
				</div>
			</div>
		</div>
	</div>

</body>

<script src="http://localhost/magang/js/bootstrap.min.js"></script>
<!-- <script src="js/inspinia.js"></script> -->
<script src="http://localhost/magang/pinchzoom-master/src/pinchzoom.js"></script>
<script>
	function load() {
		// var url = 'ISIKAN URL API';
		// $.ajax({
		// 	url: url,
		// 	type: 'get',
		// 	// dataType: 'json',
		// 	success: function(data) {
		// 		var data1 = data.replace("<title>Json</title>", "");
		// 		var data2 = data1.replace("(", "[");
		// 		var data3 = data2.replace(");", "]");
		// 		var dataJson = JSON.parse(data3);
		// 		tonageSilo09 = parseFloat(dataJson[0].tags[0].props[0].val).toFixed(2);
		// 		tonageSilo10 = parseFloat(dataJson[0].tags[1].props[0].val).toFixed(2);
		// 		tonageSilo11 = parseFloat(dataJson[0].tags[2].props[0].val).toFixed(2);
		// 		tonageSilo12 = parseFloat(dataJson[0].tags[3].props[0].val).toFixed(2);
		// 		tonageSilo13 = parseFloat(dataJson[0].tags[4].props[0].val).toFixed(2);
		// 		tonageSilo14 = parseFloat(dataJson[0].tags[5].props[0].val).toFixed(2);
		// 		tonageSilo15 = parseFloat(dataJson[0].tags[6].props[0].val).toFixed(2);
		// 		tonageSilo16 = parseFloat(dataJson[0].tags[7].props[0].val).toFixed(2);
		// 		$("#isisilo10").html(tonageSilo10);
		// 		$("#isisilo11").html(tonageSilo11);
		// 		$("#isisilo12").html(tonageSilo12);
		// 	}
		// });
	}
	$(function() {
		setInterval(load, 1000);
		$('.pinch').each(function() {
			new RTP.PinchZoom($(this), {});
		});
	});
</script>
</body>

</html>